use EENMAALANDERMAAL
go

CREATE TRIGGER CK_VerkoperInsert ON Verkoper
FOR INSERT
AS 
BEGIN
	if EXISTS (Select * from Gebruiker where gebruikerID = (select gebruikerID from inserted)  and verkoper = 1)
	Begin
		raiserror('Gebruiker is al een verkoper', 16, 1);
		rollback TRANSACTION;
	END

	UPDATE [dbo].[GEBRUIKER]
	SET verkoper = 1
	from GEBRUIKER
	WHERE gebruikerID = (select gebruikerID from inserted)
END
GO

CREATE TRIGGER CK_VoorwerpRubriekChilds ON Voorwerp_Rubriek
FOR INSERT
AS 
BEGIN
	if ((Select count(*) from Rubriek where rubriekID = (select rubriekID from inserted) AND 0 < (select count(*) from RUBRIEK where bovenliggendID = (select rubriekID from inserted))) > 0)
	Begin
		raiserror('Rubriek heeft een subrubriek. Kies een ander rubriek', 17, 1);
		rollback TRANSACTION;
	END
END
GO

CREATE TRIGGER CK_IsVerkoper ON Voorwerp
FOR INSERT
AS 
SET NOCOUNT ON
if NOT EXISTS (select * from VERKOPER where gebruikerID = (select gebruikerID from Voorwerp where voorwerpID = (select voorwerpID from inserted)))
BEGIN
		raiserror('Gebruiker is geen verkoper!', 17, 1);
		rollback TRANSACTION;
END
GO
CREATE TRIGGER TR_Activatecode ON Gebruiker
FOR INSERT
AS 
SET NOCOUNT ON
BEGIN
	UPDATE [dbo].[GEBRUIKER]
	SET activatiecode = (SELECT NEWID() as KeyValue)
	from GEBRUIKER
	WHERE gebruikerID = (select gebruikerID from inserted)
END
GO

CREATE TRIGGER CK_ActivatecodeIsSet ON Gebruiker
FOR INSERT
AS 
SET NOCOUNT ON
if NOT EXISTS (select activatiecode from inserted)
BEGIN
raiserror('Activatiecode moet gezet zijn bij ');
END
GO

