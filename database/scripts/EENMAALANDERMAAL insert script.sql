USE EENMAALANDERMAAL
GO

SELECT * FROM VOORWERP;

USE [EENMAALANDERMAAL]
GO

INSERT INTO [dbo].[VRAAG]
           ([tekst])
     VALUES
           ('Wat is de naam van je huisdier?');
INSERT INTO [dbo].[VRAAG]
           ([tekst])
     VALUES
           ('In welke straat wonen je ouders?');
INSERT INTO [dbo].[VRAAG]
           ([tekst])
     VALUES
           ('Wat is de naam van je broer/zus?');
INSERT INTO [dbo].[VRAAG]
           ([tekst])
     VALUES
           ('Wat is je geboorteplaats?');


INSERT INTO [dbo].[GEBRUIKER]
           ([vraagID]
           ,[gebruikersnaam]
           ,[achternaam]
           ,[voornaam]
           ,[adres]
           ,[antwoordtekst]
           ,[datum]
           ,[emailadres]
           ,[landnaam]
           ,[plaatsnaam]
           ,[postcode]
           ,[wachtwoord]
           ,[verkoper])
     VALUES
           (1,
		   'JanKaas',
		   'Jan',
		   'Kaas',
		   'Hogestraat 4',
		   'Kees',
		   '20161111',
		   'jankaas@hotmail.com',
		   'Nederland',
		   'Druten',
		   '6651HB',
		   'wachtwoord',
		   0)
INSERT INTO [dbo].[GEBRUIKER]
           ([vraagID]
           ,[gebruikersnaam]
           ,[achternaam]
           ,[voornaam]
           ,[adres]
           ,[antwoordtekst]
           ,[datum]
           ,[emailadres]
           ,[landnaam]
           ,[plaatsnaam]
           ,[postcode]
           ,[wachtwoord]
           ,[verkoper])
     VALUES
           (2,
		   'PietKaas',
		   'Piet',
		   'Kaas',
		   'Hogestraat 4',
		   'Kees',
		   '20161111',
		   'pietkaas@hotmail.com',
		   'Nederland',
		   'Druten',
		   '6651HB',
		   'wachtwoord',
		   0);
		   
INSERT INTO [dbo].[GEBRUIKERSTELEFOON]
           ([gebruikerID]
           ,[telefoonnummer]
           ,[volgnr])
     VALUES
           (0
           ,'0773827678'
           ,'01')
INSERT INTO [dbo].[GEBRUIKERSTELEFOON]
           ([gebruikerID]
           ,[telefoonnummer]
           ,[volgnr])
     VALUES
           (0
           ,'063668974'
           ,'02')
INSERT INTO [dbo].[GEBRUIKERSTELEFOON]
           ([gebruikerID]
           ,[telefoonnummer]
           ,[volgnr])
     VALUES
           (1
           ,'0778954235'
           ,'01')
INSERT INTO [dbo].[GEBRUIKERSTELEFOON]
           ([gebruikerID]
           ,[telefoonnummer]
           ,[volgnr])
     VALUES
           (1
           ,'0652347963'
           ,'02')
INSERT INTO [dbo].[VERKOPER]
           ([gebruikerID]
		   ,[controleoptie]
           ,[banknaam]
           ,[rekeningnummer])
     VALUES
           (1
		   ,'iets'
           ,'ABN Amro'
           ,'ABNL0302302231')
INSERT INTO [dbo].[RUBRIEK]
           (rubrieknaam)
     VALUES
           ('Auto''s, boten en motoren')
INSERT INTO [dbo].[RUBRIEK]
           (rubrieknaam)
     VALUES
           ('Kleding')
INSERT INTO [dbo].[RUBRIEK]
           (rubrieknaam)
     VALUES
           ('Technologie')
INSERT INTO [dbo].[RUBRIEK]
           (rubrieknaam
		   ,bovenliggendID)
     VALUES
           ('Auto''s'
		   ,1)
INSERT INTO [dbo].[RUBRIEK]
           (rubrieknaam
		   ,bovenliggendID)
     VALUES
           ('Motoren'
		   ,1)
INSERT INTO [dbo].[RUBRIEK]
           (rubrieknaam
		   ,bovenliggendID)
     VALUES
           ('Boten'
		   ,1)
INSERT INTO [dbo].[RUBRIEK]
           (rubrieknaam
		   ,bovenliggendID)
     VALUES
           ('Dameskleding'
		   ,2)
INSERT INTO [dbo].[RUBRIEK]
           (rubrieknaam
		   ,bovenliggendID)
     VALUES
           ('Herenkleding'
		   ,2)
INSERT INTO [dbo].[RUBRIEK]
           (rubrieknaam
		   ,bovenliggendID)
     VALUES
           ('Computer'
		   ,3)
INSERT INTO [dbo].[RUBRIEK]
           (rubrieknaam
		   ,bovenliggendID)
     VALUES
           ('Smartphones'
		   ,3)
INSERT INTO [dbo].[RUBRIEK]
           (rubrieknaam
		   ,bovenliggendID)
     VALUES
           ('Computeronderdelen'
		   ,9)
INSERT INTO [dbo].[VOORWERP]
           ([gebruikerID]
           ,[dagen]
           ,[startprijs]
           ,[beschrijving]
           ,[datum]
           ,[tijd]
           ,[titel]
		   ,[gesloten])
     VALUES
           (1
           ,10
           ,900.00
           ,'Auto te koop, bijna nooit gebruikt en in goede staat.'
           ,GETDATE()
           ,CURRENT_TIMESTAMP
           ,'Ford Focus te koop'
		   ,0)
INSERT INTO [dbo].[VOORWERP_RUBRIEK]
           ([voorwerpID]
           ,[rubriekID])
     VALUES
           (0
           ,3)
INSERT INTO [dbo].[VOORWERP]
           ([gebruikerID]
           ,[dagen]
           ,[startprijs]
           ,[beschrijving]
           ,[datum]
           ,[tijd]
           ,[titel]
		   ,[gesloten])
     VALUES
           (1
           ,10
           ,5.00
           ,'Trui te koop. in goede staat, bijna nooit gedragen.'
           ,GETDATE()
           ,CURRENT_TIMESTAMP
           ,'Trui te koop'
		   ,0)
INSERT INTO [dbo].[VOORWERP_RUBRIEK]
           ([voorwerpID]
           ,[rubriekID])
     VALUES
           (1
           ,7)
INSERT INTO [dbo].[BESTAND]
           ([bestandsnaam]
           ,[voorwerpID])
     VALUES
           ('auto.png'
           ,0)
INSERT INTO [dbo].[BESTAND]
           ([bestandsnaam]
           ,[voorwerpID])
     VALUES
           ('trui.png'
           ,1)
GO




































