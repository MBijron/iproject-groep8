<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Login/Registreren</title>
    
    <link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body>

<body>
<!-- Nieuwe navbar -->
    <div class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a href="../" class="navbar-brand"><img src="img/hamer2.png" width="25" height="25">EenmaalAndermaal</a>
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse" id="navbar-main">
                <div class="nav navbar-nav">
                    <form class="navbar-form" role="search">
                        <input type="text" class="form-control" placeholder="Zoekterm" name="zoekterm" id="zoekterm">
                        <select name="rubriek" id="rubriek" class="form-control">
                            <option value="kleding">Kleding</option>
                            <option value="autoonderdelen">Auto-Onderdelen</option>
                            <option value="overig">Overig</option>
                        </select>
                        <input class="btn btn-default" type="submit" value="Zoek">
                    </form>
                </div>


                <ul class="nav navbar-nav navbar-right">
                    <li><a href="login.php" target="_blank">Login/Registreer</a></li>
                </ul>

            </div>
        </div>
    </div>

    <main>
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <form class="form-signin">
                        <h2 class="form-signin-heading content-title">Inloggen</h2>
                        <div class="loginbox">
                            <label for="inputEmail" class="sr-only">Gebruikersnaam</label>
                            <input id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="" type="email">
                            <label for="inputPassword" class="sr-only">Wachtwoord</label>
                            <input id="inputPassword" class="form-control" placeholder="Password" required="" type="password">
                            <a href="#" class="btn btn-lg btn-primary">Login</a>
                            <div class="vergeten">
                                <a href="#">Wachtwoord vergeten?</a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-6">
                    <div class="registreer">
                        <h2 class="form-signin-heading content-title">Registreer</h2>
                        <div class="registreerblok">
                            <div class="registreertekst">
                                <div>Om in te loggen heb je een account nodig. Je kunt dan voortaan:</div>
                                <ul>
                                    <li>Je eigen veiling starten</li>
                                    <li>Bieden op andermans veilingen</li>
                                </ul>
                            </div>
                            <a href="#" class="btn btn-lg btn-primary">Nieuw account aanmaken</a>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- /container -->
    </main>

    <footer>
        <div>
            <a href="#">Over Ons</a>
            <span>|</span>
            <a href="#">Werken bij</a>
        </div>
        <p class="disclaimer">EenmaalAndermaal is niet aanspraakelijk voor (gevolg)schade die voorkomt uit het gebruik van deze site, dan wel uit fouten of ontbrekende functionaliteiten op deze site.</p>
        <p class="copyright">Copyright &copy; 2016 EenmaalAndermaal. Alle rechten voorbehouden.</p>
    </footer>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  

</body>
</body>
</html>