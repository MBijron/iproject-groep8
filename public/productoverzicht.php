<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Homepage</title>

    <link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body>

<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a href="../" class="navbar-brand"><img src="img/hamer2.png" width="25" height="25">EenmaalAndermaal</a>
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
            <div class="nav navbar-nav">
                <form class="navbar-form" role="search">
                    <input type="text" class="form-control" placeholder="Zoekterm" name="zoekterm" id="zoekterm">
                    <select name="rubriek" id="rubriek" class="form-control">
                        <option value="kleding">Kleding</option>
                        <option value="autoonderdelen">Auto-Onderdelen</option>
                        <option value="overig">Overig</option>
                    </select>
                    <input class="btn btn-default" type="submit" value="Zoek">
                </form>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="login.php" target="_blank">Login/Registreer</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <main>
        <div class="row">
            <div class="list-group col-md-3"><!-- Dit is de verticale menubalk!-->
                <a href="#" class="list-group-item active">Rubrieken</a>
                <a href="#" class="list-group-item">Kleding</a>
                <a href="#" class="list-group-item">Huishoudmiddelen</a>
                <a href="#" class="list-group-item">Auto's</a>
            </div>
            <div class="col-md-9">
                <div class="productoverzicht">
                    <div class="item-body row">
                        <a class="item-clickable-container" href="#">
                            <div class="col-sm-3">
                                <figure><img src="img/uitlaat2.jpg" width="100" height="100"></figure>
                            </div>
                            <div class="item-info col-sm-5">
                                <p>Grote uitlaat voor auto</p>
                                <p class="info-text">Dit is een grote uitlaat voor een grote auto. Wil je meer info neem dan ff contact met me op. Karel.</p>
                            </div>
                            <div class="item-prijs col-sm-4">
                                <p>Huidige bod: € 35,21</p>
                            </div>
                        </a>
                    </div>
                    <div class="item-body row">
                        <a class="item-clickable-container" href="#">
                            <div class="col-sm-3">
                                <figure><img src="img/uitlaat2.jpg" width="100" height="100"></figure>
                            </div>
                            <div class="item-info col-sm-5">
                                <p>Grote uitlaat voor auto</p>
                                <p class="info-text">Dit is een grote uitlaat voor een grote auto. Wil je meer info neem dan ff contact met me op. Karel.</p>
                            </div>
                            <div class="item-prijs col-sm-4">
                                <p>Huidige bod: € 35,21</p>
                            </div>
                        </a>
                    </div>
                    <div class="item-body row">
                        <a class="item-clickable-container" href="#">
                            <div class="col-sm-3">
                                <figure><img src="img/uitlaat2.jpg" width="100" height="100"></figure>
                            </div>
                            <div class="item-info col-sm-5">
                                <p>Grote uitlaat voor auto</p>
                                <p class="info-text">Dit is een grote uitlaat voor een grote auto. Wil je meer info neem dan ff contact met me op. Karel.</p>
                            </div>
                            <div class="item-prijs col-sm-4">
                                <p>Huidige bod: € 35,21</p>
                            </div>
                        </a>
                    </div>
                    <div class="item-body row">
                        <a class="item-clickable-container" href="#">
                            <div class="col-sm-3">
                                <figure><img src="img/uitlaat2.jpg" width="100" height="100"></figure>
                            </div>
                            <div class="item-info col-sm-5">
                                <p>Grote uitlaat voor auto</p>
                                <p class="info-text">Dit is een grote uitlaat voor een grote auto. Wil je meer info neem dan ff contact met me op. Karel.</p>
                            </div>
                            <div class="item-prijs col-sm-4">
                                <p>Huidige bod: € 35,21</p>
                            </div>
                        </a>
                    </div>
                    <div class="item-body row">
                        <a class="item-clickable-container" href="#">
                            <div class="col-sm-3">
                                <figure><img src="img/uitlaat2.jpg" width="100" height="100"></figure>
                            </div>
                            <div class="item-info col-sm-5">
                                <p>Grote uitlaat voor auto</p>
                                <p class="info-text">Dit is een grote uitlaat voor een grote auto. Wil je meer info neem dan ff contact met me op. Karel.</p>
                            </div>
                            <div class="item-prijs col-sm-4">
                                <p>Huidige bod: € 35,21</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<footer>
    <div class="container">
        <a href="#">Over Ons</a>
        <span>|</span>
        <a href="#">Werken bij</a>
        <p class="disclaimer">EenmaalAndermaal is niet aanspraakelijk voor (gevolg)schade die voorkomt uit het gebruik van deze site, dan wel uit fouten of ontbrekende functionaliteiten op deze site.</p>
        <p class="copyright">Copyright &copy; 2016 EenmaalAndermaal. Alle rechten voorbehouden.</p>
    </div>
</footer>
<div class="container">
</div>
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>