<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Login/Registreren</title>

    <link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body>
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a href="../" class="navbar-brand"><img src="img/hamer2.png" width="25" height="25">EenmaalAndermaal</a>
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
            <div class="nav navbar-nav">
                <form class="navbar-form" role="search">
                    <input type="text" class="form-control" placeholder="Zoekterm" name="zoekterm" id="zoekterm">
                    <select name="rubriek" id="rubriek" class="form-control">
                        <option value="kleding">Kleding</option>
                        <option value="autoonderdelen">Auto-Onderdelen</option>
                        <option value="overig">Overig</option>
                    </select>
                    <input class="btn btn-default" type="submit" value="Zoek">
                </form>
            </div>


            <ul class="nav navbar-nav navbar-right">
                <li><a href="login.php" target="_blank">Login/Registreer</a></li>
            </ul>

        </div>
    </div>
</div>

<main>
    <div class="row">
        <div class="col-sm-6">
            <div class="profiel">
                <div class="profiel-body">
                    <div class="profiel-titel">
                        <h3 class="content-title">KarelAppel66</h3>
                    </div>
                    <div class="profiel-infobox">
                        <div class="gebruiker-infobox-seg">
                            <p class="color-blue">E-mail</p>
                            <p>karelappelisgroot@hotmail.com</p>
                        </div>
                        <div class="gebruiker-infobox-seg">
                            <p class="color-blue">Telefoon</p>
                            <p>0644444444</p>
                            <p>04875132229</p>
                        </div>
                        <div class="gebruiker-infobox-seg">
                            <p class="color-blue">Woonplaats</p>
                            <p>Druten, Nederland</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="feedback">
                <div class="feedback-body">
                    <div class="feedback-titel">
                        <h3>Feedback</h3>
                    </div>
                    <div class="feedback-list">
                        <table class="table table-striped table-scroll">
                            <thead>
                            <tr>
                                <th>Gebruiker</th>
                                <th>Bericht</th>
                                <th>Score</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="filterable-cell">Karelpiet</td>
                                <td class="filterable-cell">Zeer goeie verkoper!</td>
                                <td class="filterable-cell">9</td>
                            </tr>
                            <tr>
                                <td class="filterable-cell">Klompenboer</td>
                                <td class="filterable-cell">Late bezorging, zeer ontevreden.</td>
                                <td class="filterable-cell">3</td>
                            </tr>
                            <tr>
                                <td class="filterable-cell">Poolas</td>
                                <td class="filterable-cell">Prima kel, jatoch.</td>
                                <td class="filterable-cell">7</td>
                            </tr>
                            <tr>
                                <td class="filterable-cell">Jshajas</td>
                                <td class="filterable-cell">Goeie prijs!</td>
                                <td class="filterable-cell">8</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="feedback-form">
                        <form id="feedback">
                            <div class="feedback-bericht">
                                <p class="color-blue">Geef hier je feedback</p>
                                <textarea name="bericht"></textarea>
                            </div>
                            <div class="feedback-score">
                                <p class="color-blue">Score</p>
                                <input type="number" name="score" max="10" min="1">
                            </div>
                            <input type="submit" value="Opslaan" id="opslaan">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="advertenties">
                <div class="advertenties-body">
                    <h3 class="content-title">Advertenties van Karelappel66</h3>
                    <div class="advertentie-item">
                        <div class="advertentie-item-body col-md-11">
                            <a class="showcase-item-clickable-container" href="#">
                                <figure><img src="img/uitlaat.jpg" width="100" height="100"></figure>
                                <div class="advertentie-item-info">
                                    <p>Uitlaat voor sportauto</p>
                                    <p class="info-text">Zeer mooie uitlaat voor een sportauto. Lekker goedkoop prijsje! Als je meer wilt weten over dit product dan kan je me altijd bereiken via de e-mail. Karel.</p>
                                </div>
                                <div class="advertentie-item-prijs">
                                    <p>Huidige bod: € 12,55</p>
                                </div>
                            </a>
                        </div>
                        <div class="advertentie-item-body col-md-11">
                            <a class="showcase-item-clickable-container" href="#">
                                <figure><img src="img/uitlaat2.jpg" width="100" height="100"></figure>
                                <div class="advertentie-item-info">
                                    <p>Grote uitlaat voor auto</p>
                                    <p class="info-text">Dit is een grote uitlaat voor een grote auto. Wil je meer info neem dan ff contact met me op. Karel.</p>
                                </div>
                                <div class="advertentie-item-prijs">
                                    <p>Huidige bod: € 35,21</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<footer>
    <div>
        <a href="#">Over Ons</a>
        <span>|</span>
        <a href="#">Werken bij</a>
    </div>
    <p class="disclaimer">EenmaalAndermaal is niet aanspraakelijk voor (gevolg)schade die voorkomt uit het gebruik van deze site, dan wel uit fouten of ontbrekende functionaliteiten op deze site.</p>
    <p class="copyright">Copyright &copy; 2016 EenmaalAndermaal. Alle rechten voorbehouden.</p>
</footer>

<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>