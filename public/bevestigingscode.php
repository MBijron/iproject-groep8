<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Registreren verkoper</title>

    <link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body>

<body>
<!-- Nieuwe navbar -->
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a href="../" class="navbar-brand"><img src="img/hamer2.png" width="25" height="25">EenmaalAndermaal</a>
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
            <div class="nav navbar-nav">
                <form class="navbar-form" role="search">
                    <input type="text" class="form-control" placeholder="Zoekterm" name="zoekterm" id="zoekterm">
                    <select name="rubriek" id="rubriek" class="form-control">
                        <option value="kleding">Kleding</option>
                        <option value="autoonderdelen">Auto-Onderdelen</option>
                        <option value="overig">Overig</option>
                    </select>
                    <input class="btn btn-default" type="submit" value="Zoek">
                </form>
            </div>


            <ul class="nav navbar-nav navbar-right">
                <li><a href="login.php" target="_blank">Login/Registreer</a></li>
            </ul>

        </div>
    </div>
</div>

<main>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <form class="bevestiging">
                    <h2 class="form-signin-heading content-title">Upgraden naar verkoopaccount</h2>
                    <div class="bevestiging-info">
                        <p>Hieronder kunt u de bevestigingscode invullen die u heeft ontvangen via de post.</p>
                        <p>Let erop! Deze code is maar een week geldig, dus na deze periode kan deze niet meer gebruikt worden.</p>
                        <div class="form-group row">
                            <label for="inputBevestiging" class="col-md-2 col-form-label">Bevestigingscode</label>
                            <div class="col-md-4">
                                <input class="form-control" type="text" id="inputBevestiging"/>
                            </div>
                        </div>
                    </div>
                    <input class="btn btn-lg btn-primary" type="submit" value="Account activeren">
                </form>
            </div>
        </div>
    </div> <!-- /container -->
</main>

<footer>
    <div>
        <a href="#">Over Ons</a>
        <span>|</span>
        <a href="#">Werken bij</a>
    </div>
    <p class="disclaimer">EenmaalAndermaal is niet aanspraakelijk voor (gevolg)schade die voorkomt uit het gebruik van deze site, dan wel uit fouten of ontbrekende functionaliteiten op deze site.</p>
    <p class="copyright">Copyright &copy; 2016 EenmaalAndermaal. Alle rechten voorbehouden.</p>
</footer>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>


</body>
</body>
</html>