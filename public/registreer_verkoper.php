<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Registreren verkoper</title>

    <link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body>

<body>
<!-- Nieuwe navbar -->
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a href="../" class="navbar-brand"><img src="img/hamer2.png" width="25" height="25">EenmaalAndermaal</a>
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
            <div class="nav navbar-nav">
                <form class="navbar-form" role="search">
                    <input type="text" class="form-control" placeholder="Zoekterm" name="zoekterm" id="zoekterm">
                    <select name="rubriek" id="rubriek" class="form-control">
                        <option value="kleding">Kleding</option>
                        <option value="autoonderdelen">Auto-Onderdelen</option>
                        <option value="overig">Overig</option>
                    </select>
                    <input class="btn btn-default" type="submit" value="Zoek">
                </form>
            </div>


            <ul class="nav navbar-nav navbar-right">
                <li><a href="login.php" target="_blank">Login/Registreer</a></li>
            </ul>

        </div>
    </div>
</div>

	<main>
		<div class="container">
			<form class="registreerverkoper">
				<h2 class="form-signin-heading content-title">Upgraden naar verkoopaccount</h2>
				<p>Als u wilt upgraden naar een verkoopaccount moet er eerst een extra verificatie stap doorlopen worden. Deze stap kan gedaan worden via creditcard of via post. Vul de onderstaande velden in. U bent verplicht een bankrekening of een creditcardnummer in te vullen.</p>
				<div class="form-group row">
				    <label for="inputBank" class="col-md-1 col-form-label">Bank</label>
				    <div class="col-md-5">
				        <input class="form-control" type="text" id="inputBank"/>
				    </div>
				    <label for="inputRekeningnummer" class="col-md-2 col-form-label">Rekeningnummer</label>
				    <div class="col-md-4">
				        <input class="form-control" type="text" id="inputRekeningnummer"/>
				    </div>
				</div>
				<div class="form-group row">
				    <label class="col-md-2 col-form-label">Controle via</label>
				    <label for="inputCreditcard" class="col-md-2 labelright">Creditcard</label>
				    <div class="col-md-2 radiobutton">
				        <input type="radio" name="keuze" id="inputCreditcard" class="form-control" value="creditcard"/>
				    </div>
				    <label for="inputPost" class="col-md-2 labelright">Post</label>
				    <div class="col-md-2 radiobutton">
				        <input type="radio" name="keuze" id="inputPost" class="form-control" value="post" />
				    </div>
				</div>
				<div class="form-group row">
				    <label for="inputCreditcardnummer" class="col-md-2 col-form-label">Creditcardnummer</label>
                    <div class="col-md-6">
				        <input class="form-control" type="text" id="inputCreditcardnummer"/>
				    </div>
				</div>
				<button class="btn btn-primary btn-block" type="submit">Aanvraag verzenden</button>
			</form>
		</div> <!-- /container -->
	</main>

    <footer>
        <div>
            <a href="#">Over Ons</a>
            <span>|</span>
            <a href="#">Werken bij</a>
        </div>
        <p class="disclaimer">EenmaalAndermaal is niet aanspraakelijk voor (gevolg)schade die voorkomt uit het gebruik van deze site, dan wel uit fouten of ontbrekende functionaliteiten op deze site.</p>
        <p class="copyright">Copyright &copy; 2016 EenmaalAndermaal. Alle rechten voorbehouden.</p>
    </footer>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  

</body>
</body>
</html>