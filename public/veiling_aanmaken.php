<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Aanmaken veiling</title>

    <link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body>

<body>
<!-- Nieuwe navbar -->
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a href="../" class="navbar-brand"><img src="img/hamer2.png" width="25" height="25">EenmaalAndermaal</a>
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
            <div class="nav navbar-nav">
                <form class="navbar-form" role="search">
                    <input type="text" class="form-control" placeholder="Zoekterm" name="zoekterm" id="zoekterm">
                    <select name="rubriek" id="rubriek" class="form-control">
                        <option value="kleding">Kleding</option>
                        <option value="autoonderdelen">Auto-Onderdelen</option>
                        <option value="overig">Overig</option>
                    </select>
                    <input class="btn btn-default" type="submit" value="Zoek">
                </form>
            </div>


            <ul class="nav navbar-nav navbar-right">
                <li><a href="login.php" target="_blank">Login/Registreer</a></li>
            </ul>

        </div>
    </div>
</div>

	<main>
		<div class="container">
			<form class="veilingaanmaken">
				<h2 class="form-signin-heading content-title">Veiling aanmaken</h2>
				<div class="form-group row">
				    <label for="inputRubriek1" class="col-md-2 col-form-label">Rubriek 1</label>
                    <div class="col-md-4">
				        <select class="form-control" id="inputRubriek1" required>
                            <option value="Kleding">Kleding</option>
					        <option value="Huishoudemiddelen">Huishoudmiddelen</option>
					        <option value="Auto's">Auto's</option>
				        </select>
				    </div>
				    <label for="inputRubriek2" class="col-md-2 col-form-label">Rubriek 2</label>
				    <div class="col-md-4">
				        <select class="form-control" id="inputRubriek2" required>
					        <option value="Kleding">Kleding</option>
					        <option value="Huishoudemiddelen">Huishoudmiddelen</option>
					        <option value="Auto's">Auto's</option>
				        </select>
				    </div>
				</div>

				<div class="form-group row">
				    <label for="inputTitel" class="col-md-2 col-form-label">Titel</label>
                    <div class="col-md-10">
				        <input class="form-control" type="text" id="inputTitel"/>
                    </div>
				</div>

				<div class="form-group row">
				    <label for="inputStartprijs" class="col-md-2 col-form-label">Startprijs</label>
				    <div class="col-sm-4">
					    <div class="form-row">
					        <div class="input-group">
					            <span class="input-group-addon">&euro;</span>
					            <input type="number" value="0" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" id="c2" />
					        </div>
				        </div>
				    </div>
				    <div class="row">
				        <label for="inputLooptijd" class="col-md-2 col-form-label">Looptijd</label>
				        <div class="col-md-2">
				            <input class="form-control" type="number" id="inputLooptijd"/>
                        </div>
				        <label for="inputLooptijd" class="col-md-2 col-form-label">dagen</label>
				    </div>
				</div>

				<div class="form-group row">
				    <label for="inputPlaats" class="col-md-2 col-form-label">Plaats</label>
				    <div class="col-md-4">
				        <input class="form-control" type="text" id="inputPlaats"/>
				    </div>
				    <label for="inputLand" class="col-md-2 col-form-label">Land</label>
				    <div class="col-md-4">
				        <input class="form-control" type="text" id="inputLand"/>
				    </div>
				</div>
				
				<div class="form-group row">
				    <label for="inputBetalingswijze" class="col-md-2 col-form-label">Betalingswijze</label>
				    <div class="col-md-4">
				        <select class="form-control" id="inputBetalingswijze" required>
					        <option value="Kleding">Contant</option>
					        <option value="Huishoudemiddelen">IDEAL</option>
					        <option value="Auto's">Creditcard</option>
				        </select>
				    </div>
				    <label for="inputBetalingsinstructie" class="col-md-2 col-form-label">Betalingsinstructie</label>
				    <div class="col-md-4">
				        <input class="form-control" type="text" id="inputBetalingsinstructie"/>
				    </div>
				</div>
				<div class="form-group row">
				    <label for="inputVerzendkosten" class="col-md-2 col-form-label">Verzendkosten</label>
                    <div class="col-md-4">
				        <input class="form-control" type="text" id="inputVerzendkosten"/>
				    </div>
				    <label for="inputVerzendinstructie" class="col-md-2 col-form-label">Verzendinstructie</label>
				    <div class="col-md-4">
				        <input class="form-control" type="text" id="inputVerzendinstructie"/>
				    </div>
				</div>
				<div class="form-group row">
				    <label for="inputBeschrijving" class="col-md-2 col-form-label">Beschrijving</label>
				    <div class="col-md-10">
				        <textarea rows="4" placeholder="Beschrijving" required class="form-control"></textarea>
				    </div>
				</div>
				<button class="btn btn-primary btn-block" type="submit">Veiling aanmaken</button>
			</form>
		</div> <!-- /container -->
	</main>

    <footer>
        <div>
            <a href="#">Over Ons</a>
            <span>|</span>
            <a href="#">Werken bij</a>
        </div>
        <p class="disclaimer">EenmaalAndermaal is niet aanspraakelijk voor (gevolg)schade die voorkomt uit het gebruik van deze site, dan wel uit fouten of ontbrekende functionaliteiten op deze site.</p>
        <p class="copyright">Copyright &copy; 2016 EenmaalAndermaal. Alle rechten voorbehouden.</p>
    </footer>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  

</body>
</body>
</html>