<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Homepage</title>

    <link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body>

<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a href="../" class="navbar-brand"><img src="img/hamer2.png" width="25" height="25">EenmaalAndermaal</a>
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
            <div class="nav navbar-nav">
                <form class="navbar-form" role="search">
                    <input type="text" class="form-control" placeholder="Zoekterm" name="zoekterm" id="zoekterm">
                    <select name="rubriek" id="rubriek" class="form-control">
                        <option value="kleding">Kleding</option>
                        <option value="autoonderdelen">Auto-Onderdelen</option>
                        <option value="overig">Overig</option>
                    </select>
                    <input class="btn btn-default" type="submit" value="Zoek">
                </form>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="login.php" target="_blank">Login/Registreer</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
	<main>
		<div class="row">
			<div class="list-group col-md-3"><!-- Dit is de verticale menubalk!-->
				<a href="#" class="list-group-item active">Rubrieken</a>
				<a href="#" class="list-group-item">Kleding</a>
				<a href="#" class="list-group-item">Huishoudmiddelen</a>
				<a href="#" class="list-group-item">Auto's</a>
			</div>
			<div class="col-md-9 content"><!-- Hier zijn de rubrieken! -->
					<div class="image-group">
						<h4><div class="rubriektitel"><a href="#nieuwenpopulair" class="content-title">Nieuw en populair</a></div></h4>
						<div class="row">
							<div class="col-sm-3 ">
								<div class="showcase-item-body">
									<a class="showcase-item-clickable-container" href="#">
										<figure><img src="img/uitlaat.jpg" width="200" height="200"></figure>
										<div class="showcase-item-card-content">
											<h5 class="showcase-item-title">Mooie sportuitlaat</h5>
											<span class="prijs">Huidig bod: € 35,00</span><br>
											<span class="info">Bieding eindigt: 30-11-2016</span>
											<button class="btn-block btn btn-orange">Bied mee</button>
										</div>
									</a>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="showcase-item-body">
									<a class="showcase-item-clickable-container" href="#">
										<figure><img src="img/uitlaat2.jpg" width="200" height="200"></figure>
										<div class="showcase-item-card-content">
											<h5 class="showcase-item-title">Grote uitlaat voor autooooooo</h5>
											<span class="prijs">Huidig bod: € 52,00</span><br>
											<span class="info">Bieding eindigt: 30-11-2016</span>
											<button class="btn-block btn btn-orange">Bied mee</button>
										</div>
									</a>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="showcase-item-body">
									<a class="showcase-item-clickable-container" href="#">
										<figure><img src="img/shirt1.jpg" width="200" height="200"></figure>
										<div class="showcase-item-card-content">
											<h5 class="showcase-item-title">Shirt voor mannen</h5>
											<span class="prijs">Huidig bod: € 9,00</span><br>
											<span class="info">Bieding eindigt: 30-11-2016</span>
											<button class="btn-block btn btn-orange">Bied mee</button>
										</div>
									</a>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="showcase-item-body">
									<a class="showcase-item-clickable-container" href="#">
										<figure><img src="img/shirt3.jpg" width="200" height="200"></figure>
										<div class="showcase-item-card-content">
											<h5 class="showcase-item-title">Shirt voor vrouwen</h5>
											<span class="prijs">Huidig bod: € 17,00</span><br>
											<span class="info">Bieding eindigt: 30-11-2016</span>
											<button class="btn-block btn btn-orange">Bied mee</button>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="image-group clear">
						<h4><div class="rubriektitel"><a href="#kleding" class="content-title">Kleding</a></div></h4>
						<div class="row">
							<div class="col-sm-3">
								<div class="showcase-item-body">
									<a class="showcase-item-clickable-container" href="#">
										<figure><img src="img/shirt2.jpg" width="200" height="200"></figure>
										<div class="showcase-item-card-content">
											<h5 class="showcase-item-title">Mooi shirt voor de mannen</h5>
											<span class="prijs">Huidig bod: € 20,00</span><br>
											<span class="info">Bieding eindigt: 30-11-2016</span>
											<button class="btn-block btn btn-orange">Bied mee</button>
										</div>
									</a>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="showcase-item-body">
									<a class="showcase-item-clickable-container" href="#">
										<figure><img src="img/broek1.jpg" width="200" height="200"></figure>
										<div class="showcase-item-card-content">
											<h5 class="showcase-item-title">lekker broekie</h5>
											<span class="prijs">Huidig bod: € 102,00</span><br>
											<span class="info">Bieding eindigt: 30-11-2016</span>
											<button class="btn-block btn btn-orange">Bied mee</button>
										</div>
									</a>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="showcase-item-body">
									<a class="showcase-item-clickable-container" href="#">
										<figure><img src="img/shirt1.jpg" width="200" height="200"></figure>
										<div class="showcase-item-card-content">
											<h5 class="showcase-item-title">Shirt voor mannen</h5>
											<span class="prijs">Huidig bod: € 9,00</span><br>
											<span class="info">Bieding eindigt: 30-11-2016</span>
											<button class="btn-block btn btn-orange">Bied mee</button>
										</div>
									</a>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="showcase-item-body">
									<a class="showcase-item-clickable-container" href="#">
										<figure><img src="img/shirt3.jpg" width="200" height="200"></figure>
										<div class="showcase-item-card-content">
											<h5 class="showcase-item-title">Shirt voor vrouwen</h5>
											<span class="prijs">Huidig bod: € 17,00</span><br>
											<span class="info">Bieding eindigt: 30-11-2016</span>
											<button class="btn-block btn btn-orange">Bied mee</button>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</main>
</div>
<footer>
	<div class="container">
		<a href="#">Over Ons</a>
		<span>|</span>
		<a href="#">Werken bij</a>
		<p class="disclaimer">EenmaalAndermaal is niet aanspraakelijk voor (gevolg)schade die voorkomt uit het gebruik van deze site, dan wel uit fouten of ontbrekende functionaliteiten op deze site.</p>
		<p class="copyright">Copyright &copy; 2016 EenmaalAndermaal. Alle rechten voorbehouden.</p>
	</div>
</footer>
		<div class="container">
		</div>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>