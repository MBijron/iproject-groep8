<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Login/Registreren</title>

    <link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body>
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a href="../" class="navbar-brand"><img src="img/hamer2.png" width="25" height="25">EenmaalAndermaal</a>
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
            <div class="nav navbar-nav">
                <form class="navbar-form" role="search">
                    <input type="text" class="form-control" placeholder="Zoekterm" name="zoekterm" id="zoekterm">
                    <select name="rubriek" id="rubriek" class="form-control">
                        <option value="kleding">Kleding</option>
                        <option value="autoonderdelen">Auto-Onderdelen</option>
                        <option value="overig">Overig</option>
                    </select>
                    <input class="btn btn-default" type="submit" value="Zoek">
                </form>
            </div>


            <ul class="nav navbar-nav navbar-right">
                <li><a href="login.php" target="_blank">Login/Registreer</a></li>
            </ul>

        </div>
    </div>
</div>

<main>
    <div class="row">
        <div class="col-sm-6">
            <div class="voorwerp">
                <div class="voorwerp-body">
                    <div class="voorwerp-titel">
                    <h3 class="content-title">Mooie kleding te koop voor iedereen</h3>
                </div>
                <div class="voorwerp-rubriek">
                    <a href="#">Kleding</a> > <a href="#">Overig</a>
                </div>
                <div class="voorwerp-afbeeldingen">
                    <div class="voorwerp-afbeeldingen-main">
                        <figure><img src="img/shirt2.jpg" width="200" height="200"></figure>
                    </div>
                    <div class="voorwerpafbeeldingen-thumb">
                        <div class="thumb-placeholder">
                            <figure><img src="img/shirt2.jpg" width="100" height="100"></figure>
                        </div>
                        <div class="thumb-placeholder">
                            <figure><img src="img/shirt1.jpg" width="100" height="100"></figure>
                        </div>
                        <div class="thumb-placeholder">
                            <figure><img src="img/broek1.jpg" width="100" height="100"></figure>
                        </div>
                        <div class="thumb-placeholder">
                            <figure><img src="img/shirt3.jpg" width="100" height="100"></figure>
                        </div>
                    </div>
                </div>
                <div class="voorwerp-info">
                    <div class="voorwerp-info-beschrijvingtitel clear">
                        <h4 class="content-title">Beschrijving</h4>
                    </div>
                    <div class="voorwerp-info-beschrijving">
                        <p>Ik heb hier een aantal mooie shirts en broeken te koop. Somminge zijn voor mannen sommige vrouwen, zoals te zien op de foto's. Ze zijn in alle maten verkrijgbaar. Je kunt ook contact opnemen met me als je meer informatie wilt hebben. Als je geïntresseerd bent graag bieden!</p>
                    </div>
                    <div class="voorwerp-info-betaling">
                        <p>Betalingswijze : Contant</p>
                        <p>Betalingsinstructie: Bij het ophalen betalen</p>
                    </div>
                    <div class="voorwerp-info-verzending">
                        <p>Verzendwijze: Ophalen</p>
                        <p>Verzendinstructie: </p>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="gebruiker">
                <div class="gebruiker-body">
                    <div class="gebruiker-body-titel">
                        <h4 class="content-title">Gebruikersinformatie</h4>
                    </div>
                    <div class="gebruiker-body-infobox">
                        <div class="gebruiker-infobox-seg">
                            <p class="color-blue">Verkoper</p>
                            <p>Jan Karel</p>
                        </div>
                        <div class="gebruiker-infobox-seg">
                            <p class="color-blue">E-mail</p>
                            <p>jankarel@hotmail.com</p>
                        </div>
                        <div class="gebruiker-infobox-seg">
                            <p class="color-blue">Telefoon</p>
                            <p>0644444444</p>
                            <p>04875132229</p>
                        </div>
                        <div class="gebruiker-infobox-seg">
                            <p class="color-blue">Voorwerplokatie</p>
                            <p>Druten, Nederland</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="biedingen">
                <div class="biedingen-body">
                    <div class="biedingen-body-titel">
                        <h4 class="content-title">Bieden</h4>
                    </div>
                    <div class="biedingen-body-input">
                        <form>
                            <div class="biedingen-input currency"><input type="number" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100"  placeholder="Plaats hier uw bod"></div>
                            <div><input type="submit" value="Plaats bod"></div>
                        </form>
                    </div>
                    <div class="biedingen-body-sluiting">
                        <p>Deze veiling eindigt op: 30/11/2016 13:22:00</p>
                    </div>
                    <div class="biedingen-body-list">
                        <table class="table table-striped table-scroll">
                            <thead>
                            <tr>
                                <th>Gebruiker</th>
                                <th>Bod</th>
                                <th>Datum</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="filterable-cell">Karelpiet</td>
                                <td class="filterable-cell">€ 13,00</td>
                                <td class="filterable-cell">30/11/2016 11:20:66</td>
                            </tr>
                            <tr>
                                <td class="filterable-cell">Appeltjeei</td>
                                <td class="filterable-cell">€ 11,00</td>
                                <td class="filterable-cell">30/11/2016 09:55:16</td>
                            </tr>
                            <tr>
                                <td class="filterable-cell">Poolas</td>
                                <td class="filterable-cell">€ 5,00</td>
                                <td class="filterable-cell">29/11/2016 19:11:33</td>
                            </tr>
                            <tr>
                                <td class="filterable-cell">Poasodosa</td>
                                <td class="filterable-cell">€ 2,00</td>
                                <td class="filterable-cell">29/11/2016 14:11:33</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<footer>
    <div>
        <a href="#">Over Ons</a>
        <span>|</span>
        <a href="#">Werken bij</a>
    </div>
    <p class="disclaimer">EenmaalAndermaal is niet aanspraakelijk voor (gevolg)schade die voorkomt uit het gebruik van deze site, dan wel uit fouten of ontbrekende functionaliteiten op deze site.</p>
    <p class="copyright">Copyright &copy; 2016 EenmaalAndermaal. Alle rechten voorbehouden.</p>
</footer>

<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>