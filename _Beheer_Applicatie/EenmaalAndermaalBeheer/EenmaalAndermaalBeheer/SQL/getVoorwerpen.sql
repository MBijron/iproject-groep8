SELECT
VOORWERP.*,
GEBRUIKER.gebruikerID AS Gebruiker_gebruikerID,
GEBRUIKER.vraagID AS Gebruiker_vraagID,
GEBRUIKER.gebruikersnaam AS Gebruiker_gebruikersnaam,
GEBRUIKER.achternaam AS Gebruiker_achternaam,
GEBRUIKER.voornaam AS Gebruiker_voornaam,
GEBRUIKER.adres AS Gebruiker_adres,
GEBRUIKER.antwoordtekst AS Gebruiker_antwoordtekst,
GEBRUIKER.datum AS Gebruiker_datum,
GEBRUIKER.email AS Gebruiker_email,
GEBRUIKER.landnaam AS Gebruiker_landnaam,
GEBRUIKER.plaatsnaam AS Gebruiker_plaatsnaam,
GEBRUIKER.postcode AS Gebruiker_postcode,
GEBRUIKER.password AS Gebruiker_password,
GEBRUIKER.verkoper AS Gebruiker_verkoper,
RUBRIEK.rubriekID AS Rubrieken_rubriekID,
RUBRIEK.bovenliggendID AS Rubrieken_bovenliggendID,
RUBRIEK.rubrieknaam AS Rubrieken_rubrieknaam,
BESTAND.bestandID AS Bestanden_bestandID,
BESTAND.bestandsnaam AS Bestanden_bestandsnaam,
BESTAND.voorwerpID AS Bestanden_voorwerpID,
BOD.bodID AS Boden_bodID,
BOD.gebruikerID AS Boden_gebruikerID,
BOD.voorwerpID AS Boden_voorwerpID,
BOD.bedrag AS Boden_bedrag,
BOD.datum AS Boden_datum,
BOD.tijd AS Boden_tijd
FROM VOORWERP 
INNER JOIN GEBRUIKER ON VOORWERP.gebruikerID=GEBRUIKER.gebruikerID
LEFT JOIN VOORWERP_RUBRIEK ON VOORWERP.voorwerpID=VOORWERP_RUBRIEK.voorwerpID
LEFT JOIN RUBRIEK ON VOORWERP_RUBRIEK.rubriekID=RUBRIEK.rubriekID
LEFT JOIN BESTAND ON VOORWERP.voorwerpID=BESTAND.voorwerpID
LEFT JOIN BOD ON VOORWERP.voorwerpID=BOD.voorwerpID