SELECT 
verkoperID,
verkoper.gebruikerID as gebruikerID,
gebruiker.gebruikerID as gebruiker_gebruikerID,
controleoptie,
banknaam,
creditcardnummer,
rekeningnummer,
vraagID as gebruiker_vraagID,
gebruikersnaam as gebruiker_gebruikersnaam,
achternaam as gebruiker_achternaam,
voornaam as gebruiker_voornaam,
adres as gebruiker_adres,
antwoordtekst as gebruiker_antwoordtekst,
datum as gebruiker_datum,
email as gebruiker_email,
landnaam as gebruiker_landnaam,
plaatsnaam as gebruiker_plaatsnaam,
postcode as gebruiker_postcode,
verkoper as gebruiker_verkoper 
FROM VERKOPER 
INNER JOIN GEBRUIKER ON GEBRUIKER.gebruikerID=VERKOPER.gebruikerID