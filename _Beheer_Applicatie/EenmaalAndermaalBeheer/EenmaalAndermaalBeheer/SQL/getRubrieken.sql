SELECT RUBRIEK.rubriekID, 
RUBRIEK.bovenliggendID, 
RUBRIEK.rubrieknaam, 
PARENT.rubriekID AS parentRubriek_rubriekID, 
PARENT.bovenliggendID AS parentRubriek_bovenliggendID, 
PARENT.rubrieknaam AS parentRubriek_rubrieknaam 
FROM RUBRIEK 
LEFT JOIN RUBRIEK AS PARENT ON RUBRIEK.bovenliggendID=PARENT.rubriekID