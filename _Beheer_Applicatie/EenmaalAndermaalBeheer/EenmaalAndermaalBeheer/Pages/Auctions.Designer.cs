﻿namespace EenmaalAndermaalBeheer.Pages
{
    partial class Auctions
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labeledInput1 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput2 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput3 = new EenmaalAndermaalBeheer.LabeledInput();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.IDColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TitelColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GebruikersnaamColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RubriekColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // labeledInput1
            // 
            this.labeledInput1.collectiveSharedId = null;
            this.labeledInput1.collectiveSharedSize = true;
            this.labeledInput1.fixedTextboxWidth = true;
            this.labeledInput1.inputText = "";
            this.labeledInput1.labelText = "Gebruikersnaam";
            this.labeledInput1.Location = new System.Drawing.Point(22, 71);
            this.labeledInput1.Name = "labeledInput1";
            this.labeledInput1.readOnly = false;
            this.labeledInput1.Size = new System.Drawing.Size(254, 20);
            this.labeledInput1.TabIndex = 2;
            this.labeledInput1.textboxWidth = 170;
            // 
            // labeledInput2
            // 
            this.labeledInput2.collectiveSharedId = null;
            this.labeledInput2.collectiveSharedSize = true;
            this.labeledInput2.fixedTextboxWidth = true;
            this.labeledInput2.inputText = "";
            this.labeledInput2.labelText = "Rubriek";
            this.labeledInput2.Location = new System.Drawing.Point(22, 97);
            this.labeledInput2.Name = "labeledInput2";
            this.labeledInput2.readOnly = false;
            this.labeledInput2.Size = new System.Drawing.Size(254, 20);
            this.labeledInput2.TabIndex = 3;
            this.labeledInput2.textboxWidth = 170;
            // 
            // labeledInput3
            // 
            this.labeledInput3.collectiveSharedId = null;
            this.labeledInput3.collectiveSharedSize = true;
            this.labeledInput3.fixedTextboxWidth = true;
            this.labeledInput3.inputText = "";
            this.labeledInput3.labelText = "Titel";
            this.labeledInput3.Location = new System.Drawing.Point(296, 71);
            this.labeledInput3.Name = "labeledInput3";
            this.labeledInput3.readOnly = false;
            this.labeledInput3.Size = new System.Drawing.Size(254, 20);
            this.labeledInput3.TabIndex = 4;
            this.labeledInput3.textboxWidth = 170;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(569, 94);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Zoek";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDColumn,
            this.TitelColumn,
            this.GebruikersnaamColumn,
            this.RubriekColumn});
            this.dataGridView1.Location = new System.Drawing.Point(22, 128);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(622, 307);
            this.dataGridView1.TabIndex = 7;
            // 
            // IDColumn
            // 
            this.IDColumn.HeaderText = "ID";
            this.IDColumn.Name = "IDColumn";
            this.IDColumn.ReadOnly = true;
            // 
            // TitelColumn
            // 
            this.TitelColumn.HeaderText = "Titel";
            this.TitelColumn.Name = "TitelColumn";
            this.TitelColumn.ReadOnly = true;
            // 
            // GebruikersnaamColumn
            // 
            this.GebruikersnaamColumn.HeaderText = "Gebruikersnaam";
            this.GebruikersnaamColumn.Name = "GebruikersnaamColumn";
            this.GebruikersnaamColumn.ReadOnly = true;
            // 
            // RubriekColumn
            // 
            this.RubriekColumn.HeaderText = "Rubriek";
            this.RubriekColumn.Name = "RubriekColumn";
            this.RubriekColumn.ReadOnly = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(569, 441);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "Verder";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Auctions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labeledInput3);
            this.Controls.Add(this.labeledInput2);
            this.Controls.Add(this.labeledInput1);
            this.Name = "Auctions";
            this.Title = "Zoeken veiling";
            this.Controls.SetChildIndex(this.labeledInput1, 0);
            this.Controls.SetChildIndex(this.labeledInput2, 0);
            this.Controls.SetChildIndex(this.labeledInput3, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.dataGridView1, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LabeledInput labeledInput1;
        private LabeledInput labeledInput2;
        private LabeledInput labeledInput3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TitelColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn GebruikersnaamColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn RubriekColumn;
    }
}
