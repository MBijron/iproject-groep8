﻿namespace EenmaalAndermaalBeheer.Pages
{
    partial class Financial
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PlaatsColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AdresColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AchternaamColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VoornaamColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GebruikersnaamColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // PlaatsColumn
            // 
            this.PlaatsColumn.HeaderText = "Plaats";
            this.PlaatsColumn.Name = "PlaatsColumn";
            this.PlaatsColumn.ReadOnly = true;
            // 
            // AdresColumn
            // 
            this.AdresColumn.HeaderText = "Adres";
            this.AdresColumn.Name = "AdresColumn";
            this.AdresColumn.ReadOnly = true;
            // 
            // AchternaamColumn
            // 
            this.AchternaamColumn.HeaderText = "Achternaam";
            this.AchternaamColumn.Name = "AchternaamColumn";
            this.AchternaamColumn.ReadOnly = true;
            // 
            // VoornaamColumn
            // 
            this.VoornaamColumn.HeaderText = "Voornaam";
            this.VoornaamColumn.Name = "VoornaamColumn";
            this.VoornaamColumn.ReadOnly = true;
            // 
            // GebruikersnaamColumn
            // 
            this.GebruikersnaamColumn.HeaderText = "Gebruikersnaam";
            this.GebruikersnaamColumn.Name = "GebruikersnaamColumn";
            this.GebruikersnaamColumn.ReadOnly = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GebruikersnaamColumn,
            this.VoornaamColumn,
            this.AchternaamColumn,
            this.AdresColumn,
            this.PlaatsColumn});
            this.dataGridView1.Location = new System.Drawing.Point(22, 58);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(622, 384);
            this.dataGridView1.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(569, 448);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Afdrukken";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // Financial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Financial";
            this.Title = "Financieel overzicht";
            this.Controls.SetChildIndex(this.dataGridView1, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridViewTextBoxColumn PlaatsColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn AdresColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn AchternaamColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn VoornaamColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn GebruikersnaamColumn;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
    }
}
