﻿namespace EenmaalAndermaalBeheer.Pages
{
    partial class AuctionDetails
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labeledInput1 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput2 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput3 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput4 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput6 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput7 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput8 = new EenmaalAndermaalBeheer.LabeledInput();
            this.label3 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labeledInput1
            // 
            this.labeledInput1.collectiveSharedId = "VeilingOverzichtLabels";
            this.labeledInput1.collectiveSharedSize = true;
            this.labeledInput1.fixedTextboxWidth = true;
            this.labeledInput1.inputText = "";
            this.labeledInput1.labelText = "Gebruikersnaam";
            this.labeledInput1.Location = new System.Drawing.Point(22, 71);
            this.labeledInput1.Name = "labeledInput1";
            this.labeledInput1.readOnly = true;
            this.labeledInput1.Size = new System.Drawing.Size(267, 20);
            this.labeledInput1.TabIndex = 4;
            this.labeledInput1.textboxWidth = 200;
            // 
            // labeledInput2
            // 
            this.labeledInput2.collectiveSharedId = "VeilingOverzichtLabels";
            this.labeledInput2.collectiveSharedSize = true;
            this.labeledInput2.fixedTextboxWidth = true;
            this.labeledInput2.inputText = "";
            this.labeledInput2.labelText = "Titel";
            this.labeledInput2.Location = new System.Drawing.Point(22, 143);
            this.labeledInput2.Name = "labeledInput2";
            this.labeledInput2.readOnly = true;
            this.labeledInput2.Size = new System.Drawing.Size(267, 20);
            this.labeledInput2.TabIndex = 5;
            this.labeledInput2.textboxWidth = 200;
            // 
            // labeledInput3
            // 
            this.labeledInput3.collectiveSharedId = "VeilingOverzichtLabels";
            this.labeledInput3.collectiveSharedSize = true;
            this.labeledInput3.fixedTextboxWidth = true;
            this.labeledInput3.inputText = "";
            this.labeledInput3.labelText = "Plaats";
            this.labeledInput3.Location = new System.Drawing.Point(22, 192);
            this.labeledInput3.Name = "labeledInput3";
            this.labeledInput3.readOnly = true;
            this.labeledInput3.Size = new System.Drawing.Size(267, 20);
            this.labeledInput3.TabIndex = 6;
            this.labeledInput3.textboxWidth = 200;
            // 
            // labeledInput4
            // 
            this.labeledInput4.collectiveSharedId = "VeilingOverzichtLabels";
            this.labeledInput4.collectiveSharedSize = true;
            this.labeledInput4.fixedTextboxWidth = true;
            this.labeledInput4.inputText = "";
            this.labeledInput4.labelText = "Begin veiling";
            this.labeledInput4.Location = new System.Drawing.Point(22, 241);
            this.labeledInput4.Name = "labeledInput4";
            this.labeledInput4.readOnly = true;
            this.labeledInput4.Size = new System.Drawing.Size(267, 20);
            this.labeledInput4.TabIndex = 7;
            this.labeledInput4.textboxWidth = 200;
            // 
            // labeledInput6
            // 
            this.labeledInput6.collectiveSharedId = "VeilingOverzichtLabels";
            this.labeledInput6.collectiveSharedSize = true;
            this.labeledInput6.fixedTextboxWidth = true;
            this.labeledInput6.inputText = "";
            this.labeledInput6.labelText = "Prijs";
            this.labeledInput6.Location = new System.Drawing.Point(365, 143);
            this.labeledInput6.Name = "labeledInput6";
            this.labeledInput6.readOnly = true;
            this.labeledInput6.Size = new System.Drawing.Size(267, 20);
            this.labeledInput6.TabIndex = 9;
            this.labeledInput6.textboxWidth = 200;
            // 
            // labeledInput7
            // 
            this.labeledInput7.collectiveSharedId = "VeilingOverzichtLabels";
            this.labeledInput7.collectiveSharedSize = true;
            this.labeledInput7.fixedTextboxWidth = true;
            this.labeledInput7.inputText = "";
            this.labeledInput7.labelText = "Land";
            this.labeledInput7.Location = new System.Drawing.Point(365, 192);
            this.labeledInput7.Name = "labeledInput7";
            this.labeledInput7.readOnly = true;
            this.labeledInput7.Size = new System.Drawing.Size(267, 20);
            this.labeledInput7.TabIndex = 10;
            this.labeledInput7.textboxWidth = 200;
            // 
            // labeledInput8
            // 
            this.labeledInput8.collectiveSharedId = "VeilingOverzichtLabels";
            this.labeledInput8.collectiveSharedSize = true;
            this.labeledInput8.fixedTextboxWidth = true;
            this.labeledInput8.inputText = "";
            this.labeledInput8.labelText = "Einde veiling";
            this.labeledInput8.Location = new System.Drawing.Point(365, 241);
            this.labeledInput8.Name = "labeledInput8";
            this.labeledInput8.readOnly = true;
            this.labeledInput8.Size = new System.Drawing.Size(267, 20);
            this.labeledInput8.TabIndex = 11;
            this.labeledInput8.textboxWidth = 200;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(57, 291);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Rubriek";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(107, 291);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(199, 121);
            this.listBox1.TabIndex = 14;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(464, 442);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 28;
            this.button2.Text = "Terug";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(563, 442);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 27;
            this.button1.Text = "Opslaan";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(452, 71);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(200, 23);
            this.button3.TabIndex = 29;
            this.button3.Text = "Veiling verwijderen";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(107, 444);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(152, 21);
            this.comboBox1.TabIndex = 30;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(107, 415);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(199, 23);
            this.button4.TabIndex = 31;
            this.button4.Text = "Verwijder";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(265, 443);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(41, 23);
            this.button5.TabIndex = 32;
            this.button5.Text = "Add";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(450, 291);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(202, 23);
            this.button6.TabIndex = 33;
            this.button6.Text = "Beheer boden";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // AuctionDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labeledInput8);
            this.Controls.Add(this.labeledInput7);
            this.Controls.Add(this.labeledInput6);
            this.Controls.Add(this.labeledInput4);
            this.Controls.Add(this.labeledInput3);
            this.Controls.Add(this.labeledInput2);
            this.Controls.Add(this.labeledInput1);
            this.Name = "AuctionDetails";
            this.Title = "Veilingsoverzicht";
            this.Load += new System.EventHandler(this.AuctionDetails_Load);
            this.Controls.SetChildIndex(this.labeledInput1, 0);
            this.Controls.SetChildIndex(this.labeledInput2, 0);
            this.Controls.SetChildIndex(this.labeledInput3, 0);
            this.Controls.SetChildIndex(this.labeledInput4, 0);
            this.Controls.SetChildIndex(this.labeledInput6, 0);
            this.Controls.SetChildIndex(this.labeledInput7, 0);
            this.Controls.SetChildIndex(this.labeledInput8, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.listBox1, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            this.Controls.SetChildIndex(this.button3, 0);
            this.Controls.SetChildIndex(this.comboBox1, 0);
            this.Controls.SetChildIndex(this.button4, 0);
            this.Controls.SetChildIndex(this.button5, 0);
            this.Controls.SetChildIndex(this.button6, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LabeledInput labeledInput1;
        private LabeledInput labeledInput2;
        private LabeledInput labeledInput3;
        private LabeledInput labeledInput4;
        private LabeledInput labeledInput6;
        private LabeledInput labeledInput7;
        private LabeledInput labeledInput8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
    }
}
