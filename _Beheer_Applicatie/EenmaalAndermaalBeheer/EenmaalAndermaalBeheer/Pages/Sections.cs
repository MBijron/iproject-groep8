﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EenmaalAndermaalBeheer.DataTypes;

namespace EenmaalAndermaalBeheer.Pages
{
    public partial class Sections : PageTemplate
    {
        public Sections(DatabaseHandler db, Form1 parent) : base(db, parent)
        {
            InitializeComponent();
        }
        protected override void OnLoad()
        {
            showSections();
        }

        protected override void Reload()
        {
            showSections();
        }

        private void showSections()
        {
            List<Rubriek> rubrieken = db.getRubrieken().OrderBy((a) => a.parentRubriek == null ? "-" : a.parentRubriek.rubrieknaam).ToList();
            DataGridViewRow[] rows = rubrieken.Select((a) => new CustomDataGridViewRow(a.rubriekID, a.rubrieknaam, a.parentRubriek == null ? "-" : a.parentRubriek.rubrieknaam)).ToArray();
            List<string> parentRubrieken = rubrieken.Select((a) => a.rubrieknaam).GroupBy((a) => a).Select((a) => a.First()).ToList();
            parentRubrieken.Insert(0, "-");
            Invoke(new Action(() =>
            {
                dataGridView1.Rows.Clear();
                dataGridView1.Rows.AddRange(rows);
                comboBox1.Items.Clear();
                comboBox1.Items.AddRange(parentRubrieken.ToArray());

            }));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(comboBox1.Text) && !string.IsNullOrEmpty(labeledInput1.inputText))
            {
                Rubriek parentRubriek = db.getRubriekByName(comboBox1.Text);
                if(parentRubriek != null || comboBox1.Text == "-")
                {
                    Rubriek newRubriek = new Rubriek();
                    newRubriek.rubrieknaam = labeledInput1.inputText;
                    if(comboBox1.Text != "-") newRubriek.bovenliggendID = parentRubriek.rubriekID;
                    db.addRubriek(newRubriek);
                    clearFields();
                    Reload();
                }
                else
                {
                    MessageBox.Show("de gekozen directe hoofdrubriek bestaat niet");
                }
            }
            else
            {
                MessageBox.Show("Geef A.U.B een rubrieknaam op en kies een directe hoofdrubriek");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                DataGridViewRow row = senderGrid.Rows[e.RowIndex];
                long id = (long)row.Cells[0].Value;
                string text = row.Cells[1].Value.ToString();
                DialogResult result = MessageBox.Show("Weet u zeker dat u de rubriek: \"" + text + "\" wilt verwijderen?", "Let op!", MessageBoxButtons.OKCancel);
                if (result == DialogResult.OK)
                {
                    db.removeRubriek(new Rubriek() { rubriekID = id });
                    Reload();
                }
            }
        }
    }
}
