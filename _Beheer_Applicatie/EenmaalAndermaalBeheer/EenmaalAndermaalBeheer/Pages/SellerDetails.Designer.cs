﻿namespace EenmaalAndermaalBeheer.Pages
{
    partial class SellerDetails
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.labeledInput7 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput6 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput5 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput4 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput3 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput2 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput1 = new EenmaalAndermaalBeheer.LabeledInput();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.labeledInput8 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput9 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput10 = new EenmaalAndermaalBeheer.LabeledInput();
            this.SuspendLayout();
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(438, 78);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(200, 23);
            this.button3.TabIndex = 27;
            this.button3.Text = "Verwijder verkoper";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(469, 376);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 26;
            this.button2.Text = "Terug";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(563, 376);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 25;
            this.button1.Text = "Opslaan";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // labeledInput7
            // 
            this.labeledInput7.collectiveSharedId = "VerkoperDetailsLabel";
            this.labeledInput7.collectiveSharedSize = true;
            this.labeledInput7.fixedTextboxWidth = true;
            this.labeledInput7.inputText = "";
            this.labeledInput7.labelText = "Land";
            this.labeledInput7.Location = new System.Drawing.Point(346, 232);
            this.labeledInput7.Name = "labeledInput7";
            this.labeledInput7.readOnly = true;
            this.labeledInput7.Size = new System.Drawing.Size(292, 20);
            this.labeledInput7.TabIndex = 24;
            this.labeledInput7.textboxWidth = 200;
            // 
            // labeledInput6
            // 
            this.labeledInput6.collectiveSharedId = "VerkoperDetailsLabel";
            this.labeledInput6.collectiveSharedSize = true;
            this.labeledInput6.fixedTextboxWidth = true;
            this.labeledInput6.inputText = "";
            this.labeledInput6.labelText = "Postcode";
            this.labeledInput6.Location = new System.Drawing.Point(346, 180);
            this.labeledInput6.Name = "labeledInput6";
            this.labeledInput6.readOnly = true;
            this.labeledInput6.Size = new System.Drawing.Size(292, 20);
            this.labeledInput6.TabIndex = 23;
            this.labeledInput6.textboxWidth = 200;
            // 
            // labeledInput5
            // 
            this.labeledInput5.collectiveSharedId = "VerkoperDetailsLabel";
            this.labeledInput5.collectiveSharedSize = true;
            this.labeledInput5.fixedTextboxWidth = true;
            this.labeledInput5.inputText = "";
            this.labeledInput5.labelText = "Achternaam";
            this.labeledInput5.Location = new System.Drawing.Point(346, 128);
            this.labeledInput5.Name = "labeledInput5";
            this.labeledInput5.readOnly = true;
            this.labeledInput5.Size = new System.Drawing.Size(292, 20);
            this.labeledInput5.TabIndex = 22;
            this.labeledInput5.textboxWidth = 200;
            // 
            // labeledInput4
            // 
            this.labeledInput4.collectiveSharedId = "VerkoperDetailsLabel";
            this.labeledInput4.collectiveSharedSize = true;
            this.labeledInput4.fixedTextboxWidth = true;
            this.labeledInput4.inputText = "";
            this.labeledInput4.labelText = "E-mail";
            this.labeledInput4.Location = new System.Drawing.Point(22, 281);
            this.labeledInput4.Name = "labeledInput4";
            this.labeledInput4.readOnly = true;
            this.labeledInput4.Size = new System.Drawing.Size(292, 20);
            this.labeledInput4.TabIndex = 21;
            this.labeledInput4.textboxWidth = 200;
            // 
            // labeledInput3
            // 
            this.labeledInput3.collectiveSharedId = "VerkoperDetailsLabel";
            this.labeledInput3.collectiveSharedSize = true;
            this.labeledInput3.fixedTextboxWidth = true;
            this.labeledInput3.inputText = "";
            this.labeledInput3.labelText = "Plaatsnaam";
            this.labeledInput3.Location = new System.Drawing.Point(22, 232);
            this.labeledInput3.Name = "labeledInput3";
            this.labeledInput3.readOnly = true;
            this.labeledInput3.Size = new System.Drawing.Size(292, 20);
            this.labeledInput3.TabIndex = 20;
            this.labeledInput3.textboxWidth = 200;
            // 
            // labeledInput2
            // 
            this.labeledInput2.collectiveSharedId = "VerkoperDetailsLabel";
            this.labeledInput2.collectiveSharedSize = true;
            this.labeledInput2.fixedTextboxWidth = true;
            this.labeledInput2.inputText = "";
            this.labeledInput2.labelText = "Adres";
            this.labeledInput2.Location = new System.Drawing.Point(22, 180);
            this.labeledInput2.Name = "labeledInput2";
            this.labeledInput2.readOnly = true;
            this.labeledInput2.Size = new System.Drawing.Size(292, 20);
            this.labeledInput2.TabIndex = 19;
            this.labeledInput2.textboxWidth = 200;
            // 
            // labeledInput1
            // 
            this.labeledInput1.collectiveSharedId = "VerkoperDetailsLabel";
            this.labeledInput1.collectiveSharedSize = true;
            this.labeledInput1.fixedTextboxWidth = true;
            this.labeledInput1.inputText = "";
            this.labeledInput1.labelText = "Voornaam";
            this.labeledInput1.Location = new System.Drawing.Point(22, 128);
            this.labeledInput1.Name = "labeledInput1";
            this.labeledInput1.readOnly = true;
            this.labeledInput1.Size = new System.Drawing.Size(292, 20);
            this.labeledInput1.TabIndex = 18;
            this.labeledInput1.textboxWidth = 200;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(114, 80);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(200, 20);
            this.textBox1.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Gebruikersnaam";
            // 
            // labeledInput8
            // 
            this.labeledInput8.collectiveSharedId = "VerkoperDetailsLabel";
            this.labeledInput8.collectiveSharedSize = true;
            this.labeledInput8.fixedTextboxWidth = true;
            this.labeledInput8.inputText = "";
            this.labeledInput8.labelText = "Bank";
            this.labeledInput8.Location = new System.Drawing.Point(346, 281);
            this.labeledInput8.Name = "labeledInput8";
            this.labeledInput8.readOnly = false;
            this.labeledInput8.Size = new System.Drawing.Size(292, 20);
            this.labeledInput8.TabIndex = 29;
            this.labeledInput8.textboxWidth = 200;
            // 
            // labeledInput9
            // 
            this.labeledInput9.collectiveSharedId = "VerkoperDetailsLabel";
            this.labeledInput9.collectiveSharedSize = true;
            this.labeledInput9.fixedTextboxWidth = true;
            this.labeledInput9.inputText = "";
            this.labeledInput9.labelText = "Creditcardnummer";
            this.labeledInput9.Location = new System.Drawing.Point(22, 330);
            this.labeledInput9.Name = "labeledInput9";
            this.labeledInput9.readOnly = false;
            this.labeledInput9.Size = new System.Drawing.Size(292, 20);
            this.labeledInput9.TabIndex = 30;
            this.labeledInput9.textboxWidth = 200;
            // 
            // labeledInput10
            // 
            this.labeledInput10.collectiveSharedId = "VerkoperDetailsLabel";
            this.labeledInput10.collectiveSharedSize = true;
            this.labeledInput10.fixedTextboxWidth = true;
            this.labeledInput10.inputText = "";
            this.labeledInput10.labelText = "Rekeningnummer";
            this.labeledInput10.Location = new System.Drawing.Point(346, 330);
            this.labeledInput10.Name = "labeledInput10";
            this.labeledInput10.readOnly = false;
            this.labeledInput10.Size = new System.Drawing.Size(292, 20);
            this.labeledInput10.TabIndex = 31;
            this.labeledInput10.textboxWidth = 200;
            // 
            // SellerDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labeledInput10);
            this.Controls.Add(this.labeledInput9);
            this.Controls.Add(this.labeledInput8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labeledInput7);
            this.Controls.Add(this.labeledInput6);
            this.Controls.Add(this.labeledInput5);
            this.Controls.Add(this.labeledInput4);
            this.Controls.Add(this.labeledInput3);
            this.Controls.Add(this.labeledInput2);
            this.Controls.Add(this.labeledInput1);
            this.Controls.Add(this.textBox1);
            this.Name = "SellerDetails";
            this.Title = "Verkoper details";
            this.Controls.SetChildIndex(this.textBox1, 0);
            this.Controls.SetChildIndex(this.labeledInput1, 0);
            this.Controls.SetChildIndex(this.labeledInput2, 0);
            this.Controls.SetChildIndex(this.labeledInput3, 0);
            this.Controls.SetChildIndex(this.labeledInput4, 0);
            this.Controls.SetChildIndex(this.labeledInput5, 0);
            this.Controls.SetChildIndex(this.labeledInput6, 0);
            this.Controls.SetChildIndex(this.labeledInput7, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            this.Controls.SetChildIndex(this.button3, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.labeledInput8, 0);
            this.Controls.SetChildIndex(this.labeledInput9, 0);
            this.Controls.SetChildIndex(this.labeledInput10, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private LabeledInput labeledInput7;
        private LabeledInput labeledInput6;
        private LabeledInput labeledInput5;
        private LabeledInput labeledInput4;
        private LabeledInput labeledInput3;
        private LabeledInput labeledInput2;
        private LabeledInput labeledInput1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private LabeledInput labeledInput8;
        private LabeledInput labeledInput9;
        private LabeledInput labeledInput10;
    }
}
