﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using EenmaalAndermaalBeheer.DataTypes;
using EenmaalAndermaalBeheer.SqlManipulation;

namespace EenmaalAndermaalBeheer.Pages
{
    public partial class SellerSearch : PageTemplate
    {
        public SellerSearch(DatabaseHandler db, Form1 parent) : base(db, parent)
        {
            InitializeComponent();
        }
        protected override void OnLoad()
        {
        }

        protected override void Reload()
        {
            doSearch();
        }

        private bool showUsers(ContainsClause[] clauses)
        {
            if (clauses.Length > 0)
            {
                Task.Run(new Action(() =>
                {
                    DataGridViewRow[] rows = db.getVerkopers(clauses).Select((a) => new CustomDataGridViewRow(a.verkoperID, a.Gebruiker.gebruikersnaam, a.Gebruiker.voornaam, a.Gebruiker.achternaam, a.Gebruiker.adres, a.Gebruiker.plaatsnaam)).ToArray();
                    Invoke(new Action(dataGridView1.Rows.Clear));
                    Invoke(new Action(() => dataGridView1.Rows.AddRange(rows)));
                }));
                return true;
            }
            return false;
        }

        private bool doSearch()
        {
            string gebruikersnaam = labeledInput1.inputText;
            string voornaam = labeledInput2.inputText;
            string achternaam = labeledInput4.inputText;
            string adres = labeledInput3.inputText;
            string plaats = labeledInput5.inputText;

            List<ContainsClause> clauses = new List<ContainsClause>();
            if (!string.IsNullOrEmpty(gebruikersnaam)) clauses.Add(new ContainsClause("gebruikersnaam", gebruikersnaam));
            if (!string.IsNullOrEmpty(voornaam)) clauses.Add(new ContainsClause("voornaam", voornaam));
            if (!string.IsNullOrEmpty(achternaam)) clauses.Add(new ContainsClause("achternaam", achternaam));
            if (!string.IsNullOrEmpty(adres)) clauses.Add(new ContainsClause("adres", adres));
            if (!string.IsNullOrEmpty(plaats)) clauses.Add(new ContainsClause("plaatsnaam", plaats));

            return showUsers(clauses.ToArray());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!doSearch()) MessageBox.Show("Vul A.U.B een of meerdere zoekvelden in");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];
                Verkoper user = db.getVerkoper((long)selectedRow.Cells[0].Value);
                SellerDetails details = new SellerDetails(db, parent, user);
                parent.displayControl(details);
            }
            else
            {
                MessageBox.Show("Selecteer A.U.B een verkoper");
            }
        }
    }
}
