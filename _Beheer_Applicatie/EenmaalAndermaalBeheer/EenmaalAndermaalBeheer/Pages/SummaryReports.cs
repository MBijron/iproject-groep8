﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EenmaalAndermaalBeheer.Pages
{
    public partial class SummaryReports : PageTemplate
    {
        public SummaryReports(DatabaseHandler db, Form1 parent) : base(db, parent)
        {
            InitializeComponent();
        }

        protected override void OnLoad()
        {
        }

        protected override void Reload()
        {
        }
    }
}
