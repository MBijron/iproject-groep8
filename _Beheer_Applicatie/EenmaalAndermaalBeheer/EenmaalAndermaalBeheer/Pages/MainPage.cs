﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EenmaalAndermaalBeheer
{
    public partial class MainPage : PageTemplate
    {
        public MainPage(DatabaseHandler db, Form1 parent) : base(db, parent)
        {
            InitializeComponent();
            Enable();
        }
        protected override void OnLoad()
        {
        }

        protected override void Reload()
        {
        }
    }
}
