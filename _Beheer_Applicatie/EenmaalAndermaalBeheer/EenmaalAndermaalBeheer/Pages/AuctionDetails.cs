﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EenmaalAndermaalBeheer.DataTypes;

namespace EenmaalAndermaalBeheer.Pages
{
    public partial class AuctionDetails : PageTemplate
    {
        private Voorwerp voorwerp;
        public AuctionDetails()
        {
            InitializeComponent();
        }

        public AuctionDetails(DatabaseHandler db, Form1 parent, Voorwerp voorwerp) : base(db, parent)
        {
            InitializeComponent();
            this.voorwerp = voorwerp;
        }

        protected override void OnLoad()
        {
            if(voorwerp != null)
            {
                showDetails();
            }
        }

        protected override void Reload()
        {
            if (voorwerp != null)
            {
                voorwerp = db.getVoorwerp(voorwerp.voorwerpID);
                clearFields();
                showDetails();
            }
        }

        private void showDetails()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(showDetails));
            }
            else
            {
                if (voorwerp != null)
                {
                    labeledInput1.inputText = voorwerp.Gebruiker.gebruikersnaam;
                    labeledInput2.inputText = voorwerp.titel;
                    labeledInput3.inputText = voorwerp.Gebruiker.plaatsnaam;
                    labeledInput4.inputText = voorwerp.datum.ToShortDateString();
                    labeledInput6.inputText = voorwerp.startprijs.ToString();
                    labeledInput7.inputText = voorwerp.Gebruiker.landnaam;
                    labeledInput8.inputText = voorwerp.datum.AddDays(voorwerp.dagen).ToShortDateString();
                    showRubrieken();
                    comboBox1.Items.Clear();
                    comboBox1.Items.AddRange(db.getBottomRubrieken().Select((a) => a.rubrieknaam).ToArray());
                }
            }
        }

        private void showRubrieken()
        {
            listBox1.Items.Clear();
            listBox1.Items.AddRange(voorwerp.Rubrieken.Select((a) => a.rubrieknaam).ToArray());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            parent.goBack();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            string name = comboBox1.SelectedItem?.ToString();
            if(name != null)
            {
                Rubriek rubriek = db.getRubriekByName(name);
                if (rubriek != null)
                {
                    voorwerp.Rubrieken.Add(rubriek);
                    showRubrieken();
                }
            }
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string name = listBox1.SelectedItem?.ToString();
            if(name != null)
            {
                Rubriek rubriek = voorwerp.Rubrieken.SingleOrDefault((a) => a.rubrieknaam == name);
                if(rubriek != null)
                {
                    voorwerp.Rubrieken.Remove(rubriek);
                    showRubrieken();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            db.updateVoorwerp(voorwerp);
            Refresh();
        }

        private void AuctionDetails_Load(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            AuctionBiddings details = new AuctionBiddings(db, parent, voorwerp);
            parent.displayControl(details);
        }
    }
}
