﻿namespace EenmaalAndermaalBeheer.Pages
{
    partial class SecurityQuestions
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.VraagnummerColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HerstelvraagColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VerwijderColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.labeledInput1 = new EenmaalAndermaalBeheer.LabeledInput();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VraagnummerColumn,
            this.HerstelvraagColumn,
            this.VerwijderColumn});
            this.dataGridView1.Location = new System.Drawing.Point(22, 56);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(622, 374);
            this.dataGridView1.TabIndex = 9;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // VraagnummerColumn
            // 
            this.VraagnummerColumn.FillWeight = 134.7716F;
            this.VraagnummerColumn.HeaderText = "Vraagnummer";
            this.VraagnummerColumn.Name = "VraagnummerColumn";
            this.VraagnummerColumn.ReadOnly = true;
            this.VraagnummerColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.VraagnummerColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // HerstelvraagColumn
            // 
            this.HerstelvraagColumn.FillWeight = 134.7716F;
            this.HerstelvraagColumn.HeaderText = "Herstelvraag";
            this.HerstelvraagColumn.Name = "HerstelvraagColumn";
            this.HerstelvraagColumn.ReadOnly = true;
            // 
            // VerwijderColumn
            // 
            this.VerwijderColumn.FillWeight = 30.45685F;
            this.VerwijderColumn.HeaderText = "Verwijder";
            this.VerwijderColumn.Name = "VerwijderColumn";
            // 
            // labeledInput1
            // 
            this.labeledInput1.fixedTextboxWidth = false;
            this.labeledInput1.inputText = "";
            this.labeledInput1.labelText = "Herstelvraag";
            this.labeledInput1.Location = new System.Drawing.Point(108, 445);
            this.labeledInput1.Name = "labeledInput1";
            this.labeledInput1.readOnly = false;
            this.labeledInput1.Size = new System.Drawing.Size(319, 20);
            this.labeledInput1.TabIndex = 10;
            this.labeledInput1.textboxWidth = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(433, 445);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "Vraag toevoegen";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // SecurityQuestions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labeledInput1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "SecurityQuestions";
            this.Title = "Herstelvragen";
            this.Load += new System.EventHandler(this.SecurityQuestions_Load);
            this.Controls.SetChildIndex(this.dataGridView1, 0);
            this.Controls.SetChildIndex(this.labeledInput1, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn VraagnummerColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn HerstelvraagColumn;
        private System.Windows.Forms.DataGridViewButtonColumn VerwijderColumn;
        private LabeledInput labeledInput1;
        private System.Windows.Forms.Button button1;
    }
}
