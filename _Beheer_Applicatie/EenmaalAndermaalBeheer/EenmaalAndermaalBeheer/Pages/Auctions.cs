﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EenmaalAndermaalBeheer.DataTypes;
using EenmaalAndermaalBeheer.SqlManipulation;

namespace EenmaalAndermaalBeheer.Pages
{
    public partial class Auctions : PageTemplate
    {
        public Auctions(DatabaseHandler db, Form1 parent) : base(db, parent)
        {
            InitializeComponent();
        }
        protected override void OnLoad()
        {
        }

        protected override void Reload()
        {
            doSearch();
        }

        private bool showUsers(ContainsClause[] clauses)
        {
            if (clauses.Length > 0)
            {
                showLoadingBar();
                Task.Run(new Action(() =>
                {
                    DataGridViewRow[] rows = db.getVoorwerpen(clauses).Select((a) => new CustomDataGridViewRow(a.voorwerpID, a.titel, a.Gebruiker.gebruikersnaam, string.Join(" | ", a.Rubrieken.Select((b) => b.rubrieknaam)))).ToArray();
                    Invoke(new Action(dataGridView1.Rows.Clear));
                    Invoke(new Action(() => dataGridView1.Rows.AddRange(rows)));
                    Invoke(new Action(() => hideLoadingBar()));
                }));
                return true;
            }
            return false;
        }

        private bool doSearch()
        {
            string gebruikersnaam = labeledInput1.inputText;
            string rubriek = labeledInput2.inputText;
            string titel = labeledInput3.inputText;

            List<ContainsClause> clauses = new List<ContainsClause>();
            if (!string.IsNullOrEmpty(gebruikersnaam)) clauses.Add(new ContainsClause("gebruikersnaam", gebruikersnaam));
            if (!string.IsNullOrEmpty(rubriek)) clauses.Add(new ContainsClause("rubrieknaam", rubriek));
            if (!string.IsNullOrEmpty(titel)) clauses.Add(new ContainsClause("titel", titel));

            return showUsers(clauses.ToArray());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //zoek
            if (!doSearch()) MessageBox.Show("Vul A.U.B een of meerdere zoekvelden in");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //verder
            if (dataGridView1.SelectedRows.Count > 0)
            {
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];
                Voorwerp voorwerp = db.getVoorwerp((long)selectedRow.Cells[0].Value);
                AuctionDetails details = new AuctionDetails(db, parent, voorwerp);
                parent.displayControl(details);
            }
            else
            {
                MessageBox.Show("Selecteer A.U.B een voorwerp");
            }
        }

        private void Auctions_Load(object sender, EventArgs e)
        {

        }
    }
}
