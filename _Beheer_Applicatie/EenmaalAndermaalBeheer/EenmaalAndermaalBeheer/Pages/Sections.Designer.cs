﻿namespace EenmaalAndermaalBeheer.Pages
{
    partial class Sections
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.RubrieknummerColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RubrieknaamColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DirecteHoofdrubriekColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VerwijderColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.labeledInput1 = new EenmaalAndermaalBeheer.LabeledInput();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RubrieknummerColumn,
            this.RubrieknaamColumn,
            this.DirecteHoofdrubriekColumn,
            this.VerwijderColumn});
            this.dataGridView1.Location = new System.Drawing.Point(22, 56);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(622, 361);
            this.dataGridView1.TabIndex = 10;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // RubrieknummerColumn
            // 
            this.RubrieknummerColumn.HeaderText = "Rubrieknummer";
            this.RubrieknummerColumn.Name = "RubrieknummerColumn";
            this.RubrieknummerColumn.ReadOnly = true;
            // 
            // RubrieknaamColumn
            // 
            this.RubrieknaamColumn.HeaderText = "Rubrieknaam";
            this.RubrieknaamColumn.Name = "RubrieknaamColumn";
            this.RubrieknaamColumn.ReadOnly = true;
            // 
            // DirecteHoofdrubriekColumn
            // 
            this.DirecteHoofdrubriekColumn.HeaderText = "Directe Hoofdrubriek";
            this.DirecteHoofdrubriekColumn.Name = "DirecteHoofdrubriekColumn";
            this.DirecteHoofdrubriekColumn.ReadOnly = true;
            // 
            // VerwijderColumn
            // 
            this.VerwijderColumn.FillWeight = 30.45685F;
            this.VerwijderColumn.HeaderText = "Verwijder";
            this.VerwijderColumn.Name = "VerwijderColumn";
            // 
            // labeledInput1
            // 
            this.labeledInput1.inputText = "";
            this.labeledInput1.labelText = "Rubrieknaam";
            this.labeledInput1.Location = new System.Drawing.Point(130, 423);
            this.labeledInput1.Name = "labeledInput1";
            this.labeledInput1.Size = new System.Drawing.Size(286, 20);
            this.labeledInput1.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(422, 423);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 46);
            this.button1.TabIndex = 13;
            this.button1.Text = "Rubriek toevoegen";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(200, 448);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(216, 21);
            this.comboBox1.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(97, 451);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Directe hoofdrubriek";
            // 
            // Sections
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labeledInput1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Sections";
            this.Title = "Rubrieken";
            this.Controls.SetChildIndex(this.dataGridView1, 0);
            this.Controls.SetChildIndex(this.labeledInput1, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.comboBox1, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private LabeledInput labeledInput1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RubrieknummerColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn RubrieknaamColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DirecteHoofdrubriekColumn;
        private System.Windows.Forms.DataGridViewButtonColumn VerwijderColumn;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
    }
}
