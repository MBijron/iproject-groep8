﻿namespace EenmaalAndermaalBeheer.Pages
{
    partial class SellerSearch
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labeledInput1 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput2 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput3 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput4 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput5 = new EenmaalAndermaalBeheer.LabeledInput();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.IDColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GebruikersnaamColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VoornaamColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AchternaamColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AdresColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlaatsColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // labeledInput1
            // 
            this.labeledInput1.collectiveSharedId = "ZoekenVerkoperLabel";
            this.labeledInput1.collectiveSharedSize = true;
            this.labeledInput1.fixedTextboxWidth = true;
            this.labeledInput1.inputText = "";
            this.labeledInput1.labelText = "Gebruikersnaam";
            this.labeledInput1.Location = new System.Drawing.Point(34, 56);
            this.labeledInput1.Name = "labeledInput1";
            this.labeledInput1.readOnly = false;
            this.labeledInput1.Size = new System.Drawing.Size(254, 20);
            this.labeledInput1.TabIndex = 0;
            this.labeledInput1.textboxWidth = 170;
            // 
            // labeledInput2
            // 
            this.labeledInput2.collectiveSharedId = "ZoekenVerkoperLabel";
            this.labeledInput2.collectiveSharedSize = true;
            this.labeledInput2.fixedTextboxWidth = true;
            this.labeledInput2.inputText = "";
            this.labeledInput2.labelText = "Voornaam";
            this.labeledInput2.Location = new System.Drawing.Point(34, 82);
            this.labeledInput2.Name = "labeledInput2";
            this.labeledInput2.readOnly = false;
            this.labeledInput2.Size = new System.Drawing.Size(254, 20);
            this.labeledInput2.TabIndex = 1;
            this.labeledInput2.textboxWidth = 170;
            // 
            // labeledInput3
            // 
            this.labeledInput3.collectiveSharedId = "ZoekenVerkoperLabel";
            this.labeledInput3.collectiveSharedSize = true;
            this.labeledInput3.fixedTextboxWidth = true;
            this.labeledInput3.inputText = "";
            this.labeledInput3.labelText = "Adres";
            this.labeledInput3.Location = new System.Drawing.Point(308, 82);
            this.labeledInput3.Name = "labeledInput3";
            this.labeledInput3.readOnly = false;
            this.labeledInput3.Size = new System.Drawing.Size(254, 20);
            this.labeledInput3.TabIndex = 3;
            this.labeledInput3.textboxWidth = 170;
            // 
            // labeledInput4
            // 
            this.labeledInput4.collectiveSharedId = "ZoekenVerkoperLabel";
            this.labeledInput4.collectiveSharedSize = true;
            this.labeledInput4.fixedTextboxWidth = true;
            this.labeledInput4.inputText = "";
            this.labeledInput4.labelText = "Achternaam";
            this.labeledInput4.Location = new System.Drawing.Point(308, 56);
            this.labeledInput4.Name = "labeledInput4";
            this.labeledInput4.readOnly = false;
            this.labeledInput4.Size = new System.Drawing.Size(254, 20);
            this.labeledInput4.TabIndex = 2;
            this.labeledInput4.textboxWidth = 170;
            // 
            // labeledInput5
            // 
            this.labeledInput5.collectiveSharedId = "ZoekenVerkoperLabel";
            this.labeledInput5.collectiveSharedSize = true;
            this.labeledInput5.fixedTextboxWidth = true;
            this.labeledInput5.inputText = "";
            this.labeledInput5.labelText = "Plaats";
            this.labeledInput5.Location = new System.Drawing.Point(308, 107);
            this.labeledInput5.Name = "labeledInput5";
            this.labeledInput5.readOnly = false;
            this.labeledInput5.Size = new System.Drawing.Size(254, 20);
            this.labeledInput5.TabIndex = 4;
            this.labeledInput5.textboxWidth = 170;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(569, 106);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Zoek";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDColumn,
            this.GebruikersnaamColumn,
            this.VoornaamColumn,
            this.AchternaamColumn,
            this.AdresColumn,
            this.PlaatsColumn});
            this.dataGridView1.Location = new System.Drawing.Point(22, 135);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(622, 310);
            this.dataGridView1.TabIndex = 6;
            // 
            // IDColumn
            // 
            this.IDColumn.HeaderText = "ID";
            this.IDColumn.Name = "IDColumn";
            this.IDColumn.ReadOnly = true;
            // 
            // GebruikersnaamColumn
            // 
            this.GebruikersnaamColumn.HeaderText = "Gebruikersnaam";
            this.GebruikersnaamColumn.Name = "GebruikersnaamColumn";
            this.GebruikersnaamColumn.ReadOnly = true;
            // 
            // VoornaamColumn
            // 
            this.VoornaamColumn.HeaderText = "Voornaam";
            this.VoornaamColumn.Name = "VoornaamColumn";
            this.VoornaamColumn.ReadOnly = true;
            // 
            // AchternaamColumn
            // 
            this.AchternaamColumn.HeaderText = "Achternaam";
            this.AchternaamColumn.Name = "AchternaamColumn";
            this.AchternaamColumn.ReadOnly = true;
            // 
            // AdresColumn
            // 
            this.AdresColumn.HeaderText = "Adres";
            this.AdresColumn.Name = "AdresColumn";
            this.AdresColumn.ReadOnly = true;
            // 
            // PlaatsColumn
            // 
            this.PlaatsColumn.HeaderText = "Plaats";
            this.PlaatsColumn.Name = "PlaatsColumn";
            this.PlaatsColumn.ReadOnly = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(569, 451);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "Verder";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // SellerSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labeledInput5);
            this.Controls.Add(this.labeledInput3);
            this.Controls.Add(this.labeledInput4);
            this.Controls.Add(this.labeledInput2);
            this.Controls.Add(this.labeledInput1);
            this.Name = "SellerSearch";
            this.Title = "Zoeken verkoper";
            this.Controls.SetChildIndex(this.labeledInput1, 0);
            this.Controls.SetChildIndex(this.labeledInput2, 0);
            this.Controls.SetChildIndex(this.labeledInput4, 0);
            this.Controls.SetChildIndex(this.labeledInput3, 0);
            this.Controls.SetChildIndex(this.labeledInput5, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.dataGridView1, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LabeledInput labeledInput1;
        private LabeledInput labeledInput2;
        private LabeledInput labeledInput3;
        private LabeledInput labeledInput4;
        private LabeledInput labeledInput5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn GebruikersnaamColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn VoornaamColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn AchternaamColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn AdresColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlaatsColumn;
    }
}
