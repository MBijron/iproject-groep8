﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EenmaalAndermaalBeheer.DataTypes;
using EenmaalAndermaalBeheer.SqlManipulation;

namespace EenmaalAndermaalBeheer.Pages
{
    public partial class UserSearch : PageTemplate
    {
        public UserSearch(DatabaseHandler db, Form1 parent) : base(db, parent)
        {
            InitializeComponent();
        }

        protected override void OnLoad()
        {
        }

        protected override void Reload()
        {
            doSearch();
        }

        private bool showUsers(SqlClause[] clauses)
        {
            if(clauses.Length > 0)
            {
                Task.Run(new Action(() =>
                {
                    DataGridViewRow[] rows = db.getGebruikers(clauses).Select((a) => new CustomDataGridViewRow(a.gebruikerID, a.gebruikersnaam, a.voornaam, a.achternaam, a.adres, a.plaatsnaam)).ToArray();
                    Invoke(new Action(dataGridView1.Rows.Clear));
                    Invoke(new Action(() => dataGridView1.Rows.AddRange(rows)));
                }));
                return true;
            }
            return false;
        }

        private bool doSearch()
        {
            string gebruikersnaam = labeledInput1.inputText;
            string voornaam = labeledInput2.inputText;
            string achternaam = labeledInput4.inputText;
            string adres = labeledInput3.inputText;
            string plaats = labeledInput5.inputText;

            List<ContainsClause> clauses = new List<ContainsClause>();
            if (!string.IsNullOrEmpty(gebruikersnaam)) clauses.Add(new ContainsClause("gebruikersnaam", gebruikersnaam));
            if (!string.IsNullOrEmpty(voornaam)) clauses.Add(new ContainsClause("voornaam", voornaam));
            if (!string.IsNullOrEmpty(achternaam)) clauses.Add(new ContainsClause("achternaam", achternaam));
            if (!string.IsNullOrEmpty(adres)) clauses.Add(new ContainsClause("adres", adres));
            if (!string.IsNullOrEmpty(plaats)) clauses.Add(new ContainsClause("plaatsnaam", plaats));

            return showUsers(clauses.ToArray());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if( !doSearch() ) MessageBox.Show("Vul A.U.B een of meerdere zoekvelden in");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(dataGridView1.SelectedRows.Count > 0)
            {
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];
                Gebruiker user = db.getGebruiker((long)selectedRow.Cells[0].Value);
                UserDetails details = new UserDetails(db, parent, user);
                parent.displayControl(details);
            }
            else
            {
                MessageBox.Show("Selecteer A.U.B een gebruiker");
            }
        }
    }
}
