﻿namespace EenmaalAndermaalBeheer.Pages
{
    partial class UserDetails
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.labeledInput1 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput2 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput3 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput4 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput5 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput6 = new EenmaalAndermaalBeheer.LabeledInput();
            this.labeledInput7 = new EenmaalAndermaalBeheer.LabeledInput();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Gebruikersnaam";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(113, 85);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(200, 20);
            this.textBox1.TabIndex = 5;
            // 
            // labeledInput1
            // 
            this.labeledInput1.collectiveSharedId = "GebruikesOverzichtLabel";
            this.labeledInput1.collectiveSharedSize = true;
            this.labeledInput1.fixedTextboxWidth = true;
            this.labeledInput1.inputText = "";
            this.labeledInput1.labelText = "Voornaam";
            this.labeledInput1.Location = new System.Drawing.Point(49, 133);
            this.labeledInput1.Name = "labeledInput1";
            this.labeledInput1.readOnly = false;
            this.labeledInput1.Size = new System.Drawing.Size(264, 20);
            this.labeledInput1.TabIndex = 6;
            this.labeledInput1.textboxWidth = 200;
            // 
            // labeledInput2
            // 
            this.labeledInput2.collectiveSharedId = "GebruikesOverzichtLabel";
            this.labeledInput2.collectiveSharedSize = true;
            this.labeledInput2.fixedTextboxWidth = true;
            this.labeledInput2.inputText = "";
            this.labeledInput2.labelText = "Adres";
            this.labeledInput2.Location = new System.Drawing.Point(49, 185);
            this.labeledInput2.Name = "labeledInput2";
            this.labeledInput2.readOnly = false;
            this.labeledInput2.Size = new System.Drawing.Size(264, 20);
            this.labeledInput2.TabIndex = 7;
            this.labeledInput2.textboxWidth = 200;
            // 
            // labeledInput3
            // 
            this.labeledInput3.collectiveSharedId = "GebruikesOverzichtLabel";
            this.labeledInput3.collectiveSharedSize = true;
            this.labeledInput3.fixedTextboxWidth = true;
            this.labeledInput3.inputText = "";
            this.labeledInput3.labelText = "Plaatsnaam";
            this.labeledInput3.Location = new System.Drawing.Point(49, 237);
            this.labeledInput3.Name = "labeledInput3";
            this.labeledInput3.readOnly = false;
            this.labeledInput3.Size = new System.Drawing.Size(264, 20);
            this.labeledInput3.TabIndex = 8;
            this.labeledInput3.textboxWidth = 200;
            // 
            // labeledInput4
            // 
            this.labeledInput4.collectiveSharedId = "GebruikesOverzichtLabel";
            this.labeledInput4.collectiveSharedSize = true;
            this.labeledInput4.fixedTextboxWidth = true;
            this.labeledInput4.inputText = "";
            this.labeledInput4.labelText = "E-mail";
            this.labeledInput4.Location = new System.Drawing.Point(49, 286);
            this.labeledInput4.Name = "labeledInput4";
            this.labeledInput4.readOnly = false;
            this.labeledInput4.Size = new System.Drawing.Size(264, 20);
            this.labeledInput4.TabIndex = 9;
            this.labeledInput4.textboxWidth = 200;
            // 
            // labeledInput5
            // 
            this.labeledInput5.collectiveSharedId = "GebruikesOverzichtLabel";
            this.labeledInput5.collectiveSharedSize = true;
            this.labeledInput5.fixedTextboxWidth = true;
            this.labeledInput5.inputText = "";
            this.labeledInput5.labelText = "Achternaam";
            this.labeledInput5.Location = new System.Drawing.Point(354, 133);
            this.labeledInput5.Name = "labeledInput5";
            this.labeledInput5.readOnly = false;
            this.labeledInput5.Size = new System.Drawing.Size(264, 20);
            this.labeledInput5.TabIndex = 10;
            this.labeledInput5.textboxWidth = 200;
            // 
            // labeledInput6
            // 
            this.labeledInput6.collectiveSharedId = "GebruikesOverzichtLabel";
            this.labeledInput6.collectiveSharedSize = true;
            this.labeledInput6.fixedTextboxWidth = true;
            this.labeledInput6.inputText = "";
            this.labeledInput6.labelText = "Postcode";
            this.labeledInput6.Location = new System.Drawing.Point(354, 185);
            this.labeledInput6.Name = "labeledInput6";
            this.labeledInput6.readOnly = false;
            this.labeledInput6.Size = new System.Drawing.Size(264, 20);
            this.labeledInput6.TabIndex = 11;
            this.labeledInput6.textboxWidth = 200;
            // 
            // labeledInput7
            // 
            this.labeledInput7.collectiveSharedId = "GebruikesOverzichtLabel";
            this.labeledInput7.collectiveSharedSize = true;
            this.labeledInput7.fixedTextboxWidth = true;
            this.labeledInput7.inputText = "";
            this.labeledInput7.labelText = "Land";
            this.labeledInput7.Location = new System.Drawing.Point(354, 237);
            this.labeledInput7.Name = "labeledInput7";
            this.labeledInput7.readOnly = false;
            this.labeledInput7.Size = new System.Drawing.Size(264, 20);
            this.labeledInput7.TabIndex = 12;
            this.labeledInput7.textboxWidth = 200;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(543, 406);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Opslaan";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(448, 406);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "Terug";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(418, 85);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(200, 23);
            this.button3.TabIndex = 16;
            this.button3.Text = "Verwijder gebruiker";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(418, 286);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(200, 95);
            this.listBox1.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(356, 286);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Telefoons";
            // 
            // UserDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labeledInput7);
            this.Controls.Add(this.labeledInput6);
            this.Controls.Add(this.labeledInput5);
            this.Controls.Add(this.labeledInput4);
            this.Controls.Add(this.labeledInput3);
            this.Controls.Add(this.labeledInput2);
            this.Controls.Add(this.labeledInput1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label3);
            this.Name = "UserDetails";
            this.Title = "Gebruikersoverzicht";
            this.Load += new System.EventHandler(this.UserDetails_Load);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.textBox1, 0);
            this.Controls.SetChildIndex(this.labeledInput1, 0);
            this.Controls.SetChildIndex(this.labeledInput2, 0);
            this.Controls.SetChildIndex(this.labeledInput3, 0);
            this.Controls.SetChildIndex(this.labeledInput4, 0);
            this.Controls.SetChildIndex(this.labeledInput5, 0);
            this.Controls.SetChildIndex(this.labeledInput6, 0);
            this.Controls.SetChildIndex(this.labeledInput7, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            this.Controls.SetChildIndex(this.button3, 0);
            this.Controls.SetChildIndex(this.listBox1, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private LabeledInput labeledInput1;
        private LabeledInput labeledInput2;
        private LabeledInput labeledInput3;
        private LabeledInput labeledInput4;
        private LabeledInput labeledInput5;
        private LabeledInput labeledInput6;
        private LabeledInput labeledInput7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label4;
    }
}
