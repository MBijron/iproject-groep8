﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EenmaalAndermaalBeheer.DataTypes;
using System.Diagnostics;

namespace EenmaalAndermaalBeheer.Pages
{
    public partial class SecurityQuestions : PageTemplate
    {
        public SecurityQuestions(DatabaseHandler db, Form1 parent) : base(db, parent)
        {
            InitializeComponent();
        }
        protected override void OnLoad()
        {
            if(db != null)
            {
                DataGridViewRow[] rows = getVragen();
                Invoke(new Action(() => dataGridView1.Rows.AddRange(rows)));
            }



        }

        protected override void Reload()
        {
            if (db != null)
            {
                DataGridViewRow[] rows = getVragen();
                Invoke(new Action(dataGridView1.Rows.Clear));
                Invoke(new Action(() => dataGridView1.Rows.AddRange(rows)));
            }
        }

        private DataGridViewRow[] getVragen()
        {
            return db.getVragen().Select((a) => new CustomDataGridViewRow(a.vraagID, a.tekst)).ToArray();
        }

        private void SecurityQuestions_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(labeledInput1.inputText))
            {
                db.addVraag(new Vraag() { tekst = labeledInput1.inputText });
                labeledInput1.inputText = "";
                Reload();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                DataGridViewRow row = senderGrid.Rows[e.RowIndex];
                long id = (long)row.Cells[0].Value;
                string text = row.Cells[1].Value.ToString();
                DialogResult result = MessageBox.Show("Weet u zeker dat u de vraag: \""+text+"\" wilt verwijderen?", "Let op!", MessageBoxButtons.OKCancel);
                if(result == DialogResult.OK)
                {
                    if(db.vraagHasGebruikers(new Vraag() { vraagID = id, tekst = text }))
                    {
                        MessageBox.Show("deze vraag kan niet worden verwijderd omdat gebruikers deze vraag gebruiken");
                    }
                    else
                    {
                        db.removeVraag(new Vraag() { vraagID = id, tekst = text });
                        Reload();
                    }
                }
            }
        }
    }
}
