﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EenmaalAndermaalBeheer.DataTypes;

namespace EenmaalAndermaalBeheer.Pages
{
    public partial class UserDetails : PageTemplate
    {
        private Gebruiker user;
        public UserDetails()
        {
            InitializeComponent();
        }

        public UserDetails(DatabaseHandler db, Form1 parent, Gebruiker user) : base(db, parent)
        {
            InitializeComponent();
            this.user = user;
        }

        protected override void OnLoad()
        {
            if (user != null)
            {
                showDetails();
            }
        }

        protected override void Reload()
        {
            if(user != null)
            {
                user = db.getGebruiker(user.gebruikerID);
                clearFields();
                showDetails();
            }
        }

        private void showDetails()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(showDetails));
            }
            else
            {
                if (user != null)
                {
                    textBox1.Text = user.gebruikersnaam;
                    labeledInput1.inputText = user.voornaam;
                    labeledInput2.inputText = user.adres;
                    labeledInput3.inputText = user.plaatsnaam;
                    labeledInput4.inputText = user.email;
                    labeledInput5.inputText = user.achternaam;
                    labeledInput6.inputText = user.postcode;
                    labeledInput7.inputText = user.landnaam;
                    listBox1.Items.Clear();
                    listBox1.Items.AddRange(user.GebruikersTelefoons.ToArray());
                }
            }
        }

        private void UserDetails_Load(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            parent.goBack();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(user != null)
            {
                DialogResult result = MessageBox.Show("Weet u zeker dat u de gebruiker wil verwijderen?", "Let op!", MessageBoxButtons.OKCancel);
                if(result == DialogResult.OK)
                {
                    try
                    {
                        db.removeGebruiker(user);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Er is iets mis gegaan met het verwijderen van de gebruiker");
                        throw ex;
                    }
                    finally
                    {
                        parent.goBack();
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Wilt u de informatie van de gebruiker updaten", "Let op!", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                user.voornaam = labeledInput1.inputText;
                user.adres = labeledInput2.inputText;
                user.plaatsnaam = labeledInput3.inputText;
                user.email = labeledInput4.inputText;
                user.achternaam = labeledInput5.inputText;
                user.postcode = labeledInput6.inputText;
                user.landnaam = labeledInput7.inputText;
                user.GebruikersTelefoons = listBox1.Items.Cast<GebruikersTelefoon>().ToList();
                //try
                //{
                    db.updateGebruiker(user);
                //}
                //    catch
                //{
                //    MessageBox.Show("Er is iets mis gegaan met opslaan");
                //}
                //finally
                //{
                //    Reload();
                //}
            }
        }
    }
}
