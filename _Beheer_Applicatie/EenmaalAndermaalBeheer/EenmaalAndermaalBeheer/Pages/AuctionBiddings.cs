﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EenmaalAndermaalBeheer.DataTypes;

namespace EenmaalAndermaalBeheer.Pages
{
    public partial class AuctionBiddings : PageTemplate
    {
        private Voorwerp voorwerp;
        public AuctionBiddings()
        {
            InitializeComponent();
        }

        public AuctionBiddings(DatabaseHandler db, Form1 parent, Voorwerp voorwerp) : base(db, parent)
        {
            InitializeComponent();
            this.voorwerp = voorwerp;
        }

        protected override void Reload()
        {
            voorwerp = db.getVoorwerp(voorwerp.voorwerpID);
            LoadControl(true);
        }

        protected override void OnLoad()
        {
            List<string[]> items = new List<string[]>();
            foreach (Bod bod in voorwerp.Boden)
            {
                items.Add(new string[] { bod.bodID.ToString(), bod.bedrag.ToString(), db.getGebruiker(bod.gebruikerID).gebruikersnaam, bod.datum.ToShortDateString() + " " + bod.tijd.ToString(@"hh\:mm\:ss") });
            }
            Invoke(new Action(() =>
            {
                dataGridView1.Rows.Clear();
                foreach (string[] row in items)
                {
                    dataGridView1.Rows.Add(row);
                }
            }));
        }

        private void AuctionBiddings_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            parent.goBack();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];
                Bod bod = this.voorwerp.Boden.FirstOrDefault(a => a.bodID == Convert.ToInt64(selectedRow.Cells[0].Value.ToString()));
                if(bod != null)
                {
                    DialogResult result = MessageBox.Show("Weet je zeker dat je bod met id:" + bod.bodID + " wil verwijderen?", "EenmaalAndermaal Beheer", MessageBoxButtons.OKCancel);
                    if(result == DialogResult.OK)
                    {
                        db.removeBod(bod);
                        ReloadControl();
                    }
                }
            }
        }
    }
}
