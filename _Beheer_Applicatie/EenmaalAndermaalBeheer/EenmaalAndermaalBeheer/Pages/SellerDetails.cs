﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EenmaalAndermaalBeheer.DataTypes;

namespace EenmaalAndermaalBeheer.Pages
{
    public partial class SellerDetails : PageTemplate
    {
        private Verkoper user;
        public SellerDetails()
        {
            InitializeComponent();
        }

        public SellerDetails(DatabaseHandler db, Form1 parent, Verkoper user) : base(db, parent)
        {
            InitializeComponent();
            this.user = user;
        }

        protected override void OnLoad()
        {
            if (user != null)
            {
                showDetails();
            }
        }

        protected override void Reload()
        {
            if (user != null)
            {
                user = db.getVerkoper(user.verkoperID);
                clearFields();
                showDetails();
            }
        }

        private void showDetails()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(showDetails));
            }
            else
            {
                if (user != null)
                {
                    textBox1.Text = user.Gebruiker.gebruikersnaam;
                    labeledInput1.inputText = user.Gebruiker.voornaam;
                    labeledInput2.inputText = user.Gebruiker.adres;
                    labeledInput3.inputText = user.Gebruiker.plaatsnaam;
                    labeledInput4.inputText = user.Gebruiker.email;
                    labeledInput5.inputText = user.Gebruiker.achternaam;
                    labeledInput6.inputText = user.Gebruiker.postcode;
                    labeledInput7.inputText = user.Gebruiker.landnaam;
                    labeledInput8.inputText = user.banknaam;
                    labeledInput9.inputText = user.creditcardnummer;
                    labeledInput10.inputText = user.rekeningnummer;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            parent.goBack();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Wilt u de informatie van de verkoper updaten", "Let op!", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                user.Gebruiker = null;
                user.banknaam = labeledInput8.inputText;
                user.creditcardnummer = labeledInput9.inputText;
                user.rekeningnummer = labeledInput10.inputText;
                try
                {
                    db.updateVerkoper(user);
                }
                catch
                {
                    MessageBox.Show("Er is iets mis gegaan met opslaan");
                }
                finally
                {
                    Reload();
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (user != null)
            {
                DialogResult result = MessageBox.Show("Weet u zeker dat u de verkoper wil verwijderen?", "Let op!", MessageBoxButtons.OKCancel);
                if (result == DialogResult.OK)
                {
                    try
                    {
                        db.removeVerkoper(user);
                    }
                    catch
                    {
                        MessageBox.Show("Er is iets mis gegaan met het verwijderen an de verkoper");
                    }
                    finally
                    {
                        parent.goBack();
                    }
                }
            }
        }
    }
}
