﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EenmaalAndermaalBeheer.SqlManipulation
{
    public abstract class SqlClause
    {
        public string columnName { get; set; }
        public string value { get; set; }

        public SqlClause(string columnName, string value)
        {
            this.columnName = columnName;
            this.value = value;
        }

        public abstract string getQueryPart();
        public abstract KeyValuePair<string, string>? getParameter();

        public static SqlStatement addClausesToQuery(string query, SqlClause[] clauses)
        {
            string whereClause = "";
            DynamicParameters parameter = new DynamicParameters();
            if (!string.IsNullOrEmpty(query))
            {
                foreach (SqlClause clause in clauses)
                {
                    if (!string.IsNullOrEmpty(whereClause)) whereClause += " AND ";
                    whereClause += clause.getQueryPart();
                    if(clause.getParameter() != null) parameter.Add(clause.getParameter().Value.Key, clause.getParameter().Value.Value);
                }
                if (!string.IsNullOrEmpty(whereClause)) query += " WHERE " + whereClause;
            }
            return new SqlStatement(query, parameter);
        }
    }
}
