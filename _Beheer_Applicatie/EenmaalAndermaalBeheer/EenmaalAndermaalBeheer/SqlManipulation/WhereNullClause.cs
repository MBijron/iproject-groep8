﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EenmaalAndermaalBeheer.SqlManipulation
{
    class WhereNullClause : SqlClause
    {
        private bool isNull = true;
        public WhereNullClause(string columnName, bool isNull) : base(columnName, "")
        {
        }

        public override string getQueryPart()
        {
            return columnName + " IS" + (isNull ? "" : " NOT") + " NULL";
        }

        public override KeyValuePair<string, string>? getParameter()
        {
            return null;
        }
    }
}
