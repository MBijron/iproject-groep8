﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EenmaalAndermaalBeheer.SqlManipulation
{
    class WhereClause : SqlClause
    {
        public WhereClause(string columnName, string value) : base(columnName, value)
        {
        }

        public override string getQueryPart()
        {
            return columnName + "=@" + columnName.Replace(".", "_") + "_CLAUSE";
        }

        public override KeyValuePair<string, string>? getParameter()
        {
            return new KeyValuePair<string, string>(columnName.Replace(".", "_") + "_CLAUSE", value);
        }
    }
}
