﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EenmaalAndermaalBeheer.SqlManipulation
{
    public class ContainsClause : SqlClause
    {

        public ContainsClause(string columnName, string contains) : base(columnName, contains)
        {
        }

        public override string getQueryPart()
        {
            return columnName + " LIKE @" + columnName.Replace(".", "_") + "_clause";
        }

        public override KeyValuePair<string, string>? getParameter()
        {
            return new KeyValuePair<string, string>("@" + columnName.Replace(".", "_") + "_clause", "%" + value + "%");
        }
    }
}
