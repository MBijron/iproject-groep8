﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EenmaalAndermaalBeheer.SqlManipulation
{
    public class SqlStatement
    {
        public DynamicParameters parameters { get; private set; }
        public string query { get; private set; }

        public SqlStatement(string query, DynamicParameters parameters)
        {
            this.parameters = parameters;
            this.query = query;
        }

        public SqlStatement(string query)
        {
            this.query = query;
        }
    }
}
