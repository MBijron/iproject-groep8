﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using EenmaalAndermaalBeheer.DataTypes;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using EenmaalAndermaalBeheer.SqlManipulation;
using System.Transactions;
using System.Windows.Forms;

namespace EenmaalAndermaalBeheer
{
    public class DatabaseHandler
    {
        private IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlServerConnString"].ConnectionString);

        public DatabaseHandler()
        {
        }

        public List<Vraag> getVragen()
        {
            return db.GetList<Vraag>().ToList();
        }

        public Vraag getVraag(long id)
        {
            return db.Get<Vraag>(id);
        }

        public void addVraag(Vraag vraag)
        {
            db.Insert(vraag);
        }

        public void removeVraag(Vraag vraag)
        {
            db.Delete(vraag);
        }

        public bool vraagHasGebruikers(Vraag vraag)
        {
            return db.RecordCount<Gebruiker>("where vraagID=@vraagID", vraag) > 0;
        }

        public List<Gebruiker> getGebruikers(SqlClause[] contains)
        {
            SqlStatement statement = SqlLoader.LoadSQLStatement("getGebruikers", contains);
            Debug.WriteLine(statement.query);
            List<dynamic> objects = (List<dynamic>)db.Query<dynamic>(statement.query, statement.parameters);
            List<Gebruiker> gebruikers = Slapper.AutoMapper.MapDynamic<Gebruiker>(objects).ToList();
            return gebruikers;
        }

        public Gebruiker getGebruiker(long id)
        {
            return getGebruikers(new SqlClause[] { new WhereClause("GEBRUIKER.gebruikerID", id.ToString()) }).FirstOrDefault();
        }

        public void removeGebruiker(Gebruiker gebruiker)
        {
            using (var transaction = new TransactionScope())
            {
                db.Delete(gebruiker);
                if(gebruiker.GebruikersTelefoons != null) db.DeleteList<GebruikersTelefoon>(gebruiker.GebruikersTelefoons);
                transaction.Complete();
            }
                
        }

        public void updateGebruiker(Gebruiker gebruiker)
        {
            using (var transaction = new TransactionScope())
            {
                db.Update(gebruiker);
                foreach (GebruikersTelefoon tel in gebruiker.GebruikersTelefoons)
                {
                    db.Update(tel);
                }
                transaction.Complete();
            }
        }

        public List<Rubriek> getRubrieken()
        {
            string query = SqlLoader.LoadSQLStatement("getRubrieken").query;
            List<dynamic> objects = (List<dynamic>)db.Query<dynamic>(query);
            List<Rubriek> rubriek = Slapper.AutoMapper.MapDynamic<Rubriek>(objects).ToList();
            return rubriek;
        }

        public List<Rubriek> getBottomRubrieken()
        {
            List<Rubriek> rubrieken = getRubrieken();
            return rubrieken.Except(rubrieken, (a) => a.rubriekID, (a) => a.bovenliggendID).ToList();
        }

        public Rubriek getRubriek(long id)
        {
            return db.Get<Rubriek>(id);
        }
        public Rubriek getRubriekByName(string name)
        {
            return db.GetList<Rubriek>("where rubrieknaam = @naam", new { naam = name }).SingleOrDefault();
        }
        public void addRubriek(Rubriek rubriek)
        {
            db.Insert(rubriek);
        }

        public void removeRubriek(Rubriek rubriek)
        {
            db.Delete(rubriek);
        }

        public List<Verkoper> getVerkopers(SqlClause[] contains)
        {
            SqlStatement statement = SqlLoader.LoadSQLStatement("getVerkopers", contains);
            Debug.WriteLine(statement.query);
            List<dynamic> objects = (List<dynamic>)db.Query<dynamic>(statement.query, statement.parameters);
            return Slapper.AutoMapper.MapDynamic<Verkoper>(objects).ToList();
        }

        public Verkoper getVerkoper(long id)
        {
            Verkoper verkoper = db.Get<Verkoper>(id);
            verkoper.Gebruiker = getGebruiker(verkoper.gebruikerID);
            return verkoper;
        }

        public void removeVerkoper(Verkoper verkoper)
        {
            using (var transaction = new TransactionScope())
            {
                Gebruiker gebruiker = getGebruiker(verkoper.gebruikerID);
                db.Delete(verkoper);
                gebruiker.verkoper = false;
                updateGebruiker(gebruiker);
                transaction.Complete();
            }
        }

        public void updateVerkoper(Verkoper verkoper)
        {
            db.Update(verkoper);
            if (verkoper.Gebruiker != null)
            {
                updateGebruiker(verkoper.Gebruiker);
            }
        }

        public List<Voorwerp> getVoorwerpen(ContainsClause[] contains)
        {
            Slapper.AutoMapper.Cache.ClearAllCaches();
            SqlStatement statement = SqlLoader.LoadSQLStatement("getVoorwerpen", contains);
            Debug.WriteLine(statement.query);
            List<dynamic> objects = (List<dynamic>)db.Query<dynamic>(statement.query, statement.parameters);
            return Slapper.AutoMapper.MapDynamic<Voorwerp>(objects).ToList();
        }

        public Voorwerp getVoorwerp(long id)
        {
            Slapper.AutoMapper.Cache.ClearAllCaches();
            string query = SqlLoader.LoadSQLStatement("getVoorwerpen").query + " WHERE VOORWERP.voorwerpID=@id";
            Debug.WriteLine(query);
            List<dynamic> objects = (List<dynamic>)db.Query<dynamic>(query, new { id = id });
            List<Voorwerp> voorwerpen = Slapper.AutoMapper.MapDynamic<Voorwerp>(objects).ToList();
            return voorwerpen.FirstOrDefault();
        }

        public void removeVoorwerp(Voorwerp voorwerp)
        {
            db.Delete(voorwerp);
        }

        public void updateVoorwerp(Voorwerp voorwerp)
        {
            db.Update(voorwerp);
            Voorwerp currentVoorwerp = getVoorwerp(voorwerp.voorwerpID);
            Rubriek[] rubriekenToBeAdded = voorwerp.Rubrieken.Except(currentVoorwerp.Rubrieken, (a) => a.rubrieknaam, (b) => b.rubrieknaam).ToArray();
            Rubriek[] rubriekenToBeDeleted = currentVoorwerp.Rubrieken.Except(voorwerp.Rubrieken, (a) => a.rubrieknaam, (b) => b.rubrieknaam).ToArray();

            foreach(Rubriek rubriek in rubriekenToBeAdded)
            {
                addVoorwerpRubriek(new VoorwerpRubriek() { rubriekID = rubriek.rubriekID, voorwerpID = voorwerp.voorwerpID });
            }
            foreach(Rubriek rubriek in rubriekenToBeDeleted)
            {
                removeVoorwerpRubriek(new VoorwerpRubriek() { rubriekID = rubriek.rubriekID, voorwerpID = voorwerp.voorwerpID });
            }
        }

        public void addVoorwerpRubriek(VoorwerpRubriek voorwerpRubriek)
        {
            db.Insert(voorwerpRubriek);
        }

        public void removeVoorwerpRubriek(VoorwerpRubriek voorwerpRubriek)
        {
            db.Query("DELETE FROM voorwerp_rubriek WHERE rubriekID=@rubriekID AND voorwerpID=@voorwerpID", voorwerpRubriek);
        }

        public void removeBod(Bod bod)
        {
            db.Delete(bod);
        }
    }
}
