﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EenmaalAndermaalBeheer.DataTypes
{
    [Table("VRAAG")]
    public partial class Vraag
    {
        [Key]
        public virtual long vraagID { get; set; }
        public virtual string tekst { get; set; }
        public virtual IEnumerable<Gebruiker> Gebruikers { get; set; }
    }

    /// <summary>
    /// A class which represents the GEBRUIKER table.
    /// </summary>
	[Table("GEBRUIKER")]
    public partial class Gebruiker
    {
        [Key]
        public virtual long gebruikerID { get; set; }
        public virtual long vraagID { get; set; }
        public virtual string gebruikersnaam { get; set; }
        public virtual string achternaam { get; set; }
        public virtual string voornaam { get; set; }
        public virtual string adres { get; set; }
        public virtual string antwoordtekst { get; set; }
        public virtual DateTime datum { get; set; }
        public virtual string email { get; set; }
        public virtual string landnaam { get; set; }
        public virtual string plaatsnaam { get; set; }
        public virtual string postcode { get; set; }
        public virtual string password { get; set; }
        public virtual bool verkoper { get; set; }
        public virtual Vraag Vraag { get; set; }
        public virtual IEnumerable<Voorwerp> Voorwerpen { get; set; }
        public virtual IEnumerable<Bod> Boden { get; set; }
        public virtual IEnumerable<Feedback> Feedback { get; set; }
        public virtual IEnumerable<GebruikersTelefoon> GebruikersTelefoons { get; set; }
        public virtual IEnumerable<Verkoper> Verkopers { get; set; }
    }

    /// <summary>
    /// A class which represents the VOORWERP table.
    /// </summary>
	[Table("VOORWERP")]
    public partial class Voorwerp
    {
        [Key]
        public virtual long voorwerpID { get; set; }
        public virtual long gebruikerID { get; set; }
        public virtual int? koper { get; set; }
        public virtual byte dagen { get; set; }
        public virtual decimal startprijs { get; set; }
        public virtual decimal? verkoopprijs { get; set; }
        public virtual string beschrijving { get; set; }
        public virtual string betalingsinstructie { get; set; }
        public virtual string betalingswijzenaam { get; set; }
        public virtual DateTime datum { get; set; }
        public virtual TimeSpan tijd { get; set; }
        public virtual string titel { get; set; }
        public virtual string verzendinstructies { get; set; }
        public virtual string verzendwijzenaam { get; set; }
        public virtual bool gesloten { get; set; }
        public virtual Gebruiker Gebruiker { get; set; }
        public virtual IEnumerable<Bestand> Bestanden { get; set; }
        public virtual IEnumerable<Bod> Boden { get; set; }
        public virtual List<Rubriek> Rubrieken { get; set; }
    }

    /// <summary>
    /// A class which represents the BESTAND table.
    /// </summary>
	[Table("BESTAND")]
    public partial class Bestand
    {
        [Key]
        public virtual long bestandID { get; set; }
        public virtual string bestandsnaam { get; set; }
        public virtual int voorwerpID { get; set; }
        public virtual Voorwerp Voorwerp { get; set; }
    }

    /// <summary>
    /// A class which represents the BOD table.
    /// </summary>
	[Table("BOD")]
    public partial class Bod
    {
        [Key]
        public virtual long bodID { get; set; }
        public virtual long gebruikerID { get; set; }
        public virtual long voorwerpID { get; set; }
        public virtual decimal bedrag { get; set; }
        public virtual DateTime datum { get; set; }
        public virtual TimeSpan tijd { get; set; }
        public virtual Gebruiker Gebruiker { get; set; }
        public virtual Voorwerp Voorwerp { get; set; }
    }

    /// <summary>
    /// A class which represents the FEEDBACK table.
    /// </summary>
	[Table("FEEDBACK")]
    public partial class Feedback
    {
        [Key]
        public virtual long feedbackID { get; set; }
        public virtual long gebruikerID { get; set; }
        public virtual string cijfer { get; set; }
        public virtual string commentaar { get; set; }
        public virtual DateTime datum { get; set; }
        public virtual DateTime tijd { get; set; }
        public virtual Gebruiker Gebruiker { get; set; }
    }

    /// <summary>
    /// A class which represents the GEBRUIKERSTELEFOON table.
    /// </summary>
	[Table("GEBRUIKERSTELEFOON")]
    public partial class GebruikersTelefoon
    {
        [Key]
        public virtual long gebruikerstelefoonID { get; set; }
        public virtual long gebruikerID { get; set; }
        public virtual string telefoonnummer { get; set; }
        public virtual string volgnr { get; set; }
        public virtual Gebruiker Gebruiker { get; set; }

        public override string ToString()
        {
            return telefoonnummer;
        }
    }

    /// <summary>
    /// A class which represents the RUBRIEK table.
    /// </summary>
	[Table("RUBRIEK")]
    public partial class Rubriek
    {
        [Key]
        public virtual long rubriekID { get; set; }
        public virtual long? bovenliggendID { get; set; }
        public virtual string rubrieknaam { get; set; }
        public virtual Rubriek parentRubriek { get; set; }
        public virtual IEnumerable<VoorwerpRubriek> VoorwerpRubrieken { get; set; }
    }

    /// <summary>
    /// A class which represents the VERKOPER table.
    /// </summary>
	[Table("VERKOPER")]
    public partial class Verkoper
    {
        [Key]
        public virtual long verkoperID { get; set; }
        public virtual long gebruikerID { get; set; }
        public virtual string controleoptie { get; set; }
        public virtual string banknaam { get; set; }
        public virtual string creditcardnummer { get; set; }
        public virtual string rekeningnummer { get; set; }
        public virtual Gebruiker Gebruiker { get; set; }
    }

    /// <summary>
    /// A class which represents the VOORWERP_RUBRIEK table.
    /// </summary>
	[Table("VOORWERP_RUBRIEK")]
    public partial class VoorwerpRubriek
    {
        [Key]
        public virtual long voorwerp_rubriekID { get; set; }
        public virtual long voorwerpID { get; set; }
        public virtual long rubriekID { get; set; }
        public virtual Voorwerp Voorwerp { get; set; }
        public virtual Rubriek Rubriek { get; set; }
    }
}
