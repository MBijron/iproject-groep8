﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EenmaalAndermaalBeheer.DataTypes
{
    class CustomDataGridViewRow : DataGridViewRow
    {
        public CustomDataGridViewRow(params object[] contents)
        {
            Cells.AddRange(contents.Select((a) => new CustomDataGridViewCell(a)).ToArray());
        }

        internal class CustomDataGridViewCell : DataGridViewTextBoxCell
        {
            public CustomDataGridViewCell(object contents)
            {
                Value = contents;
            }
        }
    }
}
