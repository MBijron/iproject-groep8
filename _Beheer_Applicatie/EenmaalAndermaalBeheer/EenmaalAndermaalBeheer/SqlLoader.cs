﻿using Dapper;
using EenmaalAndermaalBeheer.DataTypes;
using EenmaalAndermaalBeheer.SqlManipulation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EenmaalAndermaalBeheer
{
    class SqlLoader
    {
        public static SqlStatement LoadSQLStatement(string statementName)
        {
            string sqlStatement = string.Empty;

            string resourceName = typeof(SqlLoader).Namespace + ".SQL." + statementName + ".sql";

            using (Stream stm = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
            {
                if (stm != null)
                {
                    sqlStatement = new StreamReader(stm).ReadToEnd();
                }
            }
            return new SqlStatement(sqlStatement);
        }

        public static SqlStatement LoadSQLStatement(string statementName, SqlClause[] clauses)
        {
            string query = LoadSQLStatement(statementName).query;
            return SqlClause.addClausesToQuery(query, clauses);
        }
    }
}
