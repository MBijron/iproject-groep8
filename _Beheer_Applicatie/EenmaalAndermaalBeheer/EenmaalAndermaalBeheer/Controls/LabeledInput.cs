﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EenmaalAndermaalBeheer
{
    public partial class LabeledInput : UserControl
    {
        public string labelText { get { return label1.Text; } set { label1.Text = value; } }
        public string inputText { get { return textBox1.Text; } set { textBox1.Text = value; } }
        public bool readOnly { get { return textBox1.ReadOnly; } set { textBox1.ReadOnly = value; } }
        public bool fixedTextboxWidth { get; set; }
        public int textboxWidth { get; set; }
        public bool collectiveSharedSize { get; set; }
        public string collectiveSharedId { get; set; }

        private static List<LabeledInput> allLabels = new List<LabeledInput>();
        private bool notified = false;

        public LabeledInput()
        {
            InitializeComponent();
            allLabels.Add(this);
        }

        private List<LabeledInput> getCollectiveInputGroup()
        {
            return allLabels.Where(a => a.collectiveSharedId == collectiveSharedId).ToList();
        }

        private string getTextboxText()
        {
            if (string.IsNullOrEmpty(textBox1.Text)) return null;
            else return textBox1.Text;
        }

        private int getCollectiveBiggestWidth()
        {
            return getCollectiveInputGroup().Max(a => a.Width);
        }

        private int getCollectiveBiggestLabelWidth()
        {
            return getCollectiveInputGroup().Max(a => a.label1.Width);
        }

        private void notifyCollectiveGroup()
        {
            getCollectiveInputGroup().ForEach(a => a.notify());
        }

        private void label1_SizeChanged(object sender, EventArgs e)
        {
            if(!notified) refresh();
        }

        private void LabeledInput_FontChanged(object sender, EventArgs e)
        {
            label1.Font = Font;
        }

        private void LabeledInput_SizeChanged(object sender, EventArgs e)
        {
            if (!notified) refresh();
        }

        private void LabeledInput_Load(object sender, EventArgs e)
        {
            refresh();
        }

        public void notify()
        {
            notified = true;
            refresh();
            notified = false;
        }

        private void refresh()
        {
            textBox1.Location = new Point(label1.Width, 0);
            if(collectiveSharedSize)
            {
                label1.AutoSize = false;
                if(getCollectiveBiggestLabelWidth() > label1.Width)
                {
                    label1.Width = getCollectiveBiggestLabelWidth();
                }
                if(getCollectiveBiggestWidth() > Width && !fixedTextboxWidth)
                {
                    Width = getCollectiveBiggestWidth();
                }
                if(fixedTextboxWidth)
                {
                    textBox1.Width = textboxWidth;
                    Width = textBox1.Location.X + textBox1.Width;
                }
                else
                {
                    textBox1.Width = Width - textBox1.Location.X;
                }
                textBox1.Location = new Point(label1.Width, 0);
            }
            else if (fixedTextboxWidth)
            {
                label1.AutoSize = true;
                textBox1.Width = textboxWidth;
                Width = textBox1.Location.X + textBox1.Width;
            }
            else
            {
                label1.AutoSize = true;
                textBox1.Width = Width - textBox1.Location.X;
            }
            if (!notified) notifyCollectiveGroup();
        }
    }
}
