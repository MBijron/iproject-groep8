﻿namespace EenmaalAndermaalBeheer
{
    partial class PageTemplate
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.loadingBar1 = new EenmaalAndermaalBeheer.Controls.LoadingBar();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Homepage";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label2.Location = new System.Drawing.Point(641, -9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 37);
            this.label2.TabIndex = 3;
            this.label2.Text = "↺";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // loadingBar1
            // 
            this.loadingBar1.BackColor = System.Drawing.Color.White;
            this.loadingBar1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.loadingBar1.Location = new System.Drawing.Point(233, 218);
            this.loadingBar1.Name = "loadingBar1";
            this.loadingBar1.Size = new System.Drawing.Size(177, 43);
            this.loadingBar1.TabIndex = 2;
            this.loadingBar1.Visible = false;
            // 
            // PageTemplate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.loadingBar1);
            this.MinimumSize = new System.Drawing.Size(668, 488);
            this.Name = "PageTemplate";
            this.Size = new System.Drawing.Size(668, 488);
            this.SizeChanged += new System.EventHandler(this.PageTemplate_SizeChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Controls.LoadingBar loadingBar1;
        private System.Windows.Forms.Label label2;
    }
}
