﻿namespace EenmaalAndermaalBeheer
{
    partial class SideMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.customListBox1 = new EenmaalAndermaalBeheer.CustomListBox();
            this.SuspendLayout();
            // 
            // customListBox1
            // 
            this.customListBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.customListBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.customListBox1.customHoverColor = false;
            this.customListBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.customListBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.customListBox1.FormattingEnabled = true;
            this.customListBox1.hoverColor = System.Drawing.Color.Empty;
            this.customListBox1.Location = new System.Drawing.Point(0, 0);
            this.customListBox1.Name = "customListBox1";
            this.customListBox1.Size = new System.Drawing.Size(445, 442);
            this.customListBox1.TabIndex = 0;
            this.customListBox1.verticalPadding = 20;
            this.customListBox1.valueChanged += new EenmaalAndermaalBeheer.CustomListBox.ValueChangedHandler(this.customListBox1_valueChanged);
            this.customListBox1.FontChanged += new System.EventHandler(this.customListBox1_FontChanged);
            // 
            // SideMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.customListBox1);
            this.Name = "SideMenu";
            this.Size = new System.Drawing.Size(445, 442);
            this.ResumeLayout(false);

        }

        #endregion

        private CustomListBox customListBox1;
    }
}
