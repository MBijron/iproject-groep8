﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EenmaalAndermaalBeheer
{
    class ControlDisplay : Panel
    {
        public Control displayControl { get { return DisplayControl; } set { DisplayControl = value; swapControl(); } }
        private Control DisplayControl;

        public ControlDisplay()
        {
            DoubleBuffered = true;
        }
        private void swapControl()
        {
            if(DisplayControl != null)
            {
                SuspendLayout();
                Controls.Clear();
                Controls.Add(DisplayControl);
                DisplayControl.Dock = DockStyle.Fill;
                ResumeLayout();
            }
        }
    }
}
