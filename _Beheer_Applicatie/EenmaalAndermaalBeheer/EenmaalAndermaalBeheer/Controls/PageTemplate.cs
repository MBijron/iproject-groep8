﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EenmaalAndermaalBeheer.Controls;
using EenmaalAndermaalBeheer.DataTypes;

namespace EenmaalAndermaalBeheer
{
    [ToolboxItem(false)]
    public partial class PageTemplate : UserControl
    {
        public string Title { get { return label1.Text; } set { label1.Text = value; } }
        protected DatabaseHandler db;
        protected bool controlLoaded = false;
        protected bool working = false;
        protected Form1 parent;
        public PageTemplate()
        {
            InitializeComponent();
            Disable();
        }

        public PageTemplate(DatabaseHandler db, Form1 parent) : this()
        {
            this.db = db;
            this.parent = parent;
        }

        public void LoadOrRefresh()
        {
            if (controlLoaded)
                ReloadControl();
            else
                LoadControl();
        }

        //load the control
        public async void LoadControl(bool ignoreWorking = false)
        {
            if(InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    LoadControl(ignoreWorking);
                }));
            }
            else
            {
                if (!working || ignoreWorking)
                {
                    working = true;
                    await Task.Run(new Action(OnLoad));
                    controlLoaded = true;
                    Enable();
                    working = false;
                }
            }
        }

        //reload the control
        public async void ReloadControl(bool ignoreWorking = false)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    ReloadControl(ignoreWorking);
                }));
            }
            else
            {
                if (!working || ignoreWorking)
                {
                    working = true;
                    Disable();
                    await Task.Run(new Action(Reload));
                    controlLoaded = true;
                    Enable();
                    working = false;
                }
            }
        }

        //first time the control loads;
        protected virtual void OnLoad()
        {

        }

        //every time the control is reloaded
        protected virtual void Reload()
        {

        }

        protected void Disable()
        {
            Enabled = false;
            loadingBar1.Visible = true;
            loadingBar1.BringToFront();
        }

        protected void Enable()
        {
            Enabled = true;
            loadingBar1.Visible = false;
        }

        public void showLoadingBar()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() => showLoadingBar()));
            }
            else
            {
                loadingBar1.Visible = true;
            }
        }

        public void hideLoadingBar()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() => hideLoadingBar()));
            }
            else
            {
                loadingBar1.Visible = false;
            }
        }

        protected void clearFields()
        {
            if(InvokeRequired)
            {
                Invoke(new Action(clearFields));
            }
            else
            {
                foreach (Control control in this.Controls)
                {
                    if (control is TextBox)
                        ((TextBox)control).Clear();
                    else if (control is RichTextBox)
                        ((RichTextBox)control).Clear();
                    else if (control is LabeledInput)
                        ((LabeledInput)control).inputText = "";
                }
            }
            
        }

        //force double buffering
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams result = base.CreateParams;
                result.ExStyle |= 0x02000000; // WS_EX_COMPOSITED
                return result;
            }
        }

        private void PageTemplate_SizeChanged(object sender, EventArgs e)
        {
            loadingBar1.Location = new Point((Width / 2) - (loadingBar1.Width / 2), (Height / 2) - (loadingBar1.Height / 2));
            loadingBar1.BringToFront();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            ReloadControl();
        }
    }
}
