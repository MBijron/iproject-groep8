﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EenmaalAndermaalBeheer
{
    public partial class SideMenu : UserControl
    {
        //public properties
        public Color textColor { get { return customListBox1.ForeColor; } set { customListBox1.ForeColor = value; } }
        public Color backgroundColor { get { return customListBox1.BackColor; } set { customListBox1.BackColor = value; } }
        public int verticalPadding { get { return customListBox1.verticalPadding; } set { customListBox1.verticalPadding = value; } }
        public string[] items { get { return customListBox1.Items.Cast<string>().ToArray(); } set { customListBox1.Items.Clear(); customListBox1.Items.AddRange(value); } }
        public Color hoverColor { get { return customListBox1.hoverColor; } set { customListBox1.hoverColor = value; } }
        public bool customHoverColor { get { return customListBox1.customHoverColor; } set { customListBox1.customHoverColor = value; } }
        public bool centerText { get { return customListBox1.centerText; } set { customListBox1.centerText = value; } }

        //private variables
        private List<Binding> bindings = new List<Binding>();

        //eventhandlers
        public delegate void BindingFiredHandler(object sender, Binding item);
        public event BindingFiredHandler bindingFired;
        public SideMenu()
        {
            InitializeComponent();
        }

        public void bind(string item, object binding)
        {
            bindings.Add(new Binding(item, binding));
        }

        private void customListBox1_FontChanged(object sender, EventArgs e)
        {
            customListBox1.Font = Font;
        }

        //force double buffering
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams result = base.CreateParams;
                result.ExStyle |= 0x02000000; // WS_EX_COMPOSITED
                return result;
            }
        }

        private void customListBox1_valueChanged(object sender, string item)
        {
            List<Binding> firedBindings = bindings.Where((a) => a.key == item).ToList();
            foreach(Binding binding in firedBindings)
            {
                bindingFired?.Invoke(this, binding);
            }
        }

        public class Binding
        {
            public string key { get; private set; }
            public object item { get; private set; }

            public Binding(string key, object item)
            {
                this.key = key;
                this.item = item;
            }
        }
    }
}
