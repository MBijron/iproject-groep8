﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EenmaalAndermaalBeheer
{
    class CustomListBox : ListBox
    {
        //private properties
        private int hoverIndex = 0;
        private bool skipIndexEvent = false;

        //public properties
        public int verticalPadding { get; set; } = 20;
        public Color hoverColor { get; set; }
        public bool customHoverColor { get; set; }
        public bool centerText { get; set; }

        //eventhandlers
        public delegate void ValueChangedHandler(object sender, string item);
        public event ValueChangedHandler valueChanged;

        public CustomListBox() : base()
        {
            DoubleBuffered = true;
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            SetStyle(ControlStyles.DoubleBuffer, true);
            DrawMode = DrawMode.OwnerDrawVariable;
        }

        protected override void OnNotifyMessage(Message m)
        {
            //Filter out the WM_ERASEBKGND message
            if (m.Msg != 0x14)
            {
                base.OnNotifyMessage(m);
            }
        }

        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            base.OnDrawItem(e);
            //listBox1.SuspendLayout();
            e.DrawBackground();
            e.DrawFocusRectangle();
            Graphics g = e.Graphics;

            // draw the background color you want
            // mine is set to olive, change it to whatever you want
            Color backColor = BackColor;
            if (e.Index == hoverIndex) backColor = customHoverColor? hoverColor : ControlPaint.Light(backColor);
            g.FillRectangle(new SolidBrush(backColor), e.Bounds);

            // draw the text of the list item, not doing this will only show
            // the background color
            // you will need to get the text of item to display
            string text = "";
            if (e.Index < Items.Count && e.Index >= 0) text = Items[e.Index].ToString();
            //if (e.Index == highlight) foreColor = selectColor;
            RectangleF location = new RectangleF(e.Bounds.X, e.Bounds.Y + (verticalPadding / 2), e.Bounds.Width, e.Bounds.Height);
            SizeF textSize = g.MeasureString(text, e.Font);
            if (centerText)
                location = new RectangleF((e.Bounds.Width / 2) - (textSize.Width / 2), e.Bounds.Y + (verticalPadding / 2), e.Bounds.Width, e.Bounds.Height);
            g.DrawString(resizeString(text, e.Font, g), e.Font, new SolidBrush(ForeColor), location);
            //listBox1.ResumeLayout();
        }

        protected override void OnMeasureItem(MeasureItemEventArgs e)
        {
            base.OnMeasureItem(e);
            e.ItemHeight = Font.Height + verticalPadding;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            Point point = PointToClient(Cursor.Position);
            int index = IndexFromPoint(point);
            if (index != hoverIndex)
            {
                if (hoverIndex > 0 && hoverIndex < Items.Count) Invalidate(GetItemRectangle(hoverIndex));
                hoverIndex = index;
                if (hoverIndex > 0 && hoverIndex < Items.Count) Invalidate(GetItemRectangle(hoverIndex));
            }
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            if(SelectedIndex >= 0)
            {
                if(!skipIndexEvent || SelectedIndex != 0)
                {
                    valueChanged?.Invoke(this, Items[SelectedIndex].ToString());
                    skipIndexEvent = true;
                    SelectedIndex = Items.Count >= 0 ? 0 : -1;
                }
                else
                {
                    skipIndexEvent = false;
                }
            }
            base.OnSelectedIndexChanged(e);
        }

        private string resizeString(string text, Font font, Graphics g)
        {
            if (g.MeasureString(text, font).Width > Size.Width) return resizeString(text.Substring(0, text.Length - 2), font, g);
            else return text;
        }
    }
}
