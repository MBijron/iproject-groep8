﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.Diagnostics;
using System.Drawing.Imaging;

namespace EenmaalAndermaalBeheer.Controls
{
    public partial class LoadingBar : UserControl
    {
        Timer animateTimer = new Timer();
        private int currentFrame = 0;
        public LoadingBar()
        {
            InitializeComponent();
            animateTimer.Interval = 80;
            animateTimer.Tick += pictureBox1_EnabledChanged;
            animateTimer.Start();
        }

        private void pictureBox1_EnabledChanged(object sender, EventArgs e)
        {
            currentFrame = currentFrame >= 11 ? 0 : currentFrame + 1;
            pictureBox1.Image.SelectActiveFrame(new FrameDimension(pictureBox1.Image.FrameDimensionsList[0]), currentFrame);
            Invalidate();
        }

        private void pictureBox1_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
                animateTimer.Start();
            else
                animateTimer.Stop();
        }
    }
}
