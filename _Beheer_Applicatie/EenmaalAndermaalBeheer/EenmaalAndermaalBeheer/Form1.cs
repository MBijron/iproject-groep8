﻿using EenmaalAndermaalBeheer.DataTypes;
using EenmaalAndermaalBeheer.Pages;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EenmaalAndermaalBeheer
{
    public partial class Form1 : Form
    {
        private DatabaseHandler db = new DatabaseHandler();
        private PageTemplate mainPage;
        private PageTemplate previousPage;
        public Form1()
        {
            InitializeComponent();
            mainPage = new MainPage(db, this);
            controlDisplay1.displayControl = mainPage;
            sideMenu1.bind("Gebruikers", new UserSearch(db, this));
            sideMenu1.bind("Verkopers", new SellerSearch(db, this));
            sideMenu1.bind("Herstelvragen", new SecurityQuestions(db, this));
            sideMenu1.bind("Overzichtsrapporten", new SummaryReports(db, this));
            sideMenu1.bind("Rubrieken", new Sections(db, this));
            sideMenu1.bind("Veilingen", new Auctions(db, this));
            sideMenu1.bind("Financieel", new Financial(db, this));
        }

        private void sideMenu1_bindingFired(object sender, SideMenu.Binding item)
        {
            PageTemplate page = (PageTemplate)item.item;
            displayControl(page);
        }

        public void displayControl(PageTemplate page)
        {
            if (page != controlDisplay1.displayControl)
            {
                previousPage = (PageTemplate)controlDisplay1.displayControl;
                controlDisplay1.displayControl = page;
                page.LoadOrRefresh();
            }
        }

        public void goBack()
        {
            if (previousPage != null)
                displayControl(previousPage);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
    }
}
