﻿namespace EenmaalAndermaalBeheer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sideMenu1 = new EenmaalAndermaalBeheer.SideMenu();
            this.controlDisplay1 = new EenmaalAndermaalBeheer.ControlDisplay();
            this.SuspendLayout();
            // 
            // sideMenu1
            // 
            this.sideMenu1.BackColor = System.Drawing.Color.Black;
            this.sideMenu1.backgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.sideMenu1.centerText = true;
            this.sideMenu1.customHoverColor = false;
            this.sideMenu1.Dock = System.Windows.Forms.DockStyle.Left;
            this.sideMenu1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sideMenu1.ForeColor = System.Drawing.Color.White;
            this.sideMenu1.hoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.sideMenu1.items = new string[] {
        "Gebruikers",
        "Verkopers",
        "Rubrieken",
        "Veilingen",
        "Herstelvragen",
        "Overzichtsrapporten",
        "Financieel"};
            this.sideMenu1.Location = new System.Drawing.Point(0, 0);
            this.sideMenu1.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.sideMenu1.Name = "sideMenu1";
            this.sideMenu1.Size = new System.Drawing.Size(228, 479);
            this.sideMenu1.TabIndex = 0;
            this.sideMenu1.textColor = System.Drawing.Color.White;
            this.sideMenu1.verticalPadding = 30;
            this.sideMenu1.bindingFired += new EenmaalAndermaalBeheer.SideMenu.BindingFiredHandler(this.sideMenu1_bindingFired);
            // 
            // controlDisplay1
            // 
            this.controlDisplay1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.controlDisplay1.displayControl = null;
            this.controlDisplay1.Location = new System.Drawing.Point(226, 0);
            this.controlDisplay1.Name = "controlDisplay1";
            this.controlDisplay1.Size = new System.Drawing.Size(669, 479);
            this.controlDisplay1.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 479);
            this.Controls.Add(this.sideMenu1);
            this.Controls.Add(this.controlDisplay1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private SideMenu sideMenu1;
        private ControlDisplay controlDisplay1;
    }
}

