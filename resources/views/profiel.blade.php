@extends('layouts.master')

@section('title', 'EenmaalAndermaal')

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <div class="profiel">
                <div class="profiel-body">
                    <div class="profiel-titel">
                        <h3 class="content-title">{{$gebruiker->gebruikersnaam}}</h3>
                    </div>
                    <div class="profiel-infobox">
                        <div class="gebruiker-infobox-seg">
                            <p class="color-blue">E-mail</p>
							@if(!$gebruiker->email)
								<p>Geen email</p>
							@else
								<p>{{$gebruiker->email}}</p>
							@endif
                        </div>
                        <div class="gebruiker-infobox-seg">
                            <p class="color-blue">Telefoon</p>
							@if(!$gebruiker->telefoons()->first())
								<p>Geen telefoonnummer</p>
							@else
								@foreach($gebruiker->telefoons() as $telefoon)
									<p>{{$telefoon->telefoonnummer}}</p>
								@endforeach
							@endif
                        </div>
                        <div class="gebruiker-infobox-seg">
                            <p class="color-blue">Woonplaats</p>
							@if($gebruiker->plaatsnaam)
								<p>{{$gebruiker->plaatsnaam . ', ' . $gebruiker->landnaam}}</p>
							@else
								<p>{{$gebruiker->landnaam}}</p>
							@endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="feedback">
                <div class="feedback-body">
                    <div class="feedback-titel">
                        <h3>Feedback</h3>
                    </div>
                    <div class="feedback-list">
                        <table class="table table-striped table-scroll">
                            <thead>
                            <tr>
                                <th>Gebruiker</th>
                                <th>Bericht</th>
                                <th>Feedback</th>
                            </tr>
                            </thead>
                            <tbody>
							@foreach($allfeedback as $index => $feedback)
                            <tr>
                                <td class="filterable-cell">{{$feedback->gebruiker->gebruikersnaam}}</td>
                                <td class="filterable-cell">{{$feedback->commentaar}}</td>
                                <td class="filterable-cell">{{$feedback->cijfer}}</td>
                            </tr>
							@endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
			<div class="content">
				<div class="advertenties">
					<div class="advertenties-body">
						<h3 class="content-title">Advertenties van {{$gebruiker->gebruikersnaam}}</h3>
						<div class="advertentie-item">
						@if(!$veilingen->first())
							<p>Geen advertenties</p>
						@else
						@foreach($veilingen as $index => $veiling)
							<div class="advertentie-item-body col-md-11">
								<a class="showcase-item-clickable-container" href="veiling_details?id={{$veiling->voorwerpID}}">
									@if(!$veiling->bestanden()->first())
										<figure><img src="img/veilingen/no_image.png" width="100" height="100"></figure>
									@else
										<figure><img src="img/veilingen/{{$veiling->gebruikerID}}/{{$veiling->voorwerpID}}/{{$veiling->bestanden()->first()->bestandsnaam}}" width="100" height="100"></figure>
									@endif
									<div class="advertentie-item-info">
										<p>{{$veiling->titel}}</p>
										<p class="info-text">{{$veiling->beschrijving}}</p>
										@if(!$veiling->boden->max('bedrag'))
											<p>Huidige bod: &euro; 0.00</p>
										@else
											<p>Huidige bod: &euro; {{$veiling->boden->max('bedrag')}}</p>
										@endif
									</div>
								</a>
							</div>
						@endforeach
						@endif
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
@stop