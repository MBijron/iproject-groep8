@extends('layouts.master')

@section('title', 'EenmaalAndermaal')

@section('content')
<div class="container">
<div class="content">

<div class=WordSection1>

<h2>Privacybeleid&nbsp;EenmaalAndermaal</h2>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span lang=NL>Versie 0.1<br>
Deze pagina is voor het laatst aangepast op&nbsp;</span>18-01-2017.</p>

<p class=MsoNormal style='margin-bottom:12.0pt;line-height:normal'><span
lang=NL>Wij zijn er van bewust dat u vertrouwen stelt in ons. Wij zien het dan
ook als onze verantwoordelijkheid om uw privacy te beschermen. Op deze pagina
laten we u weten welke gegevens we verzamelen als u onze website gebruikt,
waarom we deze gegevens verzamelen en hoe we hiermee uw gebruikservaring
verbeteren. Zo snapt u precies hoe wij werken.</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span lang=NL>Dit privacybeleid is van toepassing op de diensten van&nbsp;</span>EenmaalAndermaal<span
lang=NL>. U dient zich ervan bewust te zijn dat&nbsp;</span>EenmaalAndermaal<span
lang=NL>&nbsp;niet verantwoordelijk is voor het privacybeleid van andere sites
en bronnen. Door gebruik te maken van deze website geeft u aan het privacy
beleid te accepteren.</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'>EenmaalAndermaal<span lang=NL>&nbsp;respecteert de privacy van alle
gebruikers van haar site en draagt er zorg voor dat de persoonlijke informatie
die u ons verschaft vertrouwelijk wordt behandeld.</span></p>

<h3>Ons gebruik van verzamelde gegevens</h3>

<h4>Gebruik van onze diensten</h4>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span lang=NL>Wanneer u zich aanmeldt voor een van onze diensten vragen
we u om persoonsgegevens te verstrekken. Deze gegevens worden gebruikt om de
dienst uit te kunnen voeren. De gegevens worden opgeslagen op eigen beveiligde
servers van&nbsp;</span>EenmaalAndermaal<span lang=NL>&nbsp;of die van een
derde partij. Wij zullen deze gegevens niet combineren met andere persoonlijke
gegevens waarover wij beschikken.</span></p>

<h4>Communicatie</h4>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span lang=NL>Wanneer u e-mail of andere berichten naar ons verzendt,
is het mogelijk dat we die berichten bewaren. Soms vragen wij u naar uw
persoonlijke gegevens die voor de desbetreffende situatie relevant zijn. Dit
maakt het mogelijk uw vragen te verwerken en uw verzoeken te beantwoorden. De gegevens
worden opgeslagen op eigen beveiligde servers van&nbsp;</span>EenmaalAndermaal<span
lang=NL>&nbsp;of die van een derde partij. Wij zullen deze gegevens niet
combineren met andere persoonlijke gegevens waarover wij beschikken.</span></p>

<h4>Cookies</h4>

<p class=MsoNormal style='margin-bottom:12.0pt;line-height:normal'><span
lang=NL>Wij verzamelen gegevens voor onderzoek om zo een beter inzicht te
krijgen in onze klanten, zodat wij onze diensten hierop kunnen afstemmen.</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'><span lang=NL>Deze website maakt gebruik van “cookies”
(tekstbestandtjes die op uw computer worden geplaatst) om de website te helpen
analyseren hoe gebruikers de site gebruiken. De door het cookie gegenereerde
informatie over uw gebruik van de website kan worden overgebracht naar eigen
beveiligde servers van&nbsp;</span>EenmaalAndermaal<span lang=NL>&nbsp;of die
van een derde partij. Wij gebruiken deze informatie om bij te houden hoe u de
website gebruikt, om rapporten over de website-activiteit op te stellen en
andere diensten aan te bieden met betrekking tot website-activiteit en
internetgebruik.</span></p>

<h4>Doeleinden</h4>

<p class=MsoNormal style='margin-bottom:12.0pt;line-height:normal'><span
lang=NL>We verzamelen of gebruiken geen informatie voor andere doeleinden dan
de doeleinden die worden beschreven in dit privacybeleid tenzij we van tevoren
uw toestemming hiervoor hebben verkregen.</span></p>

<h4>Derden</h4>

<p class=MsoNormal style='margin-bottom:12.0pt;line-height:normal'><span
lang=NL>De informatie wordt niet met derden gedeeld. In enkele gevallen kan de
informatie intern gedeeld worden. Onze werknemers zijn verplicht om de
vertrouwelijkheid van uw gegevens te respecteren.</span></p>

<h4>Veranderingen</h4>

<p class=MsoNormal style='margin-bottom:12.0pt;line-height:normal'><span
lang=NL>Deze privacyverklaring is afgestemd op het gebruik van en de
mogelijkheden op deze site. Eventuele aanpassingen en/of veranderingen van deze
site, kunnen leiden tot wijzigingen in deze privacyverklaring. Het is daarom
raadzaam om regelmatig deze privacyverklaring te raadplegen.</span></p>

<h4>Keuzes voor persoonsgegevens</h4>

<p class=MsoNormal style='margin-bottom:12.0pt;line-height:normal'><span
lang=NL>Wij bieden alle bezoekers de mogelijkheid tot het inzien, veranderen,
of verwijderen van alle persoonlijke informatie die op moment aan ons is
verstrekt.</span></p>

<h4>Aanpassen/uitschrijven dienst nieuwsbrief</h4>

<p class=MsoNormal style='margin-bottom:12.0pt;line-height:normal'><span
lang=NL>Onderaan iedere mailing vindt u de mogelijkheid om uw gegevens aan te
passen of om u af te melden.</span></p>

<h4>Aanpassen/uitschrijven communicatie</h4>

<p class=MsoNormal style='margin-bottom:12.0pt;line-height:normal'><span
lang=NL>Als u uw gegevens aan wilt passen of uzelf uit onze bestanden wilt
laten halen, kunt u contact met ons op nemen. Zie onderstaande contactgegevens.</span></p>

<h4>Cookies uitzetten</h4>

<p class=MsoNormal style='margin-bottom:12.0pt;line-height:normal'><span
lang=NL>De meeste browsers zijn standaard ingesteld om cookies te accepteren,
maar u kunt uw browser opnieuw instellen om alle cookies te weigeren of om aan
te geven wanneer een cookie wordt verzonden. Het is echter mogelijk dat sommige
functies en ”“services, op onze en andere websites, niet correct functioneren
als cookies zijn uitgeschakeld in uw browser.</span></p>

<h4>Vragen en feedback</h4>

<p class=MsoNormal style='margin-bottom:12.0pt;line-height:normal'><span
lang=NL>We controleren regelmatig of we aan dit privacybeleid voldoen. Als u
vragen heeft over dit privacybeleid,</span> kunt u contact met ons opnemen.</p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal'>EenmaalAndermaal</p>

<p class=MsoNormal><span lang=NL>&nbsp;</span></p>

</div>
</div>
@stop