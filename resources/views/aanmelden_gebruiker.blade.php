@extends('layouts.master')

@section('title', 'Aanmelden als een gebruiker')

@section('content')
	<main>
		<div class="content">
			<div class="row">
				<div class="col-md-6 col-md-push-3">
				  <form method="post" action="{{ action('GebruikerController@insertEmail') }}">
					<h2 class="form-signin-heading">Maak een account aan om te beginnen met bieden</h2>
					<p>Heb je al een account? <a href="login">Log dan nu in</a></p>
					<p>We versturen je eerst een beveiligingscode om te controleren of het e-mailadres geldig is. Deze code is 4 uur geldig. Na deze bevestigingscode ingevuld te hebben kan je verder met registratie.</p>
					<div class="form-group row">
					<label for="inputBevestigingscode" class="col-md-4 col-form-label">E-mailadres</label>
					<div class="col-md-8">
					<input class="form-control" type="text" id="inputEmailadres" name="email"/>
					</div>
					</div>
					  @if (count($errors) > 0)
						  <div class="alert alert-danger">
							  <ul>
								  @foreach ($errors->all() as $error)
									  <li>{{ $error }}</li>
								  @endforeach
							  </ul>
						  </div>
					  @endif
					<button class="btn btn-primary btn-block" type="submit">Verzend e-mail</button>
					  <input type="hidden" name="_token" value="{{ csrf_token() }}">
				  </form>
				</div>
			</div>
		</div>
	</main>