@extends('layouts.master')

@section('title', 'Bod plaatsen')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-push-3">
            <div class="biedingen col-md-11">
                <div class="biedingen-body">
                    <div class="biedingen-body-titel">
                        <h4 class="content-title">Bieden</h4>
                    </div>
                    <div class="biedingen-body-input">
                        <form method="post" action="{{ action('VeilingController@addBod')}}"}>
                            <input type="hidden" name="veilingID" value="{{$veiling->voorwerpID}}" id="veilingID">
                            <p class="content-titel">Voorwerp: {{$veiling->titel}}</p>
                            <div class="input-group">
                            <span class="input-group-addon">&euro;</span>
                            <input type="number" value="{{$veiling->boden->max('bedrag') + 5.00}}" min="{{$veiling->boden->max('bedrag') + 5.00}}" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" id="bod" name="bod" />
                            </div>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                                <div><input type="submit" value="Plaats bod" class="btn btn-orange"></div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                    </div>
                    <div class="biedingen-body-sluiting">
                        <p>Deze veiling eindigt op: {{date('d-m-Y',strtotime($veiling->datum.'+'.$veiling->dagen.' day'))}}</p>
                    </div>
                    <div class="row">
                        <table class="table-striped table-scroll" >
                            <thead>
                            <tr>
                                <th>Gebruiker</th>
                                <th>Bod</th>
                                <th>Datum</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($veiling->boden as $bod)
                                <tr>
                                    <td class="filterable-cell">{{$bod->gebruiker->gebruikersnaam}}</td>
                                    <td class="filterable-cell">&euro; {{$bod->bedrag}}</td>
                                    <td class="filterable-cell">{{$bod->datum}} {{$bod->tijd}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop