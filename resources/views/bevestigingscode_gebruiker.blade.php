@extends('layouts.master')

@section('title', 'Bevestig activatiecode')

@section('content')

    <div class="row">
        <div class="col-md-6 col-md-push-3">
            <form>
                <h2 class="form-signin-heading">Account activeren</h2>
                <h4>Account is geactiveerd!</h4>
            </form>
        </div>
    </div>
@stop