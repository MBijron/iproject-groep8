@extends('layouts.master')

@section('title', 'EenmaalAndermaal')

@section('content')
    <div class="container">
        <div class="col-md-6 col-md-push-3">
            <form method="post" action="{{ action('VeilingController@giveFeedback') }}">
                <h2 class="form-signin-heading">Feedback geven</h2>
                <p>Veiling: {{ $veiling->titel }}</p>
                <div class="form-group row">
                    <label for="cijfer" class="col-md-4 col-form-label">Cijfer</label>
                    <div class="col-md-8">
                        <select id="cijfer" class="form-control" name="cijfer" required>
                            <option value="Positief">Positief</option>
                            <option value="Neutraal">Neutraal</option>
                            <option value="Positief">Negatief</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputCommentaar" class="col-md-4 col-form-label">Commentaar</label>
                    <div class="col-md-8">
                        <input class="form-control" type="text" id="inputCommentaar" name="commentaar"/>
                    </div>
                </div>
                <button class="btn btn-primary btn-block" type="submit">Feedback geven</button>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>
        </div>
    </div> <!-- /container -->