@extends('layouts.master')

@section('title', 'Bevestig activatiecode')

@section('content')

    <div class="row">
        <div class="col-md-6 col-md-push-3">
            <form>
                <h2 class="form-signin-heading">Account activeren</h2>
                <h4>Account is niet geactiveerd!</h4>
                <p>De activatie code klopt niet of is niet meer geldig.</p>
            </form>
        </div>
    </div>
@stop