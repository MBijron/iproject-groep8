@extends('layouts.master')

@section('title', 'EenmaalAndermaal')

@section('content')
<div class="container">
<div class="col-md-10">
      <form>
        <h2 class="form-signin-heading">Upgraden naar verkoop account</h2>
		<p>Als u wilt upgraden naar een verkoopaccount moet er eerst een extra verificatie stap doorlopen worden. Deze stap kan gedaan worden via creditcard of via post. Vul de onderstaande velden in.</p>
		<div class="form-group row">
		<label for="inputBank" class="col-md-2 col-form-label">Bank</label>
		<div class="col-md-4">
		<input class="form-control" type="text" id="inputBank"/>
		</div>
		<label for="inputRekeningnummer" class="col-md-2 col-form-label">Rekeningnummer</label>
		<div class="col-md-4">
		<input class="form-control" type="text" id="inputRekeningnummer"/>
		</div>
		</div>
		<div class="form-group row">
		<label class="col-md-2 col-form-label">Controle via</label>
		<label for="inputCreditcard" class="col-md-2">Creditcard</label>
		<div class="col-md-2">
		<input type="checkbox" id="inputCreditcard" class="form-control" value="creditcard"/>
		</div>
		<label for="inputPost" class="col-md-2">Post</label>
		<div class="col-md-2">
		<input type="checkbox" id="inputPost" class="form-control" value="post" />
		</div>
		</div>
		<div class="form-group row">
		<label for="inputCreditcardnummer" class="col-md-3 col-form-label">Creditcardnummer</label>
		<div class="col-md-9">
		<input class="form-control" type="text" id="inputCreditcardnummer"/>
		</div>
		</div>
        <button class="btn btn-primary btn-block" type="submit">Aanvraag verzenden</button>
      </form>
</div>
    </div> <!-- /container -->