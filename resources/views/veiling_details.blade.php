    @extends('layouts.master')

@section('title', 'veilingaanmaken')

@section('content')
<div class="row">
    <div class="col-sm-8">
		<div class="content">
			<div class="voorwerp">
				<div class="voorwerp-body">
					<div class="voorwerp-titel">
						<h3 class="content-title">{{$veiling->titel}}</h3>
					</div>
					<div class="voorwerp-rubriek">

					{!! $rubrieken !!}

					</div>
					<div class="voorwerp-afbeeldingen">
						@foreach($veiling->bestanden as $index => $bestand)
							@if($index == 0)
								<div class="voorwerp-afbeeldingen-main">
									<figure><img  src="img/veilingen/{{$veiling->verkoper->gebruikerID}}/{{$veiling->voorwerpID}}/{{$bestand->bestandsnaam}}" width="250" height="200"></figure>
								</div>
								<div class="voorwerpafbeeldingen-thumb">
								</div>
							@elseif($index%4)
								<div class="voorwerpafbeeldingen-thumb">
									<div class="thumb-placeholder">
									<figure><img  src="img/veilingen/{{$veiling->verkoper->gebruikerID}}/{{$veiling->voorwerpID}}/{{$bestand->bestandsnaam}}" width="125" height="100"></figure>
									</div>
								</div>
							@else
								<div class="thumb-placeholder">
									<figure><img src="img/veilingen/{{$veiling->verkoper->gebruikerID}}/{{$veiling->voorwerpID}}/{{$bestand->bestandsnaam}}"></figure>
								</div>
								
							@endif
						@endforeach
					</div>

					<div class="voorwerp-info">
						<div class="voorwerp-info-beschrijvingtitel clear">
							<h4 class="content-title">Beschrijving</h4>
						</div>
						<div class="voorwerp-info-beschrijving">
							<p>{{$veiling->beschrijving}}</p>
						</div>
						<div class="voorwerp-info-betaling">
							<p>Betalingswijze :  @if($veiling->betalingswijzenaam){{$veiling->betalingswijzenaam}} @else N/A @endif</p>
							<p>Betalingsinstructie: @if($veiling->betalingsinstructie){{$veiling->betalingsinstructie}} @else N/A @endif</p>
						</div>
						<div class="voorwerp-info-verzending">
							<p>Verzendwijze: @if($veiling->verzendwijzenaam){{$veiling->verzendwijzenaam}} @else N/A @endif</p>
							<p>Verzendinstructie: @if($veiling->verzendinstructies){{$veiling->verzendinstructies}} @else N/A @endif</p>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
    <div class="col-md-4">
        <div class="gebruiker">
            <div class="gebruiker-body">
                <div class="gebruiker-body-titel">
                    <h4 class="content-title">Gebruikersinformatie</h4>
                </div>

                <div class="gebruiker-body-infobox">
                    <div class="gebruiker-infobox-seg">
                        <p class="color-blue">Verkoper</p>
                        <p><a href="{{url('profiel?id='.$veiling->verkoper->gebruikerID)}}">{{$veiling->verkoper->gebruikersnaam}}</a></p>
                    </div>
                    <div class="gebruiker-infobox-seg">
                        <p class="color-blue">E-mail</p>
						@if(!$veiling->verkoper->email)
							<p>Geen email</p>
						@else
							<p>{{$veiling->verkoper->email}}</p>
						@endif
                    </div>
                    <div class="gebruiker-infobox-seg">
                        <p class="color-blue">Telefoon</p>
						@if(!$veiling->verkoper->telefoons->first())
							<p>Geen telefoonnummer</p>
						@else
							@foreach($veiling->verkoper->telefoons as $telefoon)
								<p>{{$telefoon->telefoonnummer}}</p>
							@endforeach
						@endif
                    </div>
                    <div class="gebruiker-infobox-seg">
                        <p class="color-blue">Voorwerplocatie</p>
						@if(!$veiling->verkoper->plaatsnaam)
							<p>Geen voorwerplocatie</p>
						@else
							<p>{{$veiling->verkoper->plaatsnaam}}</p>
						@endif
                    </div>
                </div>
            </div>
        </div>
            <div class="biedingen">
                <div class="biedingen-body">
                    <div class="biedingen-body-titel">
                        <h4 class="content-title">Bieden</h4>
                    </div>
                    <div class="biedingen-body-input">
                        <form method="post" action="{{ action('VeilingController@addBod')}}"}>
                            <input type="hidden" name="veilingID" value="{{$veiling->voorwerpID}}" id="veilingID">
                            <p class="content-titel">Voorwerp: {{$veiling->titel}}</p>
							<div class="row">
								<div class="col-md-6 no-padding">
									<div class="input-group">
									<span class="input-group-addon">&euro;</span>
									<input type="number" value="{{$veiling->boden->max('bedrag') + 5.00}}" min="{{$veiling->boden->max('bedrag') + 5.00}}" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" id="bod" name="bod" />
									</div>
									@if (count($errors) > 0)
										<div class="alert alert-danger">
											<ul>
												@foreach ($errors->all() as $error)
													<li>{{ $error }}</li>
												@endforeach
											</ul>
										</div>
									@endif
								</div>
								<div class="col-md-6 no-padding">
									<div><input type="submit" value="Plaats bod" class="btn btn-orange"></div>
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
								</div>
							</div>
                        </form>
                    </div>
                    <div style="padding-left: 0;text-align: center;padding-bottom: 20px;">
                        <p>Deze veiling eindigt in: <span class="bod-countdown">{{date('Y-m-d',strtotime($veiling->datum.'+'.$veiling->dagen.' day'))}}T{{date('H:i:s',strtotime($veiling->tijd))}}Z</span></p>
						<p>Startprijs: &euro; {{$veiling->startprijs}}</p>
                    </div>
                    <div class="row">
                        <table class="table-striped table-scroll" >
                            <thead>
                            <tr>
                                <th>Gebruiker</th>
                                <th>Bod</th>
                                <th>Datum</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($veiling->boden as $bod)
                                <tr>
                                    <td class="filterable-cell">{{$bod->gebruiker->gebruikersnaam}}</td>
                                    <td class="filterable-cell">&euro; {{$bod->bedrag}}</td>
                                    <td class="filterable-cell">{{$bod->datum}} {{date('H:i:s',strtotime($veiling->tijd))}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</div>

@stop