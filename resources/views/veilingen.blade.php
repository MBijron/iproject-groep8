@extends('layouts.master')

@section('title', 'Zoekresultaten')

@section('content')
    <div class="row">
        <div class="list-group col-md-3">
            {!! $rubrieken !!}
        </div>

        <div class="row">
        <div class="col-md-9 content"><!-- Hier zijn de rubrieken! -->
            <div class="image-group">

                <h4><div class="rubriektitel"><a href="#" class="content-title">Zoekresultaten</a></div></h4>
            {!! $veilingen->render() !!}
                <!--<div class="row"> -->
                @foreach($veilingen as $index => $veiling)
                    @if($index%4 == 0)
                        <div class="row">
                            @endif
                            <div class="col-sm-3 ">
                                <div class="showcase-item-body">
										<a class="showcase-item-clickable-container" href="veiling_details?id={{$veiling->voorwerpID}}">
											@if(!$veiling->bestanden->first())
												<figure><img src="img/veilingen/no_image.png" width="200" height="200"></figure>
											@else
												<figure><img src="img/veilingen/{{$veiling->gebruikerID}}/{{$veiling->voorwerpID}}/{{$veiling->bestanden->first()->bestandsnaam}}" width="200" height="200"></figure>
											@endif
											<div class="showcase-item-card-content">
												<h5 class="showcase-item-title">{{$veiling->titel}}</h5>
												@if(!$veiling->boden->max('bedrag'))
													<span class="prijs">Huidig bod: &euro; 0.00</span><br>
												@else
												<span class="prijs">Huidig bod: &euro; {{$veiling->boden->max('bedrag')}}</span><br>
												@endif
												<span class="info">Bieding eindigt: </span>
												<br />
												<span class="bod-countdown">{{date('Y-m-d',strtotime($veiling->datum.'+'.$veiling->dagen.' day'))}}T{{date('H:i:s',strtotime($veiling->tijd))}}Z</span>
											</div>
										</a>
										<a href="bodaanmaken?id={{$veiling->voorwerpID}}"> <div class="btn-block btn btn-orange btn-no-edge">Bied mee</div></a>
									</div>
                            </div>
                            @if($index%4 == 3)
                        </div>
                    @endif
                @endforeach

            </div>
            {!! $veilingen->render() !!}
        </div>
    </div>
    </div>
@stop