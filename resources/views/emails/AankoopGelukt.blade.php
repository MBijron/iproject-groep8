<h1>Aankoop gelukt</h1>
<p>Hallo, {{$koper->voornaam." ".$koper->achternaam}}</p>
<p>De aankoop van de veiling "{{$veiling->titel}}" is gelukt.</p>
<p>Om feedback te geven ga dan aub naar de onderstaande link.</p>
<p><a href="{{ url('/')."/feedback?id=".$veiling->voorwerpID }}">{{  url('/')."/feedback?id=".$veiling->voorwerpID }}</a></p>