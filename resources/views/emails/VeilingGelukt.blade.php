<h1>Verkoop gelukt</h1>
<p>De verkoop van de veiling "{{$veiling->titel}}" is gelukt.</p>
<p>De koper is {{$koper->voornaam." ".$koper->achternaam." email:".$koper->email }}</p>
<p>Om feedback te geven ga dan aub naar de onderstaande link.</p>
<p><a href="{{ url('/')."/feedback?id=".$veiling->voorwerpID }}">{{  url('/')."/feedback?id=".$veiling->voorwerpID }}</a></p>