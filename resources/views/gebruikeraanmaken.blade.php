@extends('layouts.master')

@section('title', 'Gebruiker registreren')

@section('content')
<div class="content">
    <div class="row">

        <form method="post" action="{{ action('GebruikerController@register') }}">
            <h2 class="form-signin-heading">Account aanmaken</h2>
            <p>Invoer gegevens</p>
            <input type="hidden" name="id" value="{{$id}}">
            <div class="form-group row">
                <label for="inputGebruikersnaam" class="col-md-2 col-form-label">Gebruikersnaam</label>
                <div class="col-md-4">
                    <input class="form-control" type="text" id="inputGebruikersnaam" name="gebruikersnaam"/>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputWachtwoord" class="col-md-2 col-form-label">Wachtwoord</label>
                <div class="col-md-4">
                    <input class="form-control" type="password" id="inputWachtwoord" name="password"/>
                </div>
                <label for="inputHerhaal" class="col-md-2 col-form-label">Herhaal wachtwoord</label>
                <div class="col-md-4">
                    <input class="form-control" type="password" id="inputHerhaal" name="herhaal"/>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputVoornaam" class="col-md-2 col-form-label">Voornaam</label>
                <div class="col-md-4">
                    <input class="form-control" type="text" id="inputVoornaam" name="voornaam"/>
                </div>
                <label for="inputHerhaal" class="col-md-2 col-form-label">Tussenvoegsel</label>
                <div class="col-md-4">
                    <input class="form-control" type="text" id="inputTussenvoegsel" name="tussenvoegsel"/>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputAchternaam" class="col-md-2 col-form-label">Achternaam</label>
                <div class="col-md-4">
                    <input class="form-control" type="text" id="inputAchternaam" name="achternaam"/>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputAdres" class="col-md-2 col-form-label">Adres</label>
                <div class="col-md-4">
                    <input class="form-control" type="text" id="inputAdres" name="adres"/>
                </div>
                <label for="inputPostcode" class="col-md-2 col-form-label">Postcode</label>
                <div class="col-md-4">
                    <input class="form-control" type="text" id="inputPostcode" name="postcode"/>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputLand" class="col-md-2 col-form-label">Land</label>
                <div class="col-md-4">
                    <input class="form-control" type="text" id="inputLand" name="land"/>
                </div>
                <label for="inputPlaats" class="col-md-2 col-form-label">Plaats</label>
                <div class="col-md-4">
                    <input class="form-control" type="text" id="inputPlaats" name="plaats"/>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputTelefoon" class="col-md-2 col-form-label">Telefoon</label>
                <div class="col-md-4">
                    <input class="form-control" type="text" id="inputTelefoon" maxlength="10"/>
                </div>
                <div class="col-md-4">
                    <div class="btn btn-orange" id="btnAddPhone">Telefoon toevoegen</div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-10 col-md-push-2">
                    <select name="lstTelefoons[]" class="form-control" id="lstTelefoons" multiple required readonly>

                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputGebdatum" class="col-md-2 col-form-label">Geboortedatum</label>
                <div class="col-md-4">
                    <input class="form-control" type="date" id="inputGebdatum" name="gebdatum" required/>
                </div>
            </div>
            <div class="form-group row">
				<label for="lstVragen" class="col-md-2 col-form-label">Herstelvraag</label>
                <div class="col-md-10">
                        <select id="lstVragen" class="form-control" name="lstVragen" required>
                            @foreach($vragen as $vraag)
                            <option value="{{$vraag->vraagID}}">{{$vraag->tekst}}</option>
                            @endforeach
                        </select>
                </div>
            </div>
            <div class="form-group row">
                        <label for="antwoord" class="col-md-2 col-form-label">Herstelvraag</label>
                <div class="col-md-10">
                        <input class="form-control" type="text" id="antwoord" name="antwoord" required>
                </div>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <button class="btn btn-primary btn-block" type="submit">Aanvraag verzenden</button>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>
</div>
    <script>
        $(document).ready(function(){
            $( "#btnAddPhone" ).click(function() {
                var phone = $('#inputTelefoon').val();
                var selectObject = $('#lstTelefoons');
                if (selectObject.find('option[value="'+phone +'"]').length > 0) {
                    return false;
                }
                $('#lstTelefoons').append($('<option>', {
                    value: phone,
                    text: phone,
                    selected: true
                }));
            });

        });
        $( function() {
            $( "#inputGebdatum" ).datepicker();
        } );
    </script>

@stop