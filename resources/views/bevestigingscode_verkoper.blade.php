@extends('layouts.master')

@section('title', 'EenmaalAndermaal')

@section('content')
<div class="content" style="overflow: hidden">
	<div class="col-md-6 col-md-push-3">
		<form method="post" action="{{ action('VerkoperController@activate') }}">
			<h2 class="form-signin-heading">Upgraden naar verkoop account</h2>
			<p>Als u wilt upgraden naar een verkoopaccount moet er eerst een extra verificatie stap doorlopen worden. Deze stap kan gedaan worden via creditcard of via post. Vul de onderstaande velden in.</p>
			<div class="form-group row">
				<label for="inputBevestigingscode" class="col-md-4 col-form-label">Bevestigingscode</label>
				<div class="col-md-8">
					<input class="form-control" type="text" id="inputBevestigingscode" name="bevestigingscode"/>
				</div>
			</div>
			  @if (count($errors) > 0)
				  <div class="alert alert-danger">
					  <ul>
						  @foreach ($errors->all() as $error)
							  <li>{{ $error }}</li>
						  @endforeach
					  </ul>
				  </div>
			  @endif
			<button class="btn btn-primary btn-block" type="submit">Account bevestigen</button>
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		</form>
	</div>
</div>
@stop