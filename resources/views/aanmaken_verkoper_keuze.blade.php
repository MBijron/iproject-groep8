@extends('layouts.master')

@section('title', 'EenmaalAndermaal')

@section('content')
	
		<div class="row">
	<div class="container">
	<h2 class="form-signin-heading">Upgraden naar verkoop account</h2>
		
	
<div class="col-md-5 list-group">
		<a href="verkoperaanmaken" class="list-group-item">Nieuwe aanvraag maken voor een verkoopaccount.</a>
</div>
<div class="col-md-5 ">

		<a href="bevestigingscode_verkoper" class="list-group-item">Bevestigingscode invullen</a>
</div>
    </div> <!-- /container -->
		</div>
@stop