<html>
    <head>
        <title>@yield('title')</title>
		<link rel="stylesheet" type="text/css" href="css/main.css">

		<script src="{{ URL::asset('js/jquery-3.1.1.min.js') }}"></script>
		<script src="{{ URL::asset('js/jquery.plugin.js') }}"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script src="{{ URL::asset('js/jquery.countdown.min.js') }}"></script>
		<script src="{{ URL::asset('js/jquery.countdown-nl.js') }}"></script>
		<script src="{{ URL::asset('js/main.js') }}"></script>
    </head>
    <body>
        @section('sidebar')
            <div class="navbar navbar-default navbar-fixed-top">
				<div class="container-fluid">
					<div class="navbar-header">
						<a href="../public" class="navbar-brand"><img src="img/hamer2.png" width="25" height="25"><span style="color: #337ab7;">EenmaalAndermaal</span></a>
						<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						</button>
					</div>
					<div class="navbar-collapse collapse" id="navbar-main">
						<div class="nav navbar-nav">
							<form class="navbar-form" role="search" method="get" files="true" action="{{ action('ZoekController@create')}}">
								<input type="text" class="form-control" placeholder="Zoekterm" name="zoekterm" id="zoekterm" required>
								<select name="rubriek" id="rubriek" class="form-control">
									@foreach($masterpagerubrieken as $rubriek)
									@if($rubriek->rubriekID == -1)
										<option value="{{$rubriek->rubriekID}}">Alle rubrieken</option>
									@else
										<option value="{{$rubriek->rubriekID}}">{{$rubriek->rubrieknaam}}</option>
									@endif
									@endforeach
								</select>
								<Button class="btn btn-default" type="submit">Zoek</Button>
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
							</form>
						</div>

						<ul class="nav navbar-nav navbar-right">
							@if(! empty(Auth::user()->gebruikerID))
								<li><a href="{{url('profiel?id='.Auth::user()->gebruikerID)}}">Welkom, {{ Auth::user()->voornaam}}</a></li>
								<li><a href="logout">logout</a></li>
							@endif
								@if(empty(Auth::user()->verkoper) && ! empty(Auth::user()->gebruikerID))
										<li><a href="aanmaken_verkoper_keuze">Aanmelden als verkoper</a> </li>
								@endif
								@if(!empty(Auth::user()->verkoper))
										<li><a href="veilingaanmaken">Veiling aanmaken</a></li>
								@endif
								@if( empty(Auth::user()->gebruikerID))
									<li><a href="login">Login/Registreer</a></li>
								@endif
							@if(empty(Auth::user()->activated))
								{{Auth::logout()}}
								@endif
						</ul>
					</div>
				</div>
			</div>
		<main>
        <div class="container-fluid">
            @yield('content')
        </div>
		</main>
			<footer>
	<div class="container">
		<a href="{{ url('privacybeleid') }}">Privacybeleid</a>
		<p class="disclaimer">EenmaalAndermaal is niet aanspraakelijk voor (gevolg)schade die voorkomt uit het gebruik van deze site, dan wel uit fouten of ontbrekende functionaliteiten op deze site.</p>
		<p class="copyright">Copyright &copy; 2016 EenmaalAndermaal. Alle rechten voorbehouden.</p>
	</div>
</footer>
    </body>
</html>