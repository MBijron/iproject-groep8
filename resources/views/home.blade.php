@extends('layouts.master')

@section('title', 'EenmaalAndermaal')

@section('content')
    
		 <div class="row">
			<div class="list-group col-md-3"><!-- Dit is de verticale menubalk!-->
			<!--	<a href="#" class="list-group-item active">Rubrieken</a>
				<a href="#" class="list-group-item">Kleding</a>
				<a href="#" class="list-group-item">Huishoudmiddelen</a>
				<a href="#" class="list-group-item">Auto's</a>-->

				{!! $rubrieken !!}

			</div>

			<div class="col-md-9 content"><!-- Hier zijn de rubrieken! -->



					<div class="image-group">
						<h4><div class="rubriektitel"><a href="#nieuwenpopulair" class="content-title">Nieuw en populair</a></div></h4>
						<!--<div class="row"> -->
							@foreach($veilingen as $index => $veiling)
								@if($index%4 == 0)
									@if($index != 0)
										</div>
									@endif
								<div class="row">
								@endif

								<div class="col-sm-3">
									<div class="showcase-item-body">
										<a class="showcase-item-clickable-container" href="veiling_details?id={{$veiling->voorwerpID}}">
											@if(!$veiling->bestanden->first())
												<figure><img src="img/veilingen/no_image.png" width="200" height="200"></figure>
											@else
												<figure><img src="img/veilingen/{{$veiling->gebruikerID}}/{{$veiling->voorwerpID}}/{{$veiling->bestanden->first()->bestandsnaam}}" width="200" height="200"></figure>
											@endif
											<div class="showcase-item-card-content">
												<h5 class="showcase-item-title">{{$veiling->titel}}</h5>
												@if(!$veiling->boden->max('bedrag'))
													<span class="prijs">Huidig bod: &euro; 0.00</span><br>
												@else
												<span class="prijs">Huidig bod: &euro; {{$veiling->boden->max('bedrag')}}</span><br>
												@endif
												<span class="info">Bieding eindigt: </span>
												<span class="bod-countdown">{{date('Y-m-d',strtotime($veiling->datum.'+'.$veiling->dagen.' day'))}}T{{date('H:i:s',strtotime($veiling->tijd))}}Z</span>
											</div>
										</a>
										<a href="veiling_details?id={{$veiling->voorwerpID}}"> <div class="btn-block btn btn-orange btn-no-edge">Bied mee</div></a>
									</div>
								</div>
							@endforeach

					<!--{{--		<div class="col-sm-3">
								<div class="showcase-item-body">
									<a class="showcase-item-clickable-container" href="#">
										<figure><img src="img/uitlaat2.jpg" width="200" height="200"></figure>
										<div class="showcase-item-card-content">
											<h5 class="showcase-item-title">Grote uitlaat voor autooooooo</h5>
											<span class="prijs">Huidig bod: � 52,00</span><br>
											<span class="info">Bieding eindigt: 30-11-2016</span>
											<button class="btn-block btn btn-orange">Bied mee</button>
										</div>
									</a>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="showcase-item-body">
									<a class="showcase-item-clickable-container" href="veiling_details">
										<figure><img src="img/shirt1.jpg" width="200" height="200"></figure>
										<div class="showcase-item-card-content">
											<h5 class="showcase-item-title">Shirt voor mannen</h5>
											<span class="prijs">Huidig bod: � 9,00</span><br>
											<span class="info">Bieding eindigt: 30-11-2016</span>
											<button class="btn-block btn btn-orange">Bied mee</button>
										</div>
									</a>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="showcase-item-body">
									<a class="showcase-item-clickable-container" href="#">
										<figure><img src="img/shirt3.jpg" width="200" height="200"></figure>
										<div class="showcase-item-card-content">
											<h5 class="showcase-item-title">Shirt voor vrouwen</h5>
											<span class="prijs">Huidig bod: � 17,00</span><br>
											<span class="info">Bieding eindigt: 30-11-2016</span>
											<button class="btn-block btn btn-orange">Bied mee</button>
										</div>
									</a>
								</div>
							</div>--}}

					</div>
	{{--				<div class="image-group clear">
						<h4><div class="rubriektitel"><a href="#kleding" class="content-title">Kleding</a></div></h4>
						<div class="row">
							<div class="col-sm-3">
								<div class="showcase-item-body">
									<a class="showcase-item-clickable-container" href="#">
										<figure><img src="img/shirt2.jpg" width="200" height="200"></figure>
										<div class="showcase-item-card-content">
											<h5 class="showcase-item-title">Mooi shirt voor de mannen</h5>
											<span class="prijs">Huidig bod: � 20,00</span><br>
											<span class="info">Bieding eindigt: 30-11-2016</span>
											<button class="btn-block btn btn-orange">Bied mee</button>
										</div>
									</a>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="showcase-item-body">
									<a class="showcase-item-clickable-container" href="#">
										<figure><img src="img/broek1.jpg" width="200" height="200"></figure>
										<div class="showcase-item-card-content">
											<h5 class="showcase-item-title">lekker broekie</h5>
											<span class="prijs">Huidig bod: � 102,00</span><br>
											<span class="info">Bieding eindigt: 30-11-2016</span>
											<button class="btn-block btn btn-orange">Bied mee</button>
										</div>
									</a>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="showcase-item-body">
									<a class="showcase-item-clickable-container" href="#">
										<figure><img src="img/shirt1.jpg" width="200" height="200"></figure>
										<div class="showcase-item-card-content">
											<h5 class="showcase-item-title">Shirt voor mannen</h5>
											<span class="prijs">Huidig bod: � 9,00</span><br>
											<span class="info">Bieding eindigt: 30-11-2016</span>
											<button class="btn-block btn btn-orange">Bied mee</button>
										</div>
									</a>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="showcase-item-body">
									<a class="showcase-item-clickable-container" href="#">
										<figure><img src="img/shirt3.jpg" width="200" height="200"></figure>
										<div class="showcase-item-card-content">
											<h5 class="showcase-item-title">Shirt voor vrouwen</h5>
											<span class="prijs">Huidig bod: � 17,00</span><br>
											<span class="info">Bieding eindigt: 30-11-2016</span>
											<button class="btn-block btn btn-orange">Bied mee</button>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>--}}-->
				</div>
			</div>
@stop