@extends('layouts.master')

@section('title', 'EenmaalAndermaal')

@section('content')

<div class="content">
	<div class="row">
		<div class="col-md-6 col-md-push-3">
			<h2 class="form-signin-heading">De pagina kan niet worden gevonden</h2>
			<a href="{{url('/')}}"><p>terug naar de voorpagina</p></a>
		</div> <!-- /container -->
	</div>
</div>
@stop
