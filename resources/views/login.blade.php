@extends('layouts.master')

@section('title', 'EenmaalAndermaal')

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <form class="form-signin" role="form" method="POST" action="{{ url('/login') }}">
				{{ csrf_field() }}
			
                <h2 class="form-signin-heading content-title">Inloggen</h2>
                <div class="loginbox">
				
                    <label for="inputEmail" class="sr-only">E-mail</label>
                    <input id="inputEmail" class="form-control" placeholder="Email address" type="email" value="{{ old('email') }}" name="email" required autofocus>
					@if ($errors->has('email'))
						<span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
					
                    <label for="inputPassword" class="sr-only">Wachtwoord</label>
                    <input id="inputPassword" class="form-control" placeholder="Wachtwoord" name="password" type="password" required>
					@if ($errors->has('password'))
						<span class="help-block">
							<strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    @if (session('warning'))
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    @endif
					
                    <button type="submit" class="btn btn-lg btn-primary">Login</button>
                    <div class="vergeten">
                        <a href="{{ url('/resetpassword') }}">Wachtwoord vergeten?</a>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-6">
            <div class="registreer">
                <h2 class="form-signin-heading content-title">Registreer</h2>
                <div class="registreerblok">
                    <div class="registreertekst">
                        <div>Om in te loggen heb je een account nodig. Je kunt dan voortaan:</div>
                        <ul>
                            <li>Je eigen veiling starten</li>
                            <li>Bieden op andermans veilingen</li>
                        </ul>
                    </div>
                    <a href="{{ url('/aanmelden_gebruiker') }}" class="btn btn-lg btn-primary">Nieuw account aanmaken</a>
                </div>
            </div>
        </div>
    </div>
@stop