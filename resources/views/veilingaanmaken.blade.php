@extends('layouts.master')

@section('title', 'Veiling aanmaken')

@section('content')
<div class="content">
    <form class="veilingaanmaken" method="post" files="true" action="{{ action('VeilingController@addVeiling')}}" enctype="multipart/form-data">
        <h2 class="form-signin-heading content-title">Veiling aanmaken</h2>
		<div class="form-group row">
            <label for="inputTitel" class="col-md-2 col-form-label">Titel</label>
            <div class="col-md-10">
                <input class="form-control" type="text" id="inputTitel" name="titel"/>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputRubriek1" class="col-md-2 col-form-label">Rubriek 1</label>
            <div class="col-md-4">
                <select class="form-control" id="inputRubriek1" name="rubriek1" required>
                    @foreach($rubrieken as $rubriek)
                    <option value="{{$rubriek->rubriekID}}">{{$rubriek->rubrieknaam}}</option>
                    @endforeach
                </select>
            </div>
            <label for="inputRubriek2" class="col-md-2 col-form-label">Rubriek 2</label>
            <div class="col-md-4">
                <select class="form-control" id="inputRubriek2" name="rubriek2" required>
                    @foreach($rubrieken as $rubriek)
                        <option value="{{$rubriek->rubriekID}}">{{$rubriek->rubrieknaam}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputStartprijs" class="col-md-2 col-form-label">Startprijs</label>
            <div class="col-sm-4">
                <div class="form-row">
                    <div class="input-group">
                        <span class="input-group-addon">&euro;</span>
                        <input type="number" value="0" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" id="c2" name="min" />
                    </div>
                </div>
            </div>
            <label for="inputLooptijd" class="col-md-2 col-form-label">Looptijd</label>
            <div class="col-md-4">
                <select class="form-control" id="inputLooptijd" name="looptijd" required>
                        <option value="1">1 dag</option>
                        <option value="3">3 dagen</option>
                        <option value="5">5 dagen</option>
                        <option value="7">7 dagen</option>
                        <option value="10">10 dagen</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="inputPlaats" class="col-md-2 col-form-label">Plaats</label>
            <div class="col-md-4">
                <input class="form-control" type="text" id="inputPlaats" name="plaats"/>
            </div>
            <label for="inputLand" class="col-md-2 col-form-label">Land</label>
            <div class="col-md-4">
                <input class="form-control" type="text" id="inputLand" name="land"/>
            </div>
        </div>

        <div class="form-group row">
            <label for="inputBetalingswijze" class="col-md-2 col-form-label">Betalingswijze</label>
            <div class="col-md-4">
                <select class="form-control" id="inputBetalingswijze" name="betalingswijze" required>
                    <option value="Contant">Contant</option>
                    <option value="IDEAL">IDEAL</option>
                    <option value="Creditcard">Creditcard</option>
                </select>
            </div>
            <label for="inputBetalingsinstructie" class="col-md-2 col-form-label">Betalingsinstructie</label>
            <div class="col-md-4">
                <input class="form-control" type="text" id="inputBetalingsinstructie" name="betalingsinstructie"/>
            </div>
        </div>

        <div class="form-group row">
            <label for="inputVerzendkosten" class="col-md-2 col-form-label">Verzendkosten</label>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">&euro;</span>
					<input type="number" value="0" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" id="c2" name="verzendkosten" />
				</div>
			</div>
        </div>
        <div class="form-group row">
        <label for="inputVerzendinstructie" class="col-md-2 col-form-label">Verzendinstructie</label>
        <div class="col-md-10">
            <input class="form-control" type="text" id="inputVerzendinstructie" name="verzendinstructie"/>
        </div>
        </div>
        <div class="form-group row">
            <label for="inputBeschrijving" class="col-md-2 col-form-label">Beschrijving</label>
            <div class="col-md-10">
                <textarea rows="4" placeholder="Beschrijving" required class="form-control" name="beschrijving"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputSave" class="col-md-2 col-form-label">Selecteer afbeeldingen</label>
            <div class="col-md-10">
                <input type="file" name="afbeeldingen[]" multiple />
                {{--{!! Form::file('afbeeldingen[]', array('class' => 'form-control')) !!}--}}
                {{--<input type="button" value="Upload afbeelding" onclick="document.getElementById('fileInput').click();" class="form-control" />--}}
            </div>
        </div>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <button class="btn btn-primary btn-block" type="submit">Veiling aanmaken</button>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </form>
</div>
@stop