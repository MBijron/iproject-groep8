<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\test;
use App\Http\Middleware\testWare;

Route::get('login', function () {
    return view('login');
})->name('login');
Route::get('logout', function () {
    Auth::logout();
    return Redirect::to('/');
})->name('logout');

//Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('login',
    ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
Route::post('logout',
    ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
//Route::post('/login', 'Auth\LoginController@test')->name('login');

Route::get('/resetpassword', function() {
	return view('reset');
})->name('resetpassword');

Route::get('profiel', 
	['as' => 'profiel', 'uses' => 'ProfielController@create']);
Route::get('/aanmaken_verkoper_keuze', function () {
    return view('aanmaken_verkoper_keuze');
})->name('aanmaken_verkoper_keuze');
Route::get('privacybeleid', function () {
    return view('privacybeleid');
})->name('privacybeleid');
Route::get('/aanmelden_gebruiker', function () {
    return view('aanmelden_gebruiker');
})->name('aanmelden_gebruiker');
Route::get('bevestigingscode_verkoper', function () {
    return view('bevestigingscode_verkoper');
})->name('bevestigingscode_verkoper');

Route::get('verkoperaanmaken',
    ['as' => 'verkoperaanmaken', 'uses' => 'VerkoperController@create'])->middleware('auth');
Route::get('/',
    ['as' => 'home', 'uses' => 'HomeController@create']);
Route::get('zoekresultaat',
    ['as' => 'zoekresultaat', 'uses' => 'ZoekController@create']);
Route::get('gebruikeraanmaken',
    ['as' => 'gebruikeraanmaken', 'uses' => 'GebruikerController@create']);
Route::post('gebruikeraanmaken',
    ['as' => 'gebruikeraanmaken', 'uses' => 'GebruikerController@register']);
Route::get('verkoperaanmaken',
    ['as' => 'verkoperaanmaken', 'uses' => 'VerkoperController@create']);
Route::post('verkoperaanmaken',
    ['as' => 'verkoperaanmaken', 'uses' => 'VerkoperController@register']);
Route::get('/registreeralsgebruiker', function () {
    return view('registreeralsgebruiker');
})->name('/registreeralsgebruiker');
Route::post('aanmelden_gebruiker',
    ['as' => 'aanmelden_gebruiker', 'uses' => 'GebruikerController@insertEmail']);
Route::get('bevestigingscode_gebruiker',
    ['as' => 'bevestigingscode_gebruiker', 'uses' => 'GebruikerController@Activate']);
Route::get('veiling_details',
    ['as' => 'veiling_details', 'uses' => 'VeilingController@create']);
Route::get('bodaanmaken',
    ['as' => 'bodaanmaken', 'uses' => 'VeilingController@createBodView'])->middleware('auth');
Route::post('bodaanmaken',
    ['as' => 'bodaanmaken', 'uses' => 'VeilingController@addBod']);
Route::get('veilingaanmaken',
    ['as' => 'veilingaanmaken', 'uses' => 'VeilingController@createVeilingView'])->middleware('auth');
Route::post('veilingaanmaken',
    ['as' => 'veilingaanmaken', 'uses' => 'VeilingController@addVeiling']);
Route::get('veilingen',
    ['as' => 'veilingen', 'uses' => 'Zoekcontroller@showRubriek']);

Route::post('bevestigingscode_verkoper',
    ['as' => 'bevestigingscode_verkoper', 'uses' => 'VerkoperController@activate']);
Route::get('feedback',
    ['as' => 'feedback', 'uses' => 'VeilingController@feedback'])->middleware('auth');;
Route::post('feedback',
    ['as' => 'feedback', 'uses' => 'VeilingController@giveFeedback']);