<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class test extends Model
{
    protected $table = "test";
	public $timestamps = false;
	
	public function test2()
	{
		return $this->hasMany('App\test2', 'testID', 'testID');
	}
}
