<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bestand extends Model
{
    public $table = 'Bestand';
    public $primaryKey = 'bestandID';
    public $timestamps = false;
}
