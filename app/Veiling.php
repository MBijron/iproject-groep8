<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Veiling extends Model
{
    public $table = 'Voorwerp';
    public $primaryKey = 'voorwerpID';
    public $timestamps = false;
	protected $keyType = "bigint";


    public function bestanden()
    {
        return $this->hasMany('App\Bestand', 'voorwerpID', 'voorwerpID');
    }
    public function boden()
    {
        return $this->hasMany('App\Bod', 'voorwerpID');
    }
    public function rubrieken()
    {
        return $this->belongsToMany('App\Rubriek', 'voorwerp_rubriek', 'voorwerpID', 'rubriekID');
    }
    public function verkoper() {
        return $this->hasOne('App\Gebruiker', 'gebruikerID', 'gebruikerID');
    }
}
