<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bod extends Model
{
    public $table = 'Bod';
    public $primaryKey = 'bodID';
    public $timestamps = false;
    public function gebruiker() {
        return $this->hasOne('App\Gebruiker', 'gebruikerID', 'gebruikerID');
    }
}
