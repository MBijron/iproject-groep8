<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telefoon extends Model
{
    public $table = 'Gebruikerstelefoon';
    public $primaryKey = 'gebruikerstelefoonID';
    public $timestamps = false;
}
