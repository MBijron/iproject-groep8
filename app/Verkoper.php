<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 9-1-2017
 * Time: 15:03
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Verkoper extends Model
{
    public $table = 'Verkoper';
    public $primaryKey = 'verkoperID';
    public $timestamps = false;

    public function gebruiker()
    {
        return $this->hasOne('App\Gebruiker', 'gebruikerID', 'gebruikerID');
    }
}