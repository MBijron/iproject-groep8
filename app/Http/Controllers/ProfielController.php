<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feedback;
use App\Veiling;
use App\Gebruiker;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Query\Expression as raw;

class ProfielController extends Controller
{
	public function create(Request $request) {
		if(empty(Gebruiker::find($request->input('id')))){
			return Response::view('404', array(), 404);
		}
		
		$id = $request->input('id');
		
		$gebruiker = Gebruiker::where('gebruikerID', $id)->first();
		
		$feedback = Feedback::where('gebruikerID', $id)->get();
		
		$veilingen = Veiling::where('gebruikerID', $id)->where('gesloten', 0)->get();
		
		return view('profiel')->with('allfeedback', $feedback)->with('veilingen', $veilingen)->with('gebruiker', $gebruiker);
	}
}
