<?php

namespace App\Http\Controllers;

use App\Gebruiker;
use App\Mail\ConfirmGebruiker;
use App\Verkoper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use League\Flysystem\Exception;

class VerkoperController extends Controller
{
    public function create()
    {
        if(empty(Auth::user()->verkoper)) {
            Redirect::to('/');
        }
        return view('verkoperaanmaken');
    }
    public function register(Request $request)
    {
        if(!empty(Gebruiker::find(Auth::user()->gebruikerID)->verkoperObj)) {
            return Redirect::back()->withInput($request->input())->withErrors(['U bent al een verkoper']);
        }
        $verkoper = new Verkoper();
        $verkoper->gebruikerID = Auth::user()->gebruikerID;
        $verkoper->controleoptie = $request->keuze;

        if($verkoper->controleoptie == "post") {
            if(empty($request->rekeningnr) || empty($request->bank)) {
                return Redirect::back()->withInput($request->input())->withErrors(['Vul een rekening nummer en een banknaam in.']);
            }
            $verkoper->banknaam = $request->bank;
            $verkoper->rekeningnummer = $request->rekeningnr;
        }
        elseif($verkoper->controleoptie == "creditcard") {
            if(empty($request->creditcardnr)) {
                return Redirect::back()->withInput($request->input())->withErrors(['Vul een creditcard nummer in.']);
            }
            $verkoper->creditcardnummer = $request->creditcardnr;
        }
        else {
            return Redirect::back()->withInput($request->input())->withErrors(['U kiezen tussen post of creditcard.']);
        }
        $verkoper->save();
        $gebruiker = Gebruiker::find($verkoper->gebruikerID);
        $gebruiker->verkoper = 0;
        $gebruiker->save();
        echo '<script language="javascript">';
        echo 'alert("Account upgrade is aangevraagd!")';
        echo '</script>';
        return Redirect::to('/');
    }
    public function activate(Request $request) {
        $activatiecode = $request->bevestigingscode;
            if(Verkoper::where('activatiecode', $activatiecode)->where('activated', 0)->get()->count() > 0) {
                $verkoper = Verkoper::where('activatiecode', $activatiecode)->where('gebruikerID', Auth::user()->gebruikerID)->first();
                $verkoper->activated = 1;
                $verkoper->save();
                $gebruiker = Gebruiker::find($verkoper->gebruikerID);
                $gebruiker->verkoper = 1;
                $gebruiker->save();
                echo '<script language="javascript">';
                echo 'alert("Account is successvol geupgrade!")';
                echo '</script>';
                return Redirect::to('/');
            }
            else{
                return Redirect::back()->withInput($request->input())->withErrors(['Activatie code klopt niet.']);
            }

    }
}
