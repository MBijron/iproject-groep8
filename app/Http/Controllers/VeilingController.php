<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\Http\Requests\VeilingAanmakenRequest;
use App\Rubriek;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Veiling;
use App\Bod;
use App\Bestand;
use App\Veiling_Rubriek;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rules\In;
use League\Flysystem\Exception;
use Illuminate\Http\RedirectResponse;
use phpDocumentor\Reflection\Types\Array_;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Http\Traits\PrintRubrieken;
use Illuminate\Support\Facades\Response;

class VeilingController extends Controller
{
	use PrintRubrieken;

    public function create(Request $request) {
        if(empty(Veiling::find($request->input('id')))){
            Redirect::to('/');
        }
        $id = $request->input('id');
        $veiling = Veiling::find($id);
		if(!isset($veiling))
		{
			return Response::view('404', array(), 404);
		}
		$rubriekdata = $this->generateRubriekLine($veiling->rubrieken()->first());
        return view('veiling_details')->with('veiling', $veiling)->with('rubrieken', $rubriekdata);
    }
    public function createBodView(Request $request) {
        $id = $request->input('id');
        try {
            $veiling = Veiling::findOrFail($id);
            return view('bodaanmaken')->with('veiling', $veiling);
        }
        catch (QueryException $ex) {
            echo '<script language="javascript">';
            echo 'alert("'.$ex->getMessage().'")';
            echo '</script>';
            return Redirect::to('/');
        }
    }
    public function addBod(Request $request) {

        if(Veiling::find($request->veilingID)) {
            try {
            $veiling = Veiling::find($request->veilingID);
            if(Auth::user()->gebruikerID == $veiling->gebruikerID){
                return Redirect::back()->withInput($request->input())->withErrors(["U mag niet bieden op uw eigen veiling."]);
            }
            $max = Bod::where('voorwerpID', $veiling->voorwerpID)->max('bedrag');
            $min = $veiling->startprijs;
            if($request->input('bod') < $max || $request->input('bod') < $min){
                return Redirect::back()->withInput($request->input())->withErrors(["Bedrag mag niet minder zijn dan het huidige bod of minder zijn dan de startprijs"]);
            }
            $bod = new Bod();
            $bod->voorwerpID = $veiling->voorwerpID;
            $bod->gebruikerID = Auth::user()->gebruikerID;
            $bod->bedrag = $request->input('bod');
                $bod->save();
            }
            catch (\Exception $ex) {
                return Redirect::back()->withInput(Input::all());
            }
            echo '<script language="javascript">';
            echo 'alert("Bod geplaatst")';
            echo '</script>';
            return Redirect::route('veiling_details', ['id' => $veiling->voorwerpID]);
        }
        else{
            Redirect::to('/');
        }
    }
    public function createVeilingView() {
        $allrubrieken = Rubriek::all();

        $rubrieken = Array();
        foreach ($allrubrieken as $rubriek) {
            if(Rubriek::where('bovenliggendID', $rubriek->rubriekID)->count() === 0){
                array_push($rubrieken, $rubriek);
            }
        }
        return view('veilingaanmaken')->with('rubrieken', $rubrieken);
    }
    public function addVeiling(VeilingAanmakenRequest $request) {
        DB::beginTransaction();
        try{
            $veiling = new Veiling();
            $veiling->gebruikerID = Auth::user()->gebruikerID;
            $veiling->titel = $request->titel;
            $veiling->startprijs = $request->min;
            $veiling->dagen = $request->looptijd;
            $veiling->startprijs = $request->min;
            $veiling->beschrijving = $request->beschrijving;
            $veiling->betalingsinstructie = $request->betalingsinstructie;
            $veiling->betalingswijzenaam = $request->betalingswijze;
            $veiling->verzendinstructies = $request->verzendinstructie;
            $veiling->verzendwijzenaam = $request->verzendwijzenaam;
            $veiling->save();
			
			$veiling = Veiling::where('gebruikerID' , Auth::user()->gebruikerID)->where('titel', $request->titel)->where('beschrijving', $request->beschrijving)->first();

            $rubrieken = Array($request->rubriek1, $request->rubriek2);
            foreach($rubrieken as $r) {
                $rubriek = new Veiling_Rubriek();
                $rubriek->rubriekID = $r;
                $rubriek->voorwerpID = $veiling->voorwerpID;
                $rubriek->save();
            }
            if ($request->hasFile('afbeeldingen')) {
                if(count($request->file('afbeeldingen')) > 4) {
                    return Redirect::back()->withInput($request->input())->withErrors(["U mag niet meer dan 4 afbeeldingen uploaden."]);
                }
                $extensions = ["jpg", "png", "bmp"];
                $extensionerror = true;
                foreach ($request->file('afbeeldingen') as $afb) {
                    foreach($extensions as $extension) {
                        //dd($afb->getClientOriginalExtension());
                        if($afb->getClientOriginalExtension() == $extension){
                            $extensionerror = false;
                        }
                    }
                    if($extensionerror) {
                        return Redirect::back()->withInput($request->input())->withErrors(["Extensie moet .png, jpg of .bmp zijn."]);
                    }
                }
                foreach ($request->file('afbeeldingen') as $index => $afb) {
                    $map = base_path().'/public/img/veilingen/'.'/'.$veiling->gebruikerID.'/'.$veiling->voorwerpID.'/';
                    $filename = $index.'.'.$afb->getClientOriginalExtension();
                    $afb->move($map, $filename);
                   $bestand = new Bestand();
                    $bestand->bestandsnaam = $filename;
                    $bestand->voorwerpID = $veiling->voorwerpID;
                    $bestand->save();
                }
            }
            DB::commit();
        }
        catch (Exception $ex) {
            DB::rollback();
            return Redirect::back()->withInput(Input::all());
        }
        echo '<script language="javascript">';
        echo 'alert("Bod geplaatst")';
        echo '</script>';
        return Redirect::to('/');
    }
    public function feedback(Request $request) {
        $veilingid = $request->input('id');
        if(empty(Veiling::find($veilingid))){
            return Redirect::to('/');
        }
        $verkoperid = Veiling::find($veilingid)->gebruikerID;
        $koperid = -1;
        $feedbackgebruiker = null;
        if(!empty(Bod::where('voorwerpID', $veilingid)->orderBy('bedrag', 'desc')->take(1)->first())) {
            $koperid = Bod::where('voorwerpID', $veilingid)->orderBy('bedrag', 'desc')->take(1)->first()->gebruikerID;
            $feedbackgebruiker = $verkoperid;
        }
        elseif(empty(Bod::where('voorwerpID', $veilingid)->orderBy('bedrag', 'desc')->take(1)->first())) {
            return Redirect::to('/');
        }
        else {
            $feedbackgebruiker = $koperid;
        }
        $currentuserid = Auth::user()->gebruikerID;
        if(empty(Bod::where('voorwerpID', $veilingid)->orderBy('bedrag', 'desc')->take(1)->first()) && $currentuserid == $verkoperid){
            $veiling = Veiling::find($request->input('id'));
            return view('feedback')->with('veiling', $veiling);
        }
        elseif($currentuserid != $koperid
            || $currentuserid != $verkoperid) {
            return Redirect::to('/');
        }
        $veiling = Veiling::find($request->input('id'));
        return view('feedback')->with('veiling', $veiling)->with('feedbackgebruiker', $feedbackgebruiker);
    }
    public function giveFeedback(Request $request) {
        $veilingid = $request->input('id');
        $feedback = new Feedback();
        $feedback->gebruikerID = Auth::user()->gebruikerID;
        $feedback->cijfer = $request->cijfer;
        $feedback->commentaar = $request->commentaar;
        $feedback->save();
        echo '<script language="javascript">';
        echo 'alert("Feedback is aangemaakt.")';
        echo '</script>';
        return Redirect::to('/');
    }
}
