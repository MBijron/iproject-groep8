<?php

namespace App\Http\Controllers;

use App\Http\Traits\FilterRubrieken;
use App\Rubriek;
use App\Veiling;
use App\Veiling_Rubriek;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\PaginationServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use phpDocumentor\Reflection\Types\Array_;
use App\Http\Traits\PrintRubrieken;

class ZoekController extends Controller
{
    use PrintRubrieken;
    use FilterRubrieken;

    public function create(Request $request)
    {
        $zoekterm = $request->input('zoekterm');
        $zoekrubriek = $request->input('rubriek');
        $veilingen = $this->getzoekVeilingen($zoekrubriek, $zoekterm);
        $rubrieken = Rubriek::where('bovenliggendID', -1)->get();
        $rubriekendata = $this->generatePageTree($rubrieken);
        $veilingen->setPath(url()->current()."?zoekterm=".$zoekterm."&rubriek=".$zoekrubriek."&_token=".$request->_token);
        return view('zoekresultaat')->with('rubrieken', $rubriekendata)->with('veilingen', $veilingen);
    }
    private function getzoekVeilingen($zoekrubriek, $zoekterm) {
        if($zoekrubriek == -1)
		{
			return Veiling::where('gesloten', false)->where('titel', 'LIKE', '%'.$zoekterm.'%')->orderBy('datum')->paginate(8);
		}
		else
		{
			$ids = [];
			$rubrieken = $this->getSubrubrieken(Rubriek::find($zoekrubriek));
			if(!isset($rubrieken))
			{
				return Response::view('404', array(), 404);
			}
			foreach($rubrieken as $rubriek)
			{
				array_push($ids, $rubriek->rubriekID);
			}
			return Veiling::whereHas('rubrieken', function($a) use ($ids) {
				$a->whereIn('RUBRIEK.rubriekID', $ids);
			})->where('gesloten', false)->where('titel', 'LIKE', '%'.$zoekterm.'%')->orderBy('datum')->paginate(8);
		}
    }

    public function showRubriek(Request $request)
    {
        if(empty($request->input('id'))) {
            return Response::view('404', array(), 404);
        }
        $rubriekID = $request->input('id');
        //$veilingen = Veiling::where('gesloten', false)->get();
		$ids = [];
		$rubrieken = $this->getSubrubrieken(Rubriek::find($rubriekID));
		foreach($rubrieken as $rubriek)
		{
			array_push($ids, $rubriek->rubriekID);
		}
		$veilingen = Veiling::whereHas('rubrieken', function($a) use ($ids) {
			$a->whereIn('RUBRIEK.rubriekID', $ids);
		})->where('gesloten', false)->orderBy('datum')->get();
        $currentPage = Paginator::resolveCurrentPage();
        //$veilingen = $this->filterOpRubriek($veilingen, $rubriekID);
        $currentPageSearchResults = $veilingen->slice(($currentPage-1) * 8, 8)->all();
        $paginator = new LengthAwarePaginator($currentPageSearchResults, count($veilingen), 8);
        $paginator->setPath(url()->current()."?id=".$rubriekID);
        $rubrieken = $this->generatePageTree(Rubriek::all());
        return view('veilingen')->with('veilingen', $paginator)->with('rubrieken', $rubrieken);
    }
}
