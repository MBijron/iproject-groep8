<?php

namespace App\Http\Controllers;

//use Request;
use App\Http\Requests\GebruikerAanmakenRequest;
use App\Http\Requests\InsertEmailRequest;
use App\Mail\activation;
use App\Mail\ConfirmGebruiker;
use App\Vraag;
use Faker\Provider\cs_CZ\DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Gebruiker;
use App\Telefoon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use League\Flysystem\Exception;

class GebruikerController extends Controller
{
    public function insertEmail(InsertEmailRequest $request)
    {
        try{
            $gebruiker = new Gebruiker();
            $gebruiker->email = $request->email;
            $gebruiker->save();
            $gebruiker = Gebruiker::where('email', $request->email)->get();
            Mail::to($gebruiker[0]->email)->send(new activation($gebruiker[0]->activatiecode));
            echo '<script language="javascript">';
            echo 'alert("Account is aangemaakt! controleer aub uw email")';
            echo '</script>';
            return Redirect::to('/');
        }
        catch (Exception $ex) {
            return Redirect::back()->withInput(Input::all());
        }
    }
    public function create(Request $request)
    {
        if(empty($request->input('activatiecode'))) {
            return Response::view('404', array(), 404);
        }
        $activatiecode = $request->input('activatiecode');
        if(Gebruiker::where('activatiecode', $activatiecode)->get()) {
            $gebruiker = Gebruiker::where('activatiecode', $activatiecode)->first();
            if($gebruiker == null) {
                return Response::view('404', array(), 404);
            }
            if(strtotime($gebruiker->datum) < strtotime(date('y-m-d'))) {
                echo '<script language="javascript">';
                echo 'alert("Activatiecode is niet meer geldig")';
                echo '</script>';
                return Redirect::to('login');
            }
            $vragen = Vraag::all();
            return view('gebruikeraanmaken')->with('id', $gebruiker->gebruikerID)->with('vragen', $vragen);
        }
        return Redirect::to('/');
    }
    public function register(GebruikerAanmakenRequest $request)
    {
        DB::beginTransaction();
        try {
            $gebruiker = Gebruiker::find($request->id);

            $gebruiker->gebruikersnaam = $request->gebruikersnaam;
            $gebruiker->voornaam = $request->voornaam.$request->tussenvoegsel;
            $gebruiker->achternaam = $request->achternaam;
            $gebruiker->adres = $request->adres;
            if($request->password != $request->herhaal) {
                return Redirect::back()->withInput($request->input())->withErrors(['Wachtwoord en herhaling komen niet overeen.']);
            }
            $gebruiker->password =  bcrypt($request->password);
            $gebruiker->postcode = $request->postcode;
            $gebruiker->landnaam = $request->land;
            $gebruiker->plaatsnaam = $request->plaats;
            $gebruiker->vraagID = $request->lstVragen;
            $gebruiker->geboortedatum = $request->gebdatum;
            $gebruiker->antwoordtekst = $request->antwoord;
            $gebruiker->activated = 1;
            $gebruiker->save();

            $telefoons = $request->lstTelefoons;
            $volgnr = 1;
            if(count($telefoons) < 1) {
                return Redirect::back()->withInput($request->input())->withErrors(['U moet minimaal 1 telefoon toevoegen.']);
            }
            foreach($telefoons as $t) {
                $telefoon = new Telefoon();
                $telefoon->gebruikerID = $gebruiker->gebruikerID;
                $telefoon->telefoonnummer = $t;
                $telefoon->volgnr = $volgnr;
                $telefoon->save();
                $volgnr++;
            }
            DB::commit();
           // $gebruiker = Gebruiker::find($request->id);
           // Mail::to($gebruiker->email)->send(new ConfirmGebruiker($gebruiker->activatiecode));
            echo '<script language="javascript">';
            echo 'alert("Account is aangemaakt! controleer aub uw email om het te bevestigen")';
            echo '</script>';
            return Redirect::to('/');
        }
        catch(Exception $e){
            DB::rollback();
            return Redirect::back()->withInput(Input::all()->withErrors($this->errorBag()));
        }
    }
    public function Activate(Request $request)
    {
        if(empty($request->input('activatiecode'))) {
            return Response::view('404', array(), 404);
        }
        $activatiecode = $request->input('activatiecode');
            if(Gebruiker::where('activatiecode', $activatiecode)->where('activated', 0)->get()->count() > 0) {
                $gebruiker = Gebruiker::where('activatiecode', $activatiecode)->first();
                if(strtotime($gebruiker->datum) < strtotime(date('y-m-d'))){
                    return view('bevestigingscode_gebruiker_mislukt');
                }
                $gebruiker->activated = true;
                $gebruiker->save();

                return view('bevestigingscode_gebruiker');
            }
            else
            {
                return view('bevestigingscode_gebruiker_mislukt');
            }
    }
}