<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rubriek;
use App\Veiling;
use App\Veiling_Rubriek;
use App\Http\Traits\PrintRubrieken;
use App\Http\Traits\VeilingenSluiten;
use Illuminate\Database\Query\Expression as raw;

class HomeController extends Controller
{
    use PrintRubrieken;
    use VeilingenSluiten;
    public function create() {
        $rubrieken = Rubriek::where('bovenliggendID', -1)->get();
        $rubriekendata = $this->generatePageTree($rubrieken);

		//$nieuwenpopulair = Veiling::where('gebruikerID', 289)->get();
		$nieuwenpopulair = Veiling::select('voorwerp.voorwerpID', 'voorwerp.gebruikerID', 'voorwerp.dagen', 'voorwerp.startprijs', 'voorwerp.datum', 'voorwerp.tijd', 'voorwerp.titel', 'voorwerp.beschrijving', new raw("count(bodID) as aantalboden"))->leftJoin('bod', 'voorwerp.voorwerpID', '=', 'bod.voorwerpID')->where('gesloten', 0)->groupBy('voorwerp.voorwerpID', 'voorwerp.gebruikerID', 'voorwerp.dagen', 'voorwerp.startprijs', 'voorwerp.datum', 'voorwerp.tijd', 'voorwerp.titel', 'voorwerp.beschrijving')->orderBy('aantalboden', 'desc')->take(12)->get();
        $this->sluitVeilingen();
        //$data[] = Array('rubrieken' => $rubriekendata, 'veilingen' => $veilingen);
		//dd($nieuwenpopulair);
        return view('home')->with('rubrieken', $rubriekendata)->with('veilingen', $nieuwenpopulair);
    }
}
