<?php
namespace App\Http\Traits;

use App\Rubriek;

trait RandomRubriek {	
	
	public function randomRubriek() {
		$randomparent = Rubriek::where('bovenliggendID', -1)->inRandomOrder()->take(1)->get();
		return getChildren($randomparent, $randomparent);
	}
	
	private function hasChildren($parent) {
		return Rubriek::where('bovenliggendID', $parent)->get();
	}	
	
	private function getChildren($rubrieken, $parent) {
		$index = 0;
		foreach($rubrieken as $rubriek){
			$rubriekarray[$index] = $rubriek->rubriekID;
			$index++;
			if(Rubriek::where('bovenliggendID', $parent)->count('rubriekID')->get() > 0)
			{
				$children = hasChildren($parent);
				$returnarray = getChildren($children, $rubriek->rubriekID);
				foreach($returnarray as $return){
					$rubriekarray[$index] = $return->rubriekID;
					$index++;
				}
			}
		}
		return $rubriekarray;
	}
}