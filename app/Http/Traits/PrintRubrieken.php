<?php
namespace App\Http\Traits;
use App\Rubriek;

trait PrintRubrieken {
    public function generatePageTree($rubrieken){
		$tree = '<div class="custom-collapse">
  <button class="collapse-toggle visible-xs btn btn-orange" style="display: block; width: 100%;" type="button" data-toggle="collapse" data-parent="custom-collapse" data-target="#side-menu-collapse">
      <span class="">Bekijk rubrieken</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
  </button> ';
        $tree .= '<ul class="list-group collapse" id="side-menu-collapse">';
		$tree .= '<li class="list-group-item list-group-item-heading hidden-xs">';
		$tree .= '<h4>Rubrieken</h4>';
		$tree .= '</li>';
		$tree .= $this->createPageTree($rubrieken, -1, 0);
		$tree .= '</ul>';
		$tree .= '</div>';
		return $tree;
    }
	
	public function generateRubriekLine($rubriek)
	{
		$use = $rubriek;
		$line = array(0 => $use->rubrieknaam);
		
		for($i=1; $i<7000; $i++){
			if($use->bovenliggendID == -1){
				break;
			}
			
			$use = Rubriek::where('rubriekID', $use->bovenliggendID)->first();
			$line[$i] = $use->rubrieknaam;
		}
		
		foreach($line as $i => $element){
			$newline[count($line)-1-$i] = $element;
		}
		
		$return = '';
		for($i=0; $i < count($newline); $i++){
			$rubriekid = Rubriek::where('rubrieknaam', $newline[$i])->first();
			$return .= '<a href="veilingen?id='.$rubriekid->rubriekID.'">'.$newline[$i].'</a>';
			if($i == count($newline)-1){
			} else {
				$return .= ' > ';
			}
		}
		
		return $return;	
	}
	
	private function createPageTree($rubrieken, $parent, $depth)
	{
		if($depth > 10) return ''; // Geen endless recursion.
		$tree = '';

        foreach ($rubrieken as $rubriek) {

			if($rubriek->rubriekID > 0 && $rubriek->bovenliggendID == -1){
				if($rubriek->bovenliggendID == $parent){
					$tree .= '<li class="list-group-item dropdown-toggle">';
					$tree .= '<a href="veilingen?id='.$rubriek->rubriekID.'">';
					$tree .= $rubriek->rubrieknaam;
					$tree .= '</a>';
					$tree .= '</li>';
					$tree .= $this->createPageTree($rubrieken, $rubriek->rubriekID, $depth+1);
				}
			}
        }
        return $tree;
	}
}
?>