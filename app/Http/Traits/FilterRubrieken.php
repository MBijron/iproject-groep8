<?php
namespace App\Http\Traits;

use App\Rubriek;
use Illuminate\Database\Eloquent\Collection;

trait FilterRubrieken
{
    public function filterOpRubriek($alleveilingen, $gekozenrubriekID)
    {
        $depth = 10;
        $veilingen = new Collection();
        $rubriek = Rubriek::find($gekozenrubriekID);
        $rubrieken = $this->getSubrubrieken($rubriek);
		
		return Veiling::whereHas('rubrieken', function($a) {
			$a->where('rubriekID', $gekozenrubriekID);
		});
		
		
		
		
		
        $rubrieken->add($rubriek);
        $nonexistant = Array();
        foreach ($alleveilingen as $veiling) {
            foreach ($veiling->rubrieken as $veilingrubriek) {
                foreach ($rubrieken as $rubriek) {
                    if ($veilingrubriek->rubriekID == $rubriek->rubriekID) {
                        $veilingen->add($veiling);
                        array_push($nonexistant, $veiling->voorwerpID);
                        continue;
                    }
                }
            }
        }
        return $veilingen->unique();
    }

    public function getSubrubrieken($rubriek)
    {
		if(!isset($rubriek))
		{
			return null;
		}
        $subrubrieken = $rubriek->children;
        foreach ($subrubrieken as $subrubriek) {
            if (count($subrubriek->children) > 0) {
                $sen = $this->getSubrubrieken($subrubriek);
                foreach ($sen as $s) {
                    $subrubrieken->add($s);
                }
            }
        }
        return $subrubrieken;
    }
}
?>