<?php
namespace App\Http\Traits;

use App\Bod;
use App\Gebruiker;
use App\Mail\AankoopGelukt;
use App\Mail\VerkoopGelukt;
use App\Mail\VerkoopMislukt;
use App\Veiling;
use Illuminate\Support\Facades\Mail;

trait VeilingenSluiten {
    public function sluitVeilingen() {
        $this->updateDatabase();
        $this->mailUsers();
    }
    private function updateDatabase()
    {
        $veilingen = Veiling::where('gesloten', 0)->get();

        foreach ($veilingen as $index => $veiling) {
            $veilingDT = date('Y-m-d H:i:s', strtotime("$veiling->datum $veiling->tijd"));
            $veilingDT = date('Y-m-d H:i:s', strtotime('+'.$veiling->dagen.' day', strtotime("$veiling->datum $veiling->tijd")));
            if($veilingDT < date('Y-m-d H:i:s', time()) && $veiling->gesloten == 0) {
                $veiling->gesloten = 1;
                $veiling->save();
            }
        }
    }
    private function mailUsers()
    {
        $veilingen = Veiling::where('gesloten', 1)->where('isGemailed', 0)->orderBy('datum', 'asc')->take(5)->get();
        foreach ($veilingen as $veiling) {
            $verkoper = Gebruiker::find($veiling->gebruikerID);
            $koper = null;
            if (empty(Gebruiker::find(Bod::where('voorwerpID', $veiling->voorwerpID)->orderBy('bedrag', 'desc')->take(1)->first()))) {
                Mail::to($verkoper->email)->send(new VerkoopMislukt($verkoper, $veiling));
            } else {
                $koper = Gebruiker::find(Bod::where('voorwerpID', $veiling->voorwerpID)->orderBy('bedrag', 'desc')->take(1)->first()->gebruikerID);
                Mail::to($verkoper->email)->send(new VerkoopGelukt($verkoper, $veiling, $koper));
                Mail::to($koper->email)->send(new AankoopGelukt($koper, $veiling));
            }
            $veiling->isGemailed = 1;
            $veiling->save();
        }
    }
}
?>