<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VeilingAanmakenRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titel' => 'required|max:45|min:5',
            'beschrijving' => 'required|max:500',
            'betalingsinstructie' => 'max:500',
            'betalingswijzenaam' => 'max:9',
            'min' => 'required|min:0.01',
            'looptijd' => 'required|min:1',
            'verzendinstructies' => 'max:500',
            'verzendwijzenaam' => 'max:9',
        ];
    }
}
