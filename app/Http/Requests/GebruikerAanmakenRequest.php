<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GebruikerAanmakenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'gebruikersnaam' => 'required|unique:gebruiker|min:5x|max:35',
            'password' => 'required',
            'voornaam' => 'required',
            'achternaam' => 'required',
            'adres' => 'required',
            'antwoord' => 'required',
            'land' => 'required',
            'plaats' => 'required',
            'postcode' => 'required|regex:/[1-9][0-9][0-9][0-9][A-Z][A-Z]/',
        ];
    }
}
