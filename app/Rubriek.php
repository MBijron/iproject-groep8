<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rubriek extends Model
{
    public $table = 'Rubriek';
    public $primaryKey = 'rubriekID';
    public $timestamps = false;
	protected $keyType = "bigint";

    public function veilingen()
    {
        return $this->belongsToMany('App\Veiling', 'voorwerp_rubriek', 'rubriekID', 'voorwerpID');
    }
    public function parent()
    {
        return $this->belongsTo(self::class, 'bovenliggendID');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'bovenliggendID');
    }
}
