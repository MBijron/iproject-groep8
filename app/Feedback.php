<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 9-1-2017
 * Time: 15:03
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    public $table = 'Feedback';
    public $primaryKey = 'feedbackID';
    public $timestamps = false;

    public function gebruiker()
    {
        return $this->hasOne('App\Gebruiker', 'gebruikerID', 'gebruikerID');
    }
    public function veiling()
    {
        return $this->hasOne('App\Veiling', 'voorwerpID', 'voorwerpID');
    }
}