<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as auth;

class Gebruiker extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $table = 'Gebruiker';
    public $primaryKey = 'gebruikerID';
    public $timestamps = false;
	protected $keyType = "bigint";

    public function telefoons()
    {
        return $this->hasMany('App\Telefoon', 'gebruikerID');
    }
    public function verkoperObj()
    {
        return $this->belongsTo('App\Verkoper', 'gebruikerID', 'gebruikerID');
    }
}
