<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerkoopMislukt extends Mailable
{
    use Queueable, SerializesModels;

    public $gebruiker;
    public $veiling;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($gebruiker, $veiling)
    {
        $this->gebruiker = $gebruiker;
        $this->veiling = $veiling;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.VerkoopMislukt')->with(['gebruiker', $this->gebruiker])->with(['veiling',$this->veiling]);
    }
}
