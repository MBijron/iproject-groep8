<?php

namespace App\Mail;

use App\Gebruiker;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmGebruiker extends Mailable
{
    use Queueable, SerializesModels;

    public $code;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($code)
    {
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.BevestigGebruiker')->with(['code', $this->code]);
    }
}
