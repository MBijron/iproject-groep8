<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 16-1-2017
 * Time: 11:56
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AankoopGelukt  extends Mailable
{
    use Queueable, SerializesModels;

    public $koper;
    public $veiling;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($koper, $veiling)
    {
        $this->koper = $koper;
        $this->veiling = $veiling;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.AankoopGelukt')->with(['koper', $this->koper])->with(['veiling',$this->veiling]);
    }
}