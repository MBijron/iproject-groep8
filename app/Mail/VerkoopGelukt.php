<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 16-1-2017
 * Time: 11:23
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerkoopGelukt extends Mailable
{
    use Queueable, SerializesModels;

    public $gebruiker;
    public $veiling;
    public $koper;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($gebruiker, $veiling, $koper)
    {
        $this->gebruiker = $gebruiker;
        $this->veiling = $veiling;
        $this->koper = $koper;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.VeilingGelukt')->with(['gebruiker', $this->gebruiker])->with(['veiling',$this->veiling])->with('koper', $this->koper);
    }
}
