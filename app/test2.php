<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class test2 extends Model
{
    protected $table = "test2";
	public $timestamps = false;
	
	public function test()
    {
        return $this->belongsTo('App\test', 'testID', 'testID');
    }
}
