<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Veiling_Rubriek extends Model
{
    public $table = 'Voorwerp_Rubriek';
    public $primaryKey = 'voorwerp_rubriekID';
    public $timestamps = false;

}
